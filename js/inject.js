


url=window.location.href;


//全局变量
jkkb_bool=0;//监控看板定时变量
hyjk_bool=0;//行业监控定时变量
scdp_bool=0;//市场大盘定时变量
ssph_bool=0;//搜索排行定时变量
ssfx_bool=0;//搜索分析定时变量
ssfx_two_bool=0//搜索分析-相关分析定时变量
ssfx_three_bool=0//搜索分析-类目构成定时变量

ssrq_bool=0;//搜索人群定时变量
cpdc_bool=0;//产品洞察定时变量
jz_bool=0;//竞争全部页面定时变量
jz_bool_two=0;//竞争全部页面定时变量2
refs=[];



if (localStorage.one_time=="undefined"||localStorage.one_time==undefined) {
var timestamp =Math.round(new Date().getTime()/1000);
localStorage.one_time=timestamp;  //第一次登陆时间
}


$(function(){
	//控制范围，在生意参谋才追加
	if(url.indexOf("https://sycm.taobao.com") >= 0) { 
		   //追加layui
		   layui_css = '<link rel="stylesheet" href="'+chrome.runtime.getURL('js/layui/css/layui.css')+'">';
		   highcharts='<script src="https://cdn.highcharts.com.cn/highcharts/highcharts.js"></script>';

		   $("head").append(highcharts);
		   $("head").append(layui_css);

		 createScript(chrome.runtime.getURL('js/jquery-2.1.4.min.js'),()=>{
			createScript(chrome.runtime.getURL('js/layui/layui.js'),()=>{
				createScript(chrome.runtime.getURL('js/aes.js'),()=>{
					createScript(chrome.runtime.getURL('js/outside.js'));
				    });
			});
		});
	}
})









//市场
if(url.indexOf("https://sycm.taobao.com/mc/mq/") >= 0) { 

		//插入css样式
		xd_css=" <style> .any_color {color: #fff;background-color: #fcad33!important;border-color: #fcad33 !important;}  .canmou_btn {display: inline-block;line-height: 1;white-space: nowrap;cursor: pointer;border: 1px solid #dcdfe6;-webkit-appearance: none;text-align: center;box-sizing: border-box;outline: none;margin: 0;transition: .1s;font-weight: 500;padding: 6px 15px;margin-left: 20px;font-size: 14px;border-radius: 4px;color: #fff;background: #409EFF;border-color: #409EFF;} .button-highlight { background-color: rgb(254, 174, 27); color: rgb(255, 255, 255); border-color: rgb(254, 174, 27);}  .button-tiny {  font-size: 9.6px; height: 24px; line-height: 24px; padding: 0px 24px; } .button-primary, .button-primary-flat {  background-color: rgb(27, 154, 247); color: rgb(255, 255, 255); border-color: rgb(27, 154, 247);}  </style> ";
	
		$("head").append(xd_css);
		

		//监控看板
		if (url.indexOf("https://sycm.taobao.com/mc/mq/market_monitor") >= 0) {
				timing_monitoring_jkkb("监控看板");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/overview") >= 0) {
				timing_monitoring_jkkb("市场大盘");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/market_rank") >= 0) {
				timing_monitoring_jkkb("市场排行");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/search_rank") >= 0) {
				timing_monitoring_jkkb("搜索排行");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/search_analyze") >= 0) {
				timing_monitoring_jkkb("搜索分析");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/search_customer") >= 0) {
				timing_monitoring_jkkb("搜索人群");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/industry_customer") >= 0) {
				timing_monitoring_jkkb("行业客群");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/property_insight") >= 0) {
				timing_monitoring_jkkb("属性洞察");
		}else if (url.indexOf("https://sycm.taobao.com/mc/mq/product_insight") >= 0) {
				timing_monitoring_jkkb("产品洞察");
		}
//竞争
}else if(url.indexOf("https://sycm.taobao.com/mc/ci/") >= 0){

		//插入css样式
		xd_css=" <style> .any_color {color: #fff;background-color: #fcad33!important;border-color: #fcad33 !important;}  .canmou_btn {display: inline-block;line-height: 1;white-space: nowrap;cursor: pointer;border: 1px solid #dcdfe6;-webkit-appearance: none;text-align: center;box-sizing: border-box;outline: none;margin: 0;transition: .1s;font-weight: 500;padding: 6px 15px;margin-left: 20px;font-size: 14px;border-radius: 4px;color: #fff;background: #409EFF;border-color: #409EFF;} .button-highlight { background-color: rgb(254, 174, 27); color: rgb(255, 255, 255); border-color: rgb(254, 174, 27);}  .button-tiny {  font-size: 9.6px; height: 24px; line-height: 24px; padding: 0px 24px; } .button-primary, .button-primary-flat {  background-color: rgb(27, 154, 247); color: rgb(255, 255, 255); border-color: rgb(27, 154, 247);}  </style> ";
		$("head").append(xd_css);

		//监控店铺
		if (url.indexOf("https://sycm.taobao.com/mc/ci/shop/monitor") >= 0) {
				timing_monitoring_jkkb("监控店铺");
		}else if(url.indexOf("https://sycm.taobao.com/mc/ci/shop/recognition") >= 0){
			    timing_monitoring_jkkb("竞店识别");
		}else if(url.indexOf("https://sycm.taobao.com/mc/ci/shop/analysis") >= 0){
			    timing_monitoring_jkkb("竞店分析");
		}else if(url.indexOf("https://sycm.taobao.com/mc/ci/item/monitor") >= 0){
			    timing_monitoring_jkkb("监控商品");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/item/recognition") >= 0) {
				timing_monitoring_jkkb("竞品识别");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/item/analysis") >= 0) {
				timing_monitoring_jkkb("竞品分析");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/brand/monitor") >= 0) {
				timing_monitoring_jkkb("监控品牌");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/brand/recognition") >= 0) {
				timing_monitoring_jkkb("品牌识别");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/brand/analysis") >= 0) {
				timing_monitoring_jkkb("品牌分析");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/brand/customer") >= 0) {
				timing_monitoring_jkkb("品牌客群");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/movement/rival") >= 0) {
				timing_monitoring_jkkb("竞争动态");
		}else if (url.indexOf("https://sycm.taobao.com/mc/ci/config/rival") >= 0) {
				timing_monitoring_jkkb("竞争配置");
		}


}





$(document).on('click',".nameWrapper", function(e) { 





	if ($(this).text()=="监控看板") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="市场大盘") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="市场排行") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="搜索排行") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="搜索分析") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="搜索人群") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="行业客群") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="属性洞察") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="产品洞察") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="监控店铺") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="竞店识别") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="竞店分析") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="监控商品") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="竞品识别") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="竞品分析") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="监控品牌") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="品牌识别") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="品牌分析") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="品牌客群") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="竞争动态") {
		timing_monitoring_jkkb($(this).text());
	}else if ($(this).text()=="竞争配置") {
		timing_monitoring_jkkb($(this).text());
	}
});




















$(document).on('click',".ant-pagination-item.ant-pagination-item-active", function(e) { 
//切换分页的时候删除转换的指数

	table1 = $(this).parent().parent().parent().parent().parent().parent();

	table1.find(".zszh_tag").remove();
});


$(document).on('click',".ant-pagination-next", function(e) { 
//下一页的时候删除转换的指数

	table1 = $(this).parent().parent().parent().parent().parent().parent();
	table1.find(".zszh_tag").remove();

});

$(document).on('click',".ant-pagination-prev", function(e) { 
//上一页的时候删除转换的指数

	table1 = $(this).parent().parent().parent().parent().parent().parent();
	table1.find(".zszh_tag").remove();

});


//下一页的时候删除转换的指数
$(document).on('click',".index-page-arrow", function(e) { 

		table1 = $("#showPage13").parent().parent().parent().parent();
		table1.find(".zszh_tag").remove();

});


//选项卡切换的时候重新调用
$(document).on('click',".ebase-Switch__item.ebase-Switch__activeItem", function(e) { 

		nameWrapper =  $(".menuItem.level-leaf.selected.false.leaf").find(".nameWrapper") .text();

		if (nameWrapper=="搜索分析") {

			timing_monitoring_jkkb("搜索分析");
		}

});


//指数选项卡切换的时候清空已经转换的指数
$(document).on('click',".ant-radio.ant-radio-checked", function(e) { 
		table1 = $(this).parent().parent().parent().parent().parent().parent().parent();
		table1.find(".zszh_tag").remove();
});




$(document).on('click',"#showPage", function(e) { 

  table1 = $(this).parent().parent().parent().parent().find("table tr");

  arr_jyzs=[];
  arr_llzh=[];

 showPage_default = $(".mc-marketMonitor").find(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();

  table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

	    	zs_value_span= $(this).find(".alife-dt-card-common-table-sortable-value").find("span");

			  if (zs_value_span.length>0) {
				zs_value_span.remove();
			}

          //取所有列第三行的数据
          if (j==2) {
          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-value");
            arr_jyzs.push(new_jyzs.text());
          }

          if (showPage_default!="品牌") {
	          //取所有列第四行的数据
	          if (j==3) {
	            new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-value");
	          	arr_llzh.push(new_jyzs.text());
	          }
          }
    });
  });

  arr_all={arr_all:{arr_jyzs:arr_jyzs,arr_llzh:arr_llzh}}


  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	jyzs=result["jyzs"];
	            	llzh=result["llzh"];

				   table1.each(function(i){               
				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
				          //取所有列第二行的数据
				          if (j==2) {

				          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(jyzs[i-1]),0)+')</span>';

				          	$(this).find(".alife-dt-card-common-table-sortable-value").append(html);

				          }
				           if (showPage_default!="品牌") {
					          if (j==3) {
					        html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(llzh[i-1]),0)+')</span>';
				          	$(this).find(".alife-dt-card-common-table-sortable-value").append(html);
					          }
				            }
				    });
				  });
            }
   });

 });





$(document).on('click',"#showPage6", function(e) { 


	detection(); //检测




  obj = $(this).parent().parent().parent().parent();

  arr_jyzs=[];
  arr_zhl=[];
  butt_checked = obj.find(".ant-radio-wrapper.ant-radio-wrapper-checked").text();
  butt_checked=Trim(butt_checked);

  zs_values =  $(this).parent().parent().parent().next().find(".alife-dt-card-sycm-mc-customer-index").find("ul li");

  if (butt_checked=="搜索人气"||butt_checked=="点击人气"||butt_checked=="交易指数"||butt_checked=="客群指数"||butt_checked=="支付转化指数") {

	  	zs_values.each(function(i){               
		    zs_value = $(this).find(".index-value");

		    zs_value_span=zs_value.find("span");

			  if (zs_value_span.length>0) {

				zs_value_span.remove();
			}


			if (butt_checked=="支付转化指数") {

				if (zs_value.text()!="") {
					arr_zhl.push(zs_value.text());
				}

			}else{

				if (zs_value.text()!="") {
					arr_jyzs.push(zs_value.text());
				}
				
			}

		  	
	 	});

		  arr_all={arr_all:{arr_all:{1:arr_jyzs,zhl:arr_zhl}}};

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   
		            		arr_all=result["arr_all"];

	            			jyzs=arr_all[1];
	            			zhl_arr=arr_all["zhl"];

					     	zs_values.each(function(i){    

							    zs_value = $(this).find(".index-value");

							    if (butt_checked=="支付转化指数") {
							    html='<span myid="change" class="zszh_tag"  style="color:#409EFF">('+Math.round((zhl_arr[i]*100))+'%)</span>';
						    	zs_value.append(html);
	
							    }else{
							    html='<span myid="change" class="zszh_tag"  style="color:#409EFF">('+formatMoney(Math.round(jyzs[i]),0)+')</span>';
						    	zs_value.append(html);
	
							    }
						 	});
		            }
		   });
    }else{
    	alert("没有需要计算的数值");
    }

 });




$(document).on('click',"#showPage13", function(e) { 

  detection(); //检测

  obj = $(this).parent().parent().parent().parent();

  arr_1=[];
  zs_title=[];
  arr_zhl=[];

  butt_checked = obj.find(".ant-radio-wrapper.ant-radio-wrapper-checked").text();

  zs_values_obj = $(this).parent().parent().parent().next().find(".alife-one-design-sycm-indexes-trend-index-item-selectable").find(".oui-pull-right.oui-index-cell-subIndex-value .oui-num");


  nameWrapper =  $(".menuItem.level-leaf.selected.false.leaf").find(".nameWrapper") .text();

	zs_values_obj.each(function(i){     

			  indexValue_span = $(this).find("span");

			  if (indexValue_span.length>0) {
				indexValue_span.remove();
			   }
			  	
			  	self_title=$(this).parent().parent().parent().find(".oui-index-cell-indexName").text();

			  	if (self_title=="交易指数"||self_title=="流量指数"||self_title=="搜索人气"||self_title=="收藏人气"||self_title=="加购人气"||self_title=="客群指数"||self_title=="预售定金交易指数") {

			  		arr_1.push($(this).text());

			  	}else if(self_title=="支付转化指数"){
			  		arr_zhl.push($(this).text());	
			  		arr_1.push(0);		  		
			  	}else{
			  		arr_1.push(0);
			  	}


	});



  arr_all={arr_all:{arr_all:{1:arr_1,zhl:arr_zhl}}};

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   

		            	arr_all=result["arr_all"];
		            	jyzs_arr=arr_all[1];
		            	zhl_arr=arr_all["zhl"];

		            	tag=0;

		            	zs_values_obj.each(function(i){               
			  	
								  	self_title=$(this).parent().parent().parent().find(".oui-index-cell-indexName").text();

			  						if (self_title=="交易指数"||self_title=="流量指数"||self_title=="搜索人气"||self_title=="收藏人气"||self_title=="加购人气"||self_title=="客群指数"||self_title=="预售定金交易指数") {

								    html='  <span myid="change" class="zszh_tag"  style="color:#409EFF"> </br> ('+formatMoney(Math.round(jyzs_arr[i]),0)+')</span>';
							    	$(this).append(html);

								  	}else if(self_title=="支付转化指数"){
								  	
						  			if (nameWrapper=="竞店分析"||nameWrapper=="品牌分析") {
					            		html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>  ('+decimal((zhl_arr[tag]*100),2)+'%)</span>'
					            		tag++;
					            	}else{	
					            	   tag=i;
					            	   html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>  ('+decimal((zhl_arr[tag]*100),2)+'%)</span>'
					            	}

							    	$(this).append(html);

								  	}
						});

		            }
		   });
    

 });






$(document).on('click',"#showPage21", function(e) { 


 detection(); //检测

  obj = $(this).parent().parent().parent().parent();

  arr_1=[];
  zs_title=[];
  arr_zhl=[];


    zs_values_obj = obj.find(".oui-index-cell-indexValue.oui-num");

	zs_values_obj.each(function(i){     

			  indexValue_span = $(this).find("span");

			  if (indexValue_span.length>0) {
				indexValue_span.remove();
			   }
			  	
			  	self_title=$(this).prev().text();

			  	if (self_title=="搜索人气"||self_title=="搜索热度"||self_title=="点击人气"||self_title=="点击热度"||self_title=="交易指数") {

			  		arr_1.push($(this).text());

			  	}else if(self_title=="支付转化指数"){
			  		arr_zhl.push($(this).text());	
			  		arr_1.push(0);		  		
			  	}else{
			  		arr_1.push(0);
			  	}


	});


  arr_all={arr_all:{arr_all:{1:arr_1,zhl:arr_zhl}}};

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   

		            	arr_all=result["arr_all"];
		            	jyzs_arr=arr_all[1];
		            	zhl_arr=arr_all["zhl"];

		            	tag=0;

		            	zs_values_obj.each(function(i){               

			  						self_title=$(this).prev().text();
									if (self_title=="搜索人气"||self_title=="搜索热度"||self_title=="点击人气"||self_title=="点击热度"||self_title=="交易指数") {
								   
								    html='  <span myid="change" class="zszh_tag"  style="color:#409EFF"> </br> ('+formatMoney(Math.round(jyzs_arr[i]),0)+')</span>';
							    	$(this).append(html);

								  	}
						});

		            }
		   });
 });




$(document).on('click',"#showPage9", function(e) { 

  detection(); //检测


  obj = $(this).parent().parent().parent().parent();

  arr_jyzs=[];

  zs_values =  obj.find(".index-area-multiple-container");

  indexValue_span = zs_values.children().eq(0).find(".oui-index-cell-indexValue.oui-num").find("span");

  if (indexValue_span.length>0) {
	indexValue_span.remove();
   }

  indexValue = zs_values.children().eq(0).find(".oui-index-cell-indexValue.oui-num");

  arr_jyzs.push(indexValue.text());

  arr_all={arr_all:{arr_jyzs:arr_jyzs}}

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   
			            	jyzs=result["jyzs"];
			            	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"><br />('+formatMoney(Math.round(jyzs[0]),0)+')</span>';
			            	zs_values.children().eq(0).find(".oui-index-cell-indexValue.oui-num").append(html);

		            }
		   });

 });



$(document).on('click',"#showPage23", function(e) { 

	detection(); //检测

    obj = $(this).parent().parent();

    arr_jyzs=[];

    zs_values =  obj.next().find(".oui-tab-switch-item-custom table tbody");

	zs_values.each(function(i){               
	zs_value = $(this).children().eq(0).children().eq(1);

	zs_value_span=zs_value.find("span");

	  if (zs_value_span.length>0) {

		zs_value_span.remove();
	}

		arr_jyzs.push(zs_value.text());
	});

  arr_all={arr_all:{arr_jyzs:arr_jyzs}}

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   
			            	jyzs=result["jyzs"];
			            	
						  	zs_values.each(function(i){               
						    zs_value = $(this).children().eq(0).children().eq(1);

							html='<span myid="change" class="zszh_tag"  style="color:#409EFF"><br />('+formatMoney(Math.round(jyzs[i]),0)+')</span>';
			            	zs_value.append(html);
							});
		            }
		   });
 });




$(document).on('click',"#showPage24", function(e) { 


	detection(); //检测

    obj = $(this).parent().parent().next();

    arr_all=[];
    title_arr=[];

    table1 =  obj.find(" table tr ");

table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}

        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          
            new_jyzs_span = $(this).find(".alife-dt-card-common-table-sortable-value").find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}

	          if (title_arr[j]=="点击人气"||title_arr[j]=="点击热度") {

	          	if (arr_all.hasOwnProperty(j)) {

	          		temp_arr=arr_all[j];
	          		temp_arr.push($(this).find(".alife-dt-card-common-table-sortable-value").text());
	          		arr_all[j] = temp_arr;
	          	}else{

	          		temp_arr=[];
	          		temp_arr.push($(this).find(".alife-dt-card-common-table-sortable-value").text());
	          		arr_all[j] = temp_arr;
	          	}
	          }
    });

  });

  arr_all={arr_all:{arr_all:arr_all} };

		$.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];
	            	table1.each(function(i){   

					 	   $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

					 	   	    zs_value = $(this).find(".alife-dt-card-common-table-sortable-value");
					 	   	    if (title_arr[j]=="点击人气"||title_arr[j]=="点击热度") {
					 	   	    html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(arr_all[j][i-1]),0)+')</span>'
						    	zs_value.append(html);
						   		 }
					 	   });
				 	});
            }
   });

 });



























$(document).on('click',"#showPage11", function(e) { 

 detection(); //检测

  obj = $(this).parent().parent().parent().parent();

  arr_jyzs=[];

  multiple_container =  obj.find(".index-area-multiple-container").children();

 arr_1=[];
 arr_zhl=[];

multiple_container.each(function(i){    


zs_value_span=$(this).find(".oui-index-cell-indexValue.oui-num").find("span");
if (zs_value_span.length>0) {
zs_value_span.remove();
}

indexValue = $(this).find(".oui-index-cell-indexValue.oui-num").text();
indexName = $(this).find(".oui-index-cell-indexName").text();


	if (indexName=="交易指数"||indexName=="流量指数"||indexName=="搜索人气"||indexName=="加购人气"||indexName=="支付转化指数"||indexName=="客群指数"||indexName=="收藏人气") {

		if (indexName=="支付转化指数") {
			 arr_zhl.push(indexValue);  
			 arr_1.push(0);
		}else{

		arr_1.push(indexValue);

		}
	}

	});

  arr_all={arr_all:{arr_all:{1:arr_1,zhl:arr_zhl}}};

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   
			            	arr_all=result["arr_all"];

			            	arr_jyzs=arr_all[1];
			            	arr_zhl=arr_all["zhl"];


			            	multiple_container.each(function(i){    
							indexValue = $(this).find(".oui-index-cell-indexValue.oui-num").text();
							indexName = $(this).find(".oui-index-cell-indexName").text();
							// oui-index-cell-indexName

								if (indexName=="交易指数"||indexName=="流量指数"||indexName=="搜索人气"||indexName=="加购人气"||indexName=="支付转化指数"||indexName=="客群指数"||indexName=="收藏人气") {

									if (indexName=="支付转化指数") {

										html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>('+Math.round((arr_zhl[0]*100))+'%)</span>'
										$(this).find(".oui-index-cell-indexValue.oui-num").append(html);

									}else{



									 html='<span myid="change" class="zszh_tag"  style="color:#409EFF"><br />('+formatMoney(Math.round(arr_jyzs[i]),0)+')</span>';
									 $(this).find(".oui-index-cell-indexValue.oui-num").append(html);

									}
								}

								});

		            }
		   });

 });







$(document).on('click',"#showPage7", function(e) { 

	detection(); //检测

  obj = $(this).parent().parent().parent().parent();
  
  arr_jyzs=[];

  butt_checked = obj.find(".ant-radio-wrapper.ant-radio-wrapper-checked").text();

  zs_values =  $(this).parent().parent().parent().next().find(".alife-dt-card-sycm-mc-customer-index").find("ul li");




	  	zs_values.each(function(i){               
		    zs_value = $(this).find(".index-value");

		    zs_value_span=zs_value.find("span");

			  if (zs_value_span.length>0) {

				zs_value_span.remove();
			}

		  	arr_jyzs.push(zs_value.text());
	 	});



		  arr_all={arr_all:{arr_jyzs:arr_jyzs}}

		  $.ajax({
		            type:'post',
		            url:"https://work.dinghaiec.com/set_save.php",  
		            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
		            dataType:'json',
		            async: false,
		            beforeSend: function () {        
		            },
		            success:function(result)
		            {   
			            	jyzs=result["jyzs"];

					     	zs_values.each(function(i){    

							    zs_value = $(this).find(".index-value");

							    if (isArray(jyzs[i])) {
							    	html='<span myid="change" class="zszh_tag"  style="color:#409EFF">('+formatMoney(Math.round(jyzs[i]),0)+')</span>';
							    	zs_value.append(html);
							    }


						 	});
		            }
		   });

 });







//判断是否为数组且至少有一个元素;
function isArray(arr){
    if(!$.isArray(arr)){
        return false;
    }else if(arr.length<1){
        return false;
    }else{
        return true;
    }
}







//搜索人群转化-top省份-top城市
$(document).on('click',".showPage_common", function(e) { 

	detection(); //检测

  obj = $(this).parent().parent().parent();

  arr_all_sub=[];

  zs_values =  obj.find(".ant-row.oui-row").children();

	zs_values.each(function(i){   

	 table_trs	= $(this).find(" table tr ");

	 		arr=[];
	 
	 	  	table_trs.each(function(i){      

	 	  	zs_value_span=$(this).find(".value-td-value").find("span");

			  if (zs_value_span.length>0) {

				zs_value_span.remove();
			}


		    zs_value = $(this).find(".value-td-value").text();
		 

		    if (zs_value!="") {
		
		    	arr.push(zs_value);

		    }

	 	});

	 	  	arr_all_sub.push(arr);
 	});

		  arr_all={arr_all:{arr_all:arr_all_sub} };

		$.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];

	            	zs_values.each(function(i){   

					table_trs	= $(this).find(" table tr ");

					 arr=[];
					 
					 	table_trs.each(function(i2){               
						    zs_value = $(this).find(".value-td-value");

						    if (zs_value.text()!="") {
						
						    	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(arr_all[i][i2-1]),0)+')</span>'

						    	zs_value.append(html);
						    }
					 	});
				 	});
            }
   });
});












//竞品分析-入店搜索词转换
$(document).on('click',"#showPage18", function(e) { 

	detection(); //检测

	title_default = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();

	if (title_default=="成交关键词") {
			 obj = $(this).parent().parent().parent().parent();

			  arr_all_sub=[];

			  zs_values =  obj.find(".ant-row.oui-row").children();



				zs_values.each(function(i){   

				 table_trs	= $(this).find(" table tr ");

				 		arr=[];

				 	  	table_trs.each(function(i){      

				 	  	zs_value_span=$(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-tradeIndex").children().eq(0).find("span");

						  if (zs_value_span.length>0) {

							zs_value_span.remove();
						}


					    zs_value = $(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-tradeIndex").children().eq(0).text();
					 
					    if (zs_value!="") {
					
					    	arr.push(zs_value);

					    }

				 	});

				 	  	arr_all_sub.push(arr);
			 	});

					  arr_all={arr_all:{arr_all:arr_all_sub} };

					$.ajax({
			            type:'post',
			            url:"https://work.dinghaiec.com/set_save.php",  
			            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
			            dataType:'json',
			            async: false,
			            beforeSend: function () {        
			            },
			            success:function(result)
			            {   
				            	arr_all=result["arr_all"];

				            	zs_values.each(function(i){   

								table_trs	= $(this).find(" table tr ");

								 arr=[];
								 
								 	table_trs.each(function(i2){               
									    zs_value = $(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-tradeIndex").children().eq(0);

									    if (zs_value.text()!="") {
									
									    	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(arr_all[i][i2-1]),0)+')</span>'

									    	zs_value.append(html);
									    }
								 	});
							 	});
			            }
			   });
	}else{

		alert("没有需要计算的数值");

	}


});






















//搜索人群转化-top省份-top城市
$(document).on('click',".showPage_xykq_common", function(e) { 

	detection(); //检测

  obj = $(this).parent().parent().parent();

  arr_all_sub=[];

	 table_trs	= obj.find(" table tr ");

	 		arr_jyzs=[];
	 
	 	  	table_trs.each(function(i){      

	 	  	zs_value_span=$(this).find("td:last .custom-column").find("span");

			  if (zs_value_span.length>0) {

				zs_value_span.remove();
			}


		    zs_value = $(this).find("td:last .custom-column").text();

		    if (zs_value!="") {
		
		    	arr_jyzs.push(zs_value);

		    }

	 	});

		arr_all={arr_all:{arr_jyzs:arr_jyzs}};


		$.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   

	            	arr_jyzs=result["jyzs"];

        	 	  	table_trs.each(function(i){      

					    zs_value = $(this).find("td:last .custom-column");

					    if (zs_value.text()!="") {

					    	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(arr_jyzs[i-1]),0)+')</span>'

					    	zs_value.append(html);
					    }

				 	});
            }
   });
});







//市场大盘点击转换
$(document).on('click',"#showPage4", function(e) { 


	detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");

  arr_jyzs=[];
  arr_llzh=[];


 showPage_default = $(".ebase-Switch__item.ebase-Switch__activeItem").text();

 showPage_two_default = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


tag_one=2;
tag_two=3;
 if (showPage_default=="搜索词"&&showPage_two_default=="热搜") {
tag_one=2;
tag_two=3;		
 }else if(showPage_default=="搜索词"&&showPage_two_default=="飙升"){
tag_one=3;
tag_two=4;	
 }else if(showPage_default=="长尾词"&&showPage_two_default=="热搜"){
tag_one=2;
tag_two=3;	
 }else if(showPage_default=="长尾词"&&showPage_two_default=="飙升"){
tag_one=3;
tag_two=4;	
 }else if(showPage_default=="品牌词"&&showPage_two_default=="热搜"){
tag_one=3;
tag_two=4;
 }else if(showPage_default=="品牌词"&&showPage_two_default=="飙升"){
tag_one=4;
tag_two=5;
 }else if(showPage_default=="核心词"&&showPage_two_default=="热搜"){
tag_one=3;
tag_two=4;
 }else if(showPage_default=="核心词"&&showPage_two_default=="飙升"){
tag_one=4;
tag_two=5;
 }else if(showPage_default=="修饰词"&&showPage_two_default=="热搜"){
tag_one=3;
tag_two=4;
 }else if(showPage_default=="修饰词"&&showPage_two_default=="飙升"){
tag_one=4;
tag_two=5;
 }


  table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

          //取所有列第三行的数据
          if (j==tag_one) {
          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");
          	if (new_jyzs.text().length>0) {
          		new_jyzs.text("");
          	}

            arr_jyzs.push($(this).text());

          }

	          //取所有列第四行的数据
	          if (j==tag_two) {
	          	
	            new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

	          	if (new_jyzs.text().length>0) {
	          		new_jyzs.text("");
	          	}

	          	arr_llzh.push($(this).text());
	          }

    });
  });


  // console.log(arr_jyzs);


  arr_all={arr_all:{arr_jyzs:arr_jyzs,arr_llzh:arr_llzh}}


  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	jyzs=result["jyzs"];
	            	llzh=result["llzh"];


				   table1.each(function(i){               
				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
				          //取所有列第二行的数据
				          if (j==tag_one) {
				          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(jyzs[i-1]),0)+')</span>';
				          	$(this).find(".alife-dt-card-common-table-sortable-cycleCrc").append(html);
				          }
				           
					          if (j==tag_two) {
					          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(llzh[i-1]),0)+')</span>';
				          	    $(this).find(".alife-dt-card-common-table-sortable-cycleCrc").append(html);
					          }
				        
				    });
				  });
            }
   });

 });







//属性洞察-热门属性选项课切换
$(document).on('click',".ebase-Switch__item.ebase-Switch__activeItem", function(e) { 

	nameWrapper =  $(".menuItem.level-leaf.selected.false.leaf").find(".nameWrapper") .text();

	if (nameWrapper=="属性洞察") {

		timing_monitoring_jkkb("属性洞察");
	}else if (nameWrapper=="产品洞察") {

		timing_monitoring_jkkb("产品洞察");
	}

});



//市场大盘点击转换
$(document).on('click',"#showPage8", function(e) { 


	detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");

  arr_1=[];
  arr_zhl=[];

 showPage_default = $(".ebase-Switch__item.ebase-Switch__activeItem").text();	


  table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

          //取所有列第三行的数据
          if (j==1) {
          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");



          	if (new_jyzs.text().length>0) {
          		new_jyzs.text("");

          	}

          	arr_1.push($(this).text());

          }

          	if (showPage_default=="属性分析") {

          		 //取所有列第三行的数据
		        if (j==3) {
		          		new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

			          	if (new_jyzs.text().length>0) {
			          		new_jyzs.text("");
			          	}
			        arr_zhl.push($(this).text());  	
         		}
			}
    });
  });



if (showPage_default=="属性分析") {
  arr_all={arr_all:{arr_all:{1:arr_1,zhl:arr_zhl}}};
}else{

  arr_all={arr_all:{arr_all:{1:arr_1}}};

}



  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];
	     

				   table1.each(function(i){               
				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
				          //取所有列第二行的数据
				          if (j==1) {
	
				          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(arr_all[j][i-1]),0)+')</span>'

				          	zs_value = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

						    zs_value.append(html);

				          }

				          if (showPage_default=="属性分析") {
					           if (j==3) {
		

					          	html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>('+Math.round((arr_all["zhl"][i-1][0]*100))+'%)</span>'

					          	zs_value = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

							    zs_value.append(html);

					          }
					       }

				    });
				  });
            }
   });




 });







//产品洞察点击转换
$(document).on('click',"#showPage10", function(e) { 

	detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");

  arr_1=[];
  arr_zhl=[];

 showPage_default = $(".ebase-Switch__item.ebase-Switch__activeItem").text();	



  table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

          //取所有列第三行的数据
          if (j==1) {
          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

          	if (new_jyzs.text().length>0) {
          		new_jyzs.text("");

          	}

          	arr_1.push($(this).text());

          }

          	if (showPage_default=="产品分析") {

          		 //取所有列第三行的数据
		        if (j==3) {
		          		new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

			          	if (new_jyzs.text().length>0) {
			          		new_jyzs.text("");
			          	}
			        arr_zhl.push($(this).text());  	
         		}
			}
    });
  });



if (showPage_default=="产品分析") {
  arr_all={arr_all:{arr_all:{1:arr_1,zhl:arr_zhl}}};
}else{

  arr_all={arr_all:{arr_all:{1:arr_1}}};

}



  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];
	      

				   table1.each(function(i){               
				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
				          //取所有列第二行的数据
				          if (j==1) {
	
				          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br>('+formatMoney(Math.round(arr_all[j][i-1]),0)+')</span>'

				          	zs_value = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

						    zs_value.append(html);

				          }

				          if (showPage_default=="产品分析") {
					           if (j==3) {
		
					          	html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>('+Math.round((arr_all["zhl"][i-1][0]*100))+'%)</span>'

					          	zs_value = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

							    zs_value.append(html);

					          }
					       }

				    });
				  });
            }
   });


 });











$(document).on('click',"#showPage3", function(e) { 

	detection(); //检测


  table1 = $(this).parent().parent().parent().parent().find("table tr");

  arr_jyzs=[];
  arr_llzh=[];


table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td


        	new_jyzs_span = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc").find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}

          //取所有列第三行的数据
          if (j==1) {

          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

          	if (new_jyzs.text().length>0) {
          		new_jyzs.text("");
          	}

            arr_jyzs.push($(this).text());

          }
    });
  });


  arr_all={arr_all:{arr_jyzs:arr_jyzs}}


  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	jyzs=result["jyzs"];
	            	llzh=result["llzh"];

				   table1.each(function(i){               
				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
				          //取所有列第二行的数据
				          if (j==1) {
				          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF">('+formatMoney(Math.round(jyzs[i-1]),0)+')</span>';
				          	$(this).find(".alife-dt-card-common-table-sortable-cycleCrc").append(html);
				          }
				    });
				  });
            }
   });




 });








$(document).on('click',"#showPage2", function(e) { 


detection(); //检测

table1 = $(this).parent().parent().parent().parent().find("table tr");

default_title = $(this).parent().parent().find(".oui-tab-switch-item.oui-tab-switch-item-active.default").text(); 

  arr_jyzs=[];
  arr_llzh=[];

table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

        	new_jyzs_span = $(this).find(".alife-dt-card-common-table-sortable-value").find(".zszh_tag");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.remove();
          	}


          	if (default_title=="热门商品") {


          		  if (j==3) {

		          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-value");

		          	// if (new_jyzs.text().length>0) {
		          	// 	new_jyzs.text("");
		          	// }

		            arr_jyzs.push($(this).text());

		          }


          	}else{


		          if (j==2) {

		          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-value");

		          	// if (new_jyzs.text().length>0) {
		          	// 	new_jyzs.text("");
		          	// }

		            arr_jyzs.push($(this).text());

		          }


          	}

         
    });
  });



  arr_all={arr_all:{arr_jyzs:arr_jyzs}}

  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	jyzs=result["jyzs"];
	            	llzh=result["llzh"];

				   table1.each(function(i){               
				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

				        	if (default_title=="热门商品") {
				        		  if (j==3) {
						          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> ('+formatMoney(Math.round(jyzs[i-1]),0)+')</span>';
						          	$(this).find(".alife-dt-card-common-table-sortable-value").append(html);
						          }
				        	}else{
				        		//取所有列第二行的数据
						          if (j==2) {
						          	html='<span myid="change" class="zszh_tag"  style="color:#409EFF"> <br> ('+formatMoney(Math.round(jyzs[i-1]),0)+')</span>';
						          	$(this).find(".alife-dt-card-common-table-sortable-value").append(html);
						          }
				        	}

				          
				    });
				  });
            }
   });

 });






// oui-tab-switch-item oui-tab-switch-item-active


$(document).on('click',".oui-tab-switch-item.oui-tab-switch-item-active.default", function(e) { 


table1 = $(this).parent().parent().parent().parent().parent().find("table tr");
table1.each(function(i){               
        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          //取所有列第三行的数据
    
          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

          	if (new_jyzs.text().length>0) {
          		new_jyzs.text("");
          	}
 
    });
  });





});


$(document).on('click',"#showPage5", function(e) { 

 detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");

  arr_1=[];
  arr_2=[];
  arr_3=[];
  arr_4=[];
  arr_5=[];

title_arr=[];

table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}

        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          //取所有列第三行的数据
          
          

	       if (j==1) {

	          if (title_arr[1]=="搜索人气"||title_arr[1]=="搜索热度"||title_arr[1]=="点击人气"||title_arr[1]=="点击热度"||title_arr[1]=="交易指数") {

	          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

	          	if (new_jyzs.text().length>0) {
	          		new_jyzs.text("");
	          	}

	            arr_1.push($(this).text());

	          }
          }

          

	         if (j==2) {

	          if (title_arr[2]=="搜索人气"||title_arr[2]=="搜索热度"||title_arr[2]=="点击人气"||title_arr[2]=="点击热度"||title_arr[2]=="交易指数") {

	          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

	          	if (new_jyzs.text().length>0) {
	          		new_jyzs.text("");
	          	}

	            arr_2.push($(this).text());

	          }
          }


          

	      if (j==3) {

	         if (title_arr[3]=="搜索人气"||title_arr[3]=="搜索热度"||title_arr[3]=="点击人气"||title_arr[3]=="点击热度"||title_arr[3]=="交易指数") {

	          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

	          	if (new_jyzs.text().length>0) {
	          		new_jyzs.text("");
	          	}

	            arr_3.push($(this).text());

	          }
          }


           

	    if (j==4) {

	         if (title_arr[4]=="搜索人气"||title_arr[4]=="搜索热度"||title_arr[4]=="点击人气"||title_arr[4]=="点击热度"||title_arr[4]=="交易指数") {

	          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

	          	if (new_jyzs.text().length>0) {
	          		new_jyzs.text("");
	          	}

	            arr_4.push($(this).text());

	          }
          }




	      if (j==5) {

	          if (title_arr[5]=="搜索人气"||title_arr[5]=="搜索热度"||title_arr[5]=="点击人气"||title_arr[5]=="点击热度"||title_arr[5]=="交易指数") {

	          	new_jyzs = $(this).find(".alife-dt-card-common-table-sortable-cycleCrc");

	          	if (new_jyzs.text().length>0) {
	          		new_jyzs.text("");
	          	}

	            arr_5.push($(this).text());

	          }
          }




         
    });

  });


  arr_all={arr_all:{arr_all:{1:arr_1,2:arr_2,3:arr_3,4:arr_4,5:arr_5}}}


  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];


	            	$.each(arr_all,function(index,value){

					     	if (value.length>0) {

					     		table1.each(function(i){               
							        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
							          //取所有列第二行的数据
							          if (j==index) {

							          	sontitle="";

							          	if (title_arr[index]=="搜索人气") {
							          		sontitle="搜索人数";
							          	}else if (title_arr[index]=="搜索热度") {
							          		sontitle="搜索热度";
							          	}else if (title_arr[index]=="点击人气") {
							          		sontitle="点击人数";
							          	}else if (title_arr[index]=="点击热度") {
							          		sontitle="点击热度";
							          	}else if (title_arr[index]=="交易指数") {
							          		sontitle="交易金额";
							          	}

							          	$(this).find(".alife-dt-card-common-table-sortable-cycleCrc").text(sontitle+":"+formatMoney(Math.round(value[i-1]),0));
							          	$(this).find(".alife-dt-card-common-table-sortable-cycleCrc").css("color","red");
									   }
									});
								});
					     	}
					});
            }
   });
 });














$(document).on('click',"#showPage12", function(e) { 


 detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");


  arr_1=[];
  arr_2=[];
  arr_3=[];
  arr_4=[];
  arr_5=[];
  arr_zhl=[];

title_arr=[];

table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}


        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          //取所有列第三行的数据
          
            new_jyzs_span = $(this).find(".shop-list-tbl-td").find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}

	       if (j==1) {

	          if (title_arr[j]=="全店流量指数"||title_arr[j]=="全店搜索人气"||title_arr[j]=="全店收藏人气"||title_arr[j]=="全店加购人气"||title_arr[j]=="类目交易指数"||title_arr[j]=="全店客群指数"||title_arr[j]=="全店预售定金指数"||title_arr[j]=="全店交易指数") {

	            arr_1.push($(this).text());
	          }else if(title_arr[j]=="全店支付转化指数"){
	          	arr_zhl.push($(this).text()); 
	          }

          }

          

	         if (j==2) {

	          if (title_arr[j]=="全店流量指数"||title_arr[j]=="全店搜索人气"||title_arr[j]=="全店收藏人气"||title_arr[j]=="全店加购人气"||title_arr[j]=="类目交易指数"||title_arr[j]=="全店客群指数"||title_arr[j]=="全店预售定金指数"||title_arr[j]=="全店交易指数") {

	            arr_2.push($(this).text());

	          }else if(title_arr[j]=="全店支付转化指数"){
	          	arr_zhl.push($(this).text()); 
	          }
          }


          

	      if (j==3) {

	         if (title_arr[j]=="全店流量指数"||title_arr[j]=="全店搜索人气"||title_arr[j]=="全店收藏人气"||title_arr[j]=="全店加购人气"||title_arr[j]=="类目交易指数"||title_arr[j]=="全店客群指数"||title_arr[j]=="全店预售定金指数"||title_arr[j]=="全店交易指数") {

	            arr_3.push($(this).text());

	          }else if(title_arr[j]=="全店支付转化指数"){
	          	arr_zhl.push($(this).text()); 
	          }
          }


           

	    if (j==4) {

	         if (title_arr[j]=="全店流量指数"||title_arr[j]=="全店搜索人气"||title_arr[j]=="全店收藏人气"||title_arr[j]=="全店加购人气"||title_arr[j]=="类目交易指数"||title_arr[j]=="全店客群指数"||title_arr[j]=="全店预售定金指数"||title_arr[j]=="全店交易指数") {

	            arr_4.push($(this).text());

	           }else if(title_arr[j]=="全店支付转化指数"){
	          	arr_zhl.push($(this).text()); 
	          }
          }




	      if (j==5) {

	          if (title_arr[j]=="全店流量指数"||title_arr[j]=="全店搜索人气"||title_arr[j]=="全店收藏人气"||title_arr[j]=="全店加购人气"||title_arr[j]=="类目交易指数"||title_arr[j]=="全店客群指数"||title_arr[j]=="全店预售定金指数"||title_arr[j]=="全店交易指数") {

	          

	            arr_5.push($(this).text());

	          }else if(title_arr[j]=="全店支付转化指数"){
	          	arr_zhl.push($(this).text()); 
	          }
          }


    });

  });


  arr_all={arr_all:{arr_all:{1:arr_1,2:arr_2,3:arr_3,4:arr_4,5:arr_5,zhl:arr_zhl}}}


  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];
	            	arr_zhl=arr_all["zhl"];

	            	$.each(arr_all,function(index,value){

					     	if (value.length>0) {

					     		table1.each(function(i){               
							        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
							          //取所有列第二行的数据
							          if (j==index) {

							          	if (title_arr[index]=="全店流量指数"||title_arr[index]=="全店搜索人气"||title_arr[index]=="全店收藏人气"||title_arr[index]=="全店加购人气"||title_arr[index]=="类目交易指数"||title_arr[index]=="全店客群指数"||title_arr[index]=="全店预售定金指数"||title_arr[index]=="全店交易指数") {

							          	html='<span myid="change" style="color:#409EFF"> <br>('+formatMoney(Math.round(value[i-1]),0)+')</span>';

							          	$(this).find(".shop-list-tbl-td").append(html);

							          	}
									   }

									});
								});
					     	}
					});


	            	//转化率单独处理

	            	if (arr_zhl.length>0) {

	            		table1.each(function(i){               
							        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
							          //取所有列第二行的数据
							        
							          if(title_arr[j]=="全店支付转化指数"){

							          	html='<span myid="change" style="color:#409EFF"> <br>('+Math.round(arr_zhl[i-1]*100)+'%)</span>';

							          	$(this).find(".shop-list-tbl-td").append(html);

							          	}
									   
									});
								});
	            	}
            }
   });
 });










$(document).on('click',"#showPage16", function(e) { 


 detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");


  arr_1=[];
  arr_2=[];
  arr_3=[];
  arr_4=[];
  arr_5=[];
  arr_zhl=[];

title_arr=[];

table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });
		}


        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          //取所有列第三行的数据
          
            new_jyzs_span = $(this).find(".alife-dt-card-common-table-sortable-value").find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}

	       if (j==1) {

	       		sortable_value = $(this).find(".alife-dt-card-common-table-sortable-value").text();

	          if (title_arr[j]=="流量指数"||title_arr[j]=="收藏人气"||title_arr[j]=="加购人气"||title_arr[j]=="交易指数"||title_arr[j]=="搜索人气"||title_arr[j]=="搜索人气") {

	            arr_1.push(sortable_value);
	          }else if(title_arr[j]=="支付转化指数"){
	          	arr_zhl.push(sortable_value); 
	          }

          }

          

	         if (j==2) {

	         	sortable_value = $(this).find(".alife-dt-card-common-table-sortable-value").text();

	          if (title_arr[j]=="流量指数"||title_arr[j]=="收藏人气"||title_arr[j]=="加购人气"||title_arr[j]=="交易指数"||title_arr[j]=="搜索人气"||title_arr[j]=="搜索人气") {



	            arr_2.push(sortable_value);

	          }else if(title_arr[j]=="支付转化指数"){
	          	arr_zhl.push($(this).text()); 
	          }
          }


          

	      if (j==3) {
	      		sortable_value = $(this).find(".alife-dt-card-common-table-sortable-value").text();
	         if (title_arr[j]=="流量指数"||title_arr[j]=="收藏人气"||title_arr[j]=="加购人气"||title_arr[j]=="交易指数"||title_arr[j]=="搜索人气"||title_arr[j]=="搜索人气") {
	            arr_3.push(sortable_value);
	          }else if(title_arr[j]=="支付转化指数"){
	          	arr_zhl.push(sortable_value); 
	          }
          }


           

	    if (j==4) {
	    	 sortable_value = $(this).find(".alife-dt-card-common-table-sortable-value").text();
	         if (title_arr[j]=="流量指数"||title_arr[j]=="收藏人气"||title_arr[j]=="加购人气"||title_arr[j]=="交易指数"||title_arr[j]=="搜索人气"||title_arr[j]=="搜索人气") {

	            arr_4.push(sortable_value);

	           }else if(title_arr[j]=="支付转化指数"){
	          	arr_zhl.push(sortable_value); 
	          }
          }




	      if (j==5) {
	      	  sortable_value = $(this).find(".alife-dt-card-common-table-sortable-value").text();
	          if (title_arr[j]=="流量指数"||title_arr[j]=="收藏人气"||title_arr[j]=="加购人气"||title_arr[j]=="交易指数"||title_arr[j]=="搜索人气"||title_arr[j]=="搜索人气") {

	            arr_5.push(sortable_value);

	          }else if(title_arr[j]=="支付转化指数"){
	          	arr_zhl.push(sortable_value); 
	          }
          }


    });

  });


  arr_all={arr_all:{arr_all:{1:arr_1,2:arr_2,3:arr_3,4:arr_4,5:arr_5,zhl:arr_zhl}}}

  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];
	            	arr_zhl=arr_all["zhl"];

	            	$.each(arr_all,function(index,value){

					     	if (value.length>0) {

					     		table1.each(function(i){               
							        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
							          //取所有列第二行的数据
							          if (j==index) {

							          	if (title_arr[index]=="流量指数"||title_arr[index]=="收藏人气"||title_arr[index]=="加购人气"||title_arr[index]=="交易指数"||title_arr[index]=="搜索人气") {

							          	html='<span myid="change" style="color:#409EFF"> <br>('+formatMoney(Math.round(value[i-1]),0)+')</span>';

							          	$(this).find(".alife-dt-card-common-table-sortable-value").append(html);

							          	}
									   }

									});
								});
					     	}
					});


	            	//转化率单独处理

	            	if (arr_zhl.length>0) {

	            		table1.each(function(i){               
							        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
							          //取所有列第二行的数据
							        
							          if(title_arr[j]=="支付转化指数"){

							          	html='<span myid="change" style="color:#409EFF"> <br>('+Math.round(arr_zhl[i-1]*100)+'%)</span>';

							          	$(this).find(".alife-dt-card-common-table-sortable-value").append(html);

							          	}
									   
									});
								});
	            	}
            }
   });
 });












$(document).on('click',"#showPage19", function(e) { 


 detection(); //检测


  table1 = $(this).parent().parent().parent().parent().find("table tr");



  // default_title =  $(this).parent().parent().parent().parent().find(".ant-radio.ant-radio-checked").next().text();





	arr_all={};
	arr_zhl=[];
	title_arr=[];


append_html='p';

	


is_parameter=0;
table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}


        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          
            new_jyzs_span = $(this).find(append_html).find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}

          		temp_title=title_arr[j];

	          if (temp_title.indexOf("客群指数")>=0||temp_title.indexOf("交易指数")>=0||temp_title.indexOf("流量指数")>=0) {

	          	is_parameter=1;
	          	if (arr_all.hasOwnProperty(j)) {

	          		temp_arr=arr_all[j];
	          		temp_arr.push($(this).find(append_html).text());
	          		arr_all[j] = temp_arr;
	          	}else{

	          		temp_arr=[];
	          		temp_arr.push($(this).find(append_html).text());
	          		arr_all[j] = temp_arr;
	          	}

	          }else if(temp_title.indexOf("支付转化指数")>=0){
	          	is_parameter=1;

	          	 	if (arr_all.hasOwnProperty("zhl")) {

		          		temp_arr=arr_all["zhl"];
		          		temp_arr.push($(this).find(append_html).text());
		          		arr_all["zhl"] = temp_arr;
		          	}else{

		          		temp_arr=[];
		          		temp_arr.push($(this).find(append_html).text());
		          		arr_all["zhl"] = temp_arr;
		          	}
	          }
    });

  });



	if (is_parameter==0) {
		alert("没有需要转化的数值");
		return ;
	}


  arr_all={arr_all:{arr_all:arr_all}};



  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            

	            	if (result.hasOwnProperty("arr_all")) {
	            		arr_all=result["arr_all"];

	            		$.each(arr_all,function(index,value){

						     	if (value.length>0) {

						     		table1.each(function(i){               
								        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
								          //取所有列第二行的数据
								          if (j==index) {

								          	temp_title=title_arr[index];

								           if (temp_title.indexOf("客群指数")>=0||temp_title.indexOf("支付转化指数")>=0||temp_title.indexOf("交易指数")>=0||temp_title.indexOf("流量指数")>=0) {
								          	
								          	html='<span myid="change" class="zszh_tag" style="color:#409EFF"> <br>('+formatMoney(Math.round(value[i-1]),0)+')</span>';

								          	$(this).find(append_html).append(html);

								          	}
										   }

										});
									});
						     	}
						});
	            	

	            	

	            	 	if (arr_all.hasOwnProperty("zhl")) {

	            	 		number=0
	            	 		arr_zhl=arr_all["zhl"];

            	 			table1.each(function(i){               
						        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
						          //取所有列第二行的数据

									   temp_title=title_arr[j];
									   if(temp_title.indexOf("支付转化指数")>=0){





										          	html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>('+Math.round(arr_zhl[number]*100)+'%)</span>';

										          	$(this).find(append_html).append(html);

										          	number++;

									   }

								});
							});

	            	 	}

	            	}
            }
   });
 });


















$(document).on('click',"#showPage20", function(e) { 


detection(); //检测

table1 = $(this).parent().parent().parent().parent().find("table tr");

arr_all={};
arr_zhl=[];
title_arr=[];

append_html='.alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-tradeIndex';

table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}

        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          
            new_jyzs_span = $(this).find(append_html).children().eq(0).find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}


          	new_jyzs_span_two = $(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-payRateIndex").children().eq(0).find("span");

          	if (new_jyzs_span_two.text().length>0) {
          		new_jyzs_span_two.text("");
          	}


          	


          	temp_title=title_arr[j];

	          if (temp_title.indexOf("客群指数")>=0||temp_title.indexOf("交易指数")>=0) {



	          	if (arr_all.hasOwnProperty(j)) {

	          		temp_arr=arr_all[j];
	          		temp_arr.push($(this).find(append_html).children().eq(0).text());
	          		arr_all[j] = temp_arr;
	          	}else{

	          		temp_arr=[];
	          		temp_arr.push($(this).find(append_html).children().eq(0).text());
	          		arr_all[j] = temp_arr;
	          	}

	          }else if(temp_title.indexOf("支付转化指数")>=0){

	          	 	if (arr_all.hasOwnProperty("zhl")) {

		          		temp_arr=arr_all["zhl"];
		          		temp_arr.push($(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-payRateIndex").children().eq(0).text());
		          		arr_all["zhl"] = temp_arr;
		          	}else{

		          		temp_arr=[];
		          		temp_arr.push($(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-payRateIndex").children().eq(0).text());
		          		arr_all["zhl"] = temp_arr;
		          	}



	          }
    });

  });



  arr_all={arr_all:{arr_all:arr_all}};



  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            

	            	if (result.hasOwnProperty("arr_all")) {
	            		arr_all=result["arr_all"];

	            		$.each(arr_all,function(index,value){

						     	// if (value.length>0) {

						     		table1.each(function(i){               
								        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
								          //取所有列第二行的数据
								          if (j==index) {

								          	temp_title=title_arr[index];

								           if (temp_title.indexOf("客群指数")>=0||temp_title.indexOf("支付转化指数")>=0||temp_title.indexOf("交易指数")>=0) {
								          	
								          	html='<span myid="change" class="zszh_tag" style="color:#409EFF"> <br>('+formatMoney(Math.round(value[i-1]),0)+')</span>';

								          	$(this).find(append_html).children().eq(0).append(html);

								          	}
										   }

										});
									});
						     	// }
						});
	            	

	            	

	            	 	if (arr_all.hasOwnProperty("zhl")) {

	            	 		arr_zhl=arr_all["zhl"];

            	 			table1.each(function(i){               
						        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
						          //取所有列第二行的数据

									   temp_title=title_arr[j];
									   if(temp_title.indexOf("支付转化指数")>=0){

										          	html='<span myid="change"  class="zszh_tag"  style="color:#409EFF"> <br>('+Math.round(arr_zhl[i-1]*100)+'%)</span>';

										          	$(this).find(".alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-payRateIndex").children().eq(0).append(html);

									   }

								});
							});

	            	 	}

	            	}
            }
   });
 });
































$(document).on('click',".showPage_zh_common", function(e) { 


  detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");

	arr_all=[];
	arr_1=[];
	arr_2=[];
	arr_3=[];
	arr_4=[];
	arr_5=[];
	arr_zhl=[];
	title_arr=[];


append_html='.alife-dt-card-common-table-sortable-value';
// append_html='.alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-tradeIndex';




table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}


        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
          
            new_jyzs_span = $(this).find(append_html).find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}

	          if (title_arr[j]=="流失指数"||title_arr[j]=="流失人气"||title_arr[j]=="流量指数"||title_arr[j]=="搜索人气"||title_arr[j]=="交易指数"||title_arr[j]=="点击人气"||title_arr[j]=="搜索热度"||title_arr[j]=="点击热度"||title_arr[j]=="交易指数") {


	          	if (arr_all.hasOwnProperty(j)) {

	          		temp_arr=arr_all[j];
	          		temp_arr.push($(this).text());
	          		arr_all[j] = temp_arr;
	          	}else{

	          		temp_arr=[];
	          		temp_arr.push($(this).text());
	          		arr_all[j] = temp_arr;
	          	}

	    

	          }


    });

  });



	


  arr_all={arr_all:{arr_all:arr_all}}

  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   
	            	arr_all=result["arr_all"];

	            	$.each(arr_all,function(index,value){

					     	if (value.length>0) {

					     		table1.each(function(i){               
							        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
							          //取所有列第二行的数据
							          if (j==index) {

							          	if (title_arr[index]=="流失指数"||title_arr[index]=="流失人气"||title_arr[index]=="流量指数"||title_arr[index]=="搜索人气"||title_arr[index]=="交易指数"||title_arr[index]=="点击人气"||title_arr[index]=="搜索热度"||title_arr[index]=="点击热度"||title_arr[index]=="交易指数") {

							          	html='<span myid="change" style="color:#409EFF"> <br>('+formatMoney(Math.round(value[i-1]),0)+')</span>';

							          	$(this).find(append_html).append(html);

							          	}
									   }

									});
								});
					     	}
					});

	            	 	if (arr_all.hasOwnProperty("zhl")) {
				            	//转化率单独处理
				            	if (arr_zhl.length>0) {

				            		table1.each(function(i){               
										        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td
										          //取所有列第二行的数据
										       
										          if(title_arr[j]=="全店支付转化指数"){

										          	html='<span myid="change" style="color:#409EFF"> <br>('+Math.round(arr_zhl[i-1]*100)+'%)</span>';

										          	$(this).find(append_html).append(html);

										          	}
												   
												});
											});
				            	}
	            	 	}
            }
   });
 });









$(document).on('click',"#showPage14", function(e) { 


 detection(); //检测

  table1 = $(this).parent().parent().parent().parent().find("table tr");

	arr_all=[];
	arr_jyzs=[];
	title_arr=[];


title_default = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();

	if (title_default=="热销") {
		append_html='.alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-tradeIndex';
	}else {
		append_html='.alife-dt-card-common-table-sortable-td.alife-dt-card-common-table-uvIndex';
	}


table1.each(function(i){    

		if (i==0) {

			    $(this).children('th').each(function(j){  // 遍历 tr 的各个 td

			    title_arr.push($(this).text());
			    });

		}


        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td

            new_jyzs_span = $(this).find(append_html).children().eq(0).find("span");

          	if (new_jyzs_span.text().length>0) {
          		new_jyzs_span.text("");
          	}


          if (title_arr[j]=="交易指数"||title_arr[j]=="流量指数") {

          		arr_jyzs.push($(this).find(append_html).children().eq(0).text());
          
          }

    });

  });

  arr_all={arr_all:{arr_jyzs:arr_jyzs}}






  $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function () {        
            },
            success:function(result)
            {   

            	jyzs=result["jyzs"];

            	g=0;
				table1.each(function(i){    

				        $(this).children('td').each(function(j){  // 遍历 tr 的各个 td




					          if (title_arr[j]=="交易指数"||title_arr[j]=="流量指数") {

					          	// console.log($(this),i);


					          		html='  <span myid="change" class="zszh_tag"  style="color:#409EFF"> </br>('+formatMoney(Math.round(jyzs[g]),0)+')</span>';

					          		$(this).find(append_html).children().eq(0).append(html);
					          		g++;
					             } 
				         });

				});

            }
        });
 

});




function timing_monitoring_jkkb(title) {



	//每次进新页面,清空所有定时计划
	if (refs.length>0) {
		for (var i = 0; i < refs.length; i++) {
		clearInterval(refs[i]);			
		}
	refs=[];
	}

	if (title=="监控看板") {
			ref = setInterval(function(){
			detection_jkmb(ref);
			},500);
			ref_xyjk = setInterval(function(){
			detection_xyjk(ref_xyjk);
			},500);
			refs.push(ref);
			refs.push(ref_xyjk);
	}else if(title=="市场大盘") {
			ref = setInterval(function(){
			detection_scdp(ref);
			},500);
		    refs.push(ref);

		    ref_jpcx = setInterval(function(){
				app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
			},500);
		    refs.push(ref_jpcx);

	}else if(title=="市场排行") {
			ref = setInterval(function(){
			detection_scph(ref);
			},500);
		    refs.push(ref);

			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="搜索排行") {
			ref = setInterval(function(){
			detection_ssph(ref);
			},500);
			refs.push(ref);
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="搜索分析") { 
			ref = setInterval(function(){
			detection_ssfx(ref);
			},500);
			refs.push(ref);
	}else if(title=="搜索人群") { 
			ref = setInterval(function(){
			detection_ssrq(ref);
			},500);
			refs.push(ref);
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="行业客群") { 
			ref = setInterval(function(){
			detection_hykq(ref);
			},500);
			refs.push(ref);
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="属性洞察") { 
			ref = setInterval(function(){
			detection_sxdc(ref);
			},500);
			refs.push(ref);
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="产品洞察") { 
			ref = setInterval(function(){
			detection_cpdc(ref);
			},500);
			refs.push(ref);
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="监控店铺") { 
			ref = setInterval(function(){
			detection_jkdp(ref);
			},500);
			refs.push(ref);
			jz_bool=0;
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="竞店识别") { 
			ref = setInterval(function(){
			detection_jdsb(ref);
			},500);
			refs.push(ref);
			jz_bool=0;
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="竞店分析") { 
			ref = setInterval(function(){
			detection_jdfx(ref);
			},500);
			
			ref_two = setInterval(function(){
			detection_jdfx_two(ref_two);
			},500);
			refs.push(ref);
			jz_bool=0;
			refs.push(ref_two);
			jz_bool_two=0;

			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="监控商品") { 
			ref = setInterval(function(){
			detection_jksp(ref);
			},500);
			refs.push(ref);
			jz_bool=0;

			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);

	}else if(title=="竞品识别") {
			ref = setInterval(function(){
			detection_jpsb(ref);
			},500);
			refs.push(ref);
			jz_bool=0;
			ref_jpcx = setInterval(function(){
					app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
				},500);
			refs.push(ref_jpcx);
	}else if(title=="竞品分析") { 
			ref = setInterval(function(){
			detection_jpfx(ref);
			},500);
		
			ref_jpfx_two = setInterval(function(){
			detection_jpfx_two(ref_jpfx_two);
			},500);
			refs.push(ref);
			jz_bool=0;
			refs.push(ref_jpfx_two);
			jz_bool_two=0;

			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);
	}else if(title=="监控品牌") { 
			ref = setInterval(function(){
			detection_jkpp(ref);
			},500);
			refs.push(ref);
			jz_bool=0;

			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);
	}else if(title=="品牌识别") { 
			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);

	}else if(title=="品牌分析") { 
			ref = setInterval(function(){
			detection_ppfx(ref);
			},500);
		
			ref_ppfx_two = setInterval(function(){
			detection_ppfx_two(ref_ppfx_two);
			},500);
			refs.push(ref);
			jz_bool=0;
			refs.push(ref_ppfx_two);
			jz_bool_two=0;

			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);

	}else if(title=="品牌客群") { 
			ref = setInterval(function(){
			detection_ppkq(ref);
			},500);
			refs.push(ref);
			jz_bool=0;

			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);
	}else if(title=="竞争动态") { 
			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);

	}else if(title=="竞争配置") { 
			ref_jpcx = setInterval(function(){
						app_arbitrarily_good(ref_jpcx,"ebase-FaCommonFilter__left");
					},500);
			refs.push(ref_jpcx);

	}

}



/*
 * formatMoney(s,type)
 * 功能：金额按千位逗号分割
 * 参数：s，需要格式化的金额数值.
 * 参数：type,判断格式化后的金额是否需要小数位.
 * 返回：返回格式化后的数值字符串.
 */
function formatMoney(s, type) {
	if (/[^0-9\.]/.test(s))
		return "0";
	if (s == null || s == "")
		return "0";
	s = s.toString().replace(/^(\d*)$/, "$1.");
	s = (s + "00").replace(/(\d*\.\d\d)\d*/, "$1");
	s = s.replace(".", ",");
	var re = /(\d)(\d{3},)/;
	while (re.test(s))
		s = s.replace(re, "$1,$2");
	s = s.replace(/,(\d\d)$/, ".$1");
	if (type == 0) {// 不带小数位(默认是有小数位)
		var a = s.split(".");
		if (a[1] == "00") {
			s = a[0];
		}
	}
	return s;
}







//检测市场大盘是否加载完毕
function detection_scdp(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage3");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    scdp_bool++;
		
			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (scdp_bool==200) {
				clearInterval(ref);
			}

			html = $("#cateCons");

			if (html.length>0) {

			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage3" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>        </div>';

			html.find(".oui-card-header").append(self_html);

			}
		}
}





//检测市场排行是否加载完毕
function detection_scph(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $(".showPage25");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    scdp_bool++;
		
			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (scdp_bool==200) {
				clearInterval(ref);
			}

			html = $(".oui-card");

			if (html.length>0) {

				// console.log("123");

			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;" id="export1"  class="button button-primary button-tiny">导出数据</button>        </div>';

			html.find(".oui-card-header").append(self_html);

			}
		}
}








//添加任意竞品按钮
function app_arbitrarily_good(ref,alass_title){

	//检测是否有任意竞品查询
    showPage_common_boj = $("#inserGoodsHelpBtn");

    if (showPage_common_boj.length>0) {
    	clearInterval(ref);
    }else{

    	html = $(".ebase-FaCommonFilter__top").find(".ebase-FaCommonFilter__left");
		if (html.length>0) {
		//停止定时计划
		clearInterval(ref);
		inserGoodsHelpBtn = ' <span style="margin-right: -18px; margin-left: 8px;  display:inline">店助手:</span>   <div id="inserLoginBtn"   style=" background: red;border: 1px solid red;"     class="canmou_btn orange_color" title="此功能由[店透视]提供">用户信息</div>  <a id="inserGoodsHelpBtn"    class="canmou_btn any_color   " title="此功能由[小鼎智投]提供" data-spm-anchor-id="a21ag.11815228.0.i2.186050a5IgIS8j">任意竞品查询</a>';
		html.append(inserGoodsHelpBtn);
		}
    }
}







//检测属性洞察是否加载完毕
function detection_sxdc(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage8");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    scdp_bool++;
		
			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (scdp_bool==200) {
				clearInterval(ref);
			}

				html = $(".oui-card");

				if (html.length>0) {
				//停止定时计划,插入转化按钮
				clearInterval(ref);

				showPage_default = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
				FaCommonFilter_root = $(".ebase-FaCommonFilter__root").next();
				

				if (showPage_default=="属性分析") {

					self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage9" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>        </div>';
					FaCommonFilter_root.children("div").eq(1).find(".oui-card-header").append(self_html);
					self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage8" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>         <button id="export_data89" style="      background: #f3d024!important;margin-left: 15px;   border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">导出数据</button>           </div>';
					FaCommonFilter_root.children("div").eq(2).find(".oui-card-header").append(self_html_two);

				}else{

					self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage8" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>         <button id="export_data89" style="      background: #f3d024!important;margin-left: 15px;   border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">导出数据</button>           </div>';
				 	html.find(".oui-card-header").append(self_html);

				}

				

			}
		}
}





	





$(document).on('keypress',".ant-input", function(e) { 

	    if (e.which == 13) {
	    	ref = setInterval(function(){
			detection_ssrq(ref);
			},500);
        }

});



//检测搜索人群是否加载完毕
function detection_ssrq(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $(".showPage_common");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    ssrq_bool++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (ssrq_bool==100) {
				clearInterval(ref);
			}

			html = $("#completeShopPortrait");

			if (html.length>0) {
			//停止定时计划,插入转化按钮

			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage6" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button></div>';
			self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="    border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_common button button-primary button-tiny">指数换算</button></div>';
			self_html_three='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="    border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_common button button-primary button-tiny">指数换算</button></div>';

			html.find(".oui-card-title").append(self_html);
			html.find(".ant-row.oui-row").children("div").eq(3).find(".portrait-title").append(self_html_two);
			html.find(".ant-row.oui-row").children("div").eq(4).find(".portrait-title").append(self_html_three);


			}
	    }	
}









//检测品牌客群是否加载完毕
function detection_ppkq(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $(".showPage_common");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    ssrq_bool++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (ssrq_bool==100) {
				clearInterval(ref);
			}

			html = $("#completeShopPortrait");

			if (html.length>0) {
			//停止定时计划,插入转化按钮

			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage6" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button></div>';
			self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="    border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_common button button-primary button-tiny">指数换算</button></div>';
			self_html_three='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="    border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_common button button-primary button-tiny">指数换算</button></div>';

			html.find(".oui-card-title").append(self_html);
			html.find(".ant-row.oui-row").children("div").eq(3).find(".portrait-title").append(self_html_two);
			html.find(".ant-row.oui-row").children("div").eq(4).find(".portrait-title").append(self_html_three);


			}
	    }	
}















//检测竞品分析是否加载完毕
function detection_jpfx(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage18");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    jz_bool++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==100) {
				clearInterval(ref);
			}

			FaCommonFilter_root_html = $("#itemAnalysisTrend");
			FaCommonFilter_root_html_two = $("#itemAnalysisKeyword");

			if (FaCommonFilter_root_html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage13" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button>	</div>';
			self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="border-radius: 5px;height: 30px;font-size: 14px;"    id="showPage18"   class="  button button-primary button-tiny">指数换算</button>	</div>';
			FaCommonFilter_root_html.find(".oui-card-header").append(self_html);
			FaCommonFilter_root_html_two.find(".oui-card-header").append(self_html_two);

			}
	    }	
}

//检测竞品识别是否加载完毕
function detection_jpsb(ref){

	//检测是否有指数转换按钮
    showPage_common_boj = $(".showPage25");

    if (showPage_common_boj.length>0) {
    	clearInterval(ref);
    }else{

	    scdp_bool++;
	
		//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
		if (scdp_bool==200) {
			clearInterval(ref);
		}

		html = $(".oui-card");

		if (html.length>0) {

			console.log("123");

		//停止定时计划,插入转化按钮
		clearInterval(ref);

		self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;" id="jpsb"  class="button button-primary button-tiny">导出数据</button>        </div>';

		html.find(".oui-card-header").append(self_html);

		}
	}
}












//检测品牌分析是否加载完毕
function detection_ppfx(ref){


		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage20");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    jz_bool++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==100) {
				clearInterval(ref);
			}

			FaCommonFilter_root_html = $("#brandAnalysisTrend");
			FaCommonFilter_root_html_two = $("#brandAnalysisItems");

			if (FaCommonFilter_root_html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage13" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button></div>';
			self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="border-radius: 5px;height: 30px;font-size: 14px;"    id="showPage20"   class="  button button-primary button-tiny">指数换算</button></div>';

			FaCommonFilter_root_html.find(".oui-card-header").append(self_html);
			FaCommonFilter_root_html_two.find(".oui-card-header").append(self_html_two);
// 
			}
	    }	
}




//检测品牌分析-top榜单是否加载完毕
function detection_ppfx_two(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $(".showPage_ppfx");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    jz_bool_two++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool_two==100) {
				clearInterval(ref);
			}

			FaCommonFilter_root_html = $("#brandAnalysisShops");

			if (FaCommonFilter_root_html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage20" style="border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_ppfx   button button-primary button-tiny">指数换算</button></div>';
			// self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="border-radius: 5px;height: 30px;font-size: 14px;"    id="showPage20"   class="  button button-primary button-tiny">指数换算</button></div>';

			FaCommonFilter_root_html.find(".oui-card-header").append(self_html);
			// FaCommonFilter_root_html_two.find(".oui-card-header").append(self_html_two);

			}
	    }	
}









//检测竞品分析-入店来源是否加载完毕
function detection_jpfx_two(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage19");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    jz_bool_two++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool_two==100) {
				clearInterval(ref);
			}

			FaCommonFilter_root_html = $("#sycm-mc-flow-analysis");

			if (FaCommonFilter_root_html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage19" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button><button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;" id="rdly" class="button button-primary button-tiny" data-spm-anchor-id="a21ag.11815280.0.i0.257650a5qzSeuh">导出数据</button></div>';

			FaCommonFilter_root_html.find(".oui-card-header").append(self_html);

			}
	    }	
}













//检测竞店分析是否加载完毕
function detection_jdfx(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage14");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    jz_bool++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==100) {
				clearInterval(ref);
			}

			FaCommonFilter_root_html = $(".op-mc-shop-analysis").children("div").eq(2);
			FaCommonFilter_root_html_two = $(".op-mc-shop-analysis").children("div").eq(3);

			if (FaCommonFilter_root_html.length>0&&FaCommonFilter_root_html_two.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" >  <button  id="showPage13" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button>	</div>';
			self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage14"  style="border-radius: 5px;height: 30px;font-size: 14px;"  class="button button-primary button-tiny">指数换算</button>	<button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;" id="topsp" class="button button-primary button-tiny">导出数据</button></div>';

			FaCommonFilter_root_html.find(".oui-card-header").append(self_html);
			FaCommonFilter_root_html_two.find(".oui-card-header").append(self_html_two);

			}
	    }	
}








//检测竞店分析是否加载完毕2
function detection_jdfx_two(ref){


		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage19");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    jz_bool_two++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool_two==100) {
				clearInterval(ref);
			}

			FaCommonFilter_root_html = $(".op-mc-shop-analysis").find("#sycm-mc-flow-analysis").find(".oui-card-header");

			if (FaCommonFilter_root_html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage19" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button><button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;" id="rdly" class="button button-primary button-tiny" data-spm-anchor-id="a21ag.11815280.0.i0.257650a5qzSeuh">导出数据</button></div>';

			FaCommonFilter_root_html.append(self_html);

			}
	    }	
}



//检测行业客群是否加载完毕
function detection_hykq(ref){

		//检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage7");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

		    ssrq_bool++;
		    //100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (ssrq_bool==100) {
				clearInterval(ref);
			}

			html = $("#completeShopPurchase .mc-Purchase");

			if (html.length>0) {
			//停止定时计划,插入转化按钮

			clearInterval(ref);

			self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="    border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_xykq_common button button-primary button-tiny">指数换算</button></div>';
			self_html_three='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  style="    border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_xykq_common button button-primary button-tiny">指数换算</button></div>';
			// self_html_four='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;"  class="button button-primary button-tiny" id="kqqs">导出数据</button></div>';

			html.children("div").eq(0).find("h4").append(self_html_two);
			html.children("div").eq(1).find("h4").append(self_html_three);
			// $('.oui-card-title').eq(0).append(self_html_four);

			}
	    }	
}




//检测搜索分析是否加载完毕
function detection_ssfx(ref){
	    ssfx_bool++;
	    //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage21");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (ssfx_bool==100) {
				clearInterval(ref);
			}


			showPage_default = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
			showPage_default=Trim(showPage_default);


			if (showPage_default=="概况") {

				html = $("#searchTrend");

				if (html.length>0) {
				//停止定时计划,插入转化按钮
				clearInterval(ref);

				self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage21" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button></div>';
				
				html.find(".oui-card-header").append(self_html);

				}

			}else if(showPage_default=="相关分析"){


					html = $(".oui-card");

					if (html.length>0) {
					//停止定时计划,插入转化按钮
					clearInterval(ref);

					self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button   style="border-radius: 5px;height: 30px;font-size: 14px;" class=" showPage_zh_common  button button-primary button-tiny">指数换算</button></div>';
					
					html.find(".oui-card-header").append(self_html);

					}


			}else if(showPage_default=="类目构成"){

					html = $(".oui-card");

					if (html.length>0) {
						//停止定时计划,插入转化按钮
						clearInterval(ref);

						self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage23"  style="border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button></div>';
						
						self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage24"  style="border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button></div>';
						
						// html.children().eq(0).find(".oui-card-header-wrapper .oui-card-header-wrapper").append(self_html);
						html.children().eq(0).append(self_html);

						html.find(".oui-tab-switch.op-mc-search-analyze-cate-menu").append(self_html_two);
					}
			}
	    }
}


//检测监控商品是否加载完毕
function detection_jksp(ref){


	    jz_bool++;
	    //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage16");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==100) {
				clearInterval(ref);
			}

			html = $("#completeItem");

			if (html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button  id="showPage16" style="border-radius: 5px;height: 30px;font-size: 14px;" class="  button button-primary button-tiny">指数换算</button>	<button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;" id="jksp_export" class="button button-primary button-tiny" data-spm-anchor-id="a21ag.11815245.0.i0.328350a5qxvzqs">导出数据</button></div>';
			html.find(".oui-card-header").append(self_html);

			}
	    }
}










//检测监控店铺是否加载完毕
function detection_jkdp(ref){


	    jz_bool++;
	    //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage12");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==100) {
				clearInterval(ref);
			}

			html = $(".oui-card");

			if (html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

				self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage12" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>         <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;"  class="button button-primary button-tiny" id="export_data8">导出数据</button>           </div>';
				 	html.find(".oui-card-header").append(self_html);

			}
	    }
}











//检测搜索排行是否加载完毕
function detection_ssph(ref){

	    ssph_bool++;
	      //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage4");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (ssph_bool==200) {
				clearInterval(ref);
			}

			html = $(".oui-card");

			if (html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage4" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>    <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;margin-left:8px;height: 30px;font-size: 14px;" id="ssph" class="button button-primary button-tiny">导出数据</button>        </div>';

			html.find(".oui-card-header").append(self_html);

			}
		}
}




//检测竞店识别是否加载完毕
function detection_jdsb(ref){

	    jz_bool++;
	      //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage_zh_common");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==200) {
				clearInterval(ref);
			}

			html = $("#shopRecognitionDrainShopList");

			if (html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button     style="border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_zh_common button button-primary button-tiny">指数换算</button>         <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;margin-left:8px;height: 30px;font-size: 14px;" id="jdsb_export" class="button button-primary button-tiny">导出数据</button>           </div>';
				 
			html.find(".oui-card-header").append(self_html);

			}
		}
}





//检测监控品牌是否加载完毕
function detection_jkpp(ref){

	    jz_bool++;
	      //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage_zh_common");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (jz_bool==200) {
				clearInterval(ref);
			}

			html = $("#completeBrand");

			if (html.length>0) {
			//停止定时计划,插入转化按钮
			clearInterval(ref);

			self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button     style="border-radius: 5px;height: 30px;font-size: 14px;" class="showPage_zh_common button button-primary button-tiny">指数换算</button>         <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;" id="jkpp" class="button button-primary button-tiny">导出数据</button>           </div>';
				 
			html.find(".oui-card-header").append(self_html);

			}
		}
}











//检测产品洞察是否加载完毕
function detection_cpdc(ref){

	    cpdc_bool++;
	      //检测是否有指数转换按钮
	    showPage_common_boj = $("#showPage4");

	    if (showPage_common_boj.length>0) {
	    	clearInterval(ref);
	    }else{

			//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
			if (cpdc_bool==200) {
				clearInterval(ref);
			}

			  showPage_default = $(".ebase-Switch__item.ebase-Switch__activeItem").text();

			  if (showPage_default=="产品分析") {

			  	    html = $("#productTrend");

			  	    FaCommonFilter_root = $(".ebase-FaCommonFilter__root").next();

			  	    if (html.length>0&&FaCommonFilter_root.find(".oui-card").length>0) {
						clearInterval(ref);

					    self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage11" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>        </div>';
						FaCommonFilter_root.children("div").eq(1).find(".oui-card-header").append(self_html);

						self_html_two='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage10" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>         <button id="export_data89" style="      background: #f3d024!important;margin-left: 15px;   border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">导出数据</button>           </div>';

						FaCommonFilter_root.children("div").eq(2).find(".oui-card-header").append(self_html_two);

					}
				}else{

					html = $(".oui-card");

					if (html.length>0) {
						clearInterval(ref);
					}

					self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage10" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>         <button id="export_data89" style="      background: #f3d024!important;margin-left: 15px;   border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">导出数据</button>           </div>';

					$(".oui-card").find(".oui-card-header").append(self_html);

				}
		}
}






//检测监控面板是否加载完毕
function detection_jkmb(ref){

    jkkb_bool++;

     //检测是否有指数转换按钮
	 showPage_common_boj = $("#showPage4");

    if (showPage_common_boj.length>0) {
    	clearInterval(ref);
    }else{

		//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
		if (jkkb_bool==200) {
			clearInterval(ref);
		}


		html_jkmb = $(".mc-marketMonitor");
		cell_title = $(".cell-title");

		if (html_jkmb.length>0&&cell_title.length>0) {
		//停止定时计划,插入转化按钮
		clearInterval(ref);

		self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > ';
		self_html=self_html+'<button id="showPage" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>    <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;" id="jkmb" class="button button-primary button-tiny">导出数据</button>      </div>';


		inserGoodsHelpBtn = '  <span style="margin-right: -18px; margin-left: 8px;  display:inline">店助手:</span>   <div id="inserLoginBtn"   style=" background: red;border: 1px solid red;"   class="canmou_btn orange_color" title="此功能由[店透视]提供">用户信息</div>  <a id="inserGoodsHelpBtn"    class="canmou_btn any_color   " title="此功能由[小鼎智投]提供" data-spm-anchor-id="a21ag.11815228.0.i2.186050a5IgIS8j">任意竞品查询</a>'

		$(".cell-title").append(inserGoodsHelpBtn);


		html_jkmb.find(".oui-card-header").append(self_html);

		}
	}

}  









//检测行业监控是否加载完毕
function detection_xyjk(ref){

    hyjk_bool++;
     //检测是否有指数转换按钮
	 showPage_common_boj = $("#showPage2");

    if (showPage_common_boj.length>0) {
    	clearInterval(ref);
    }else{

		//100秒内没有加载我的监控页面，暂停定时调用，防止定时调用多了浏览器卡顿
		if (hyjk_bool==200) {
			clearInterval(ref);
		}

		html_hyjk = $(".op-mc-market-monitor-industryCard");

		if (html_hyjk.length>0) {
		//停止定时计划,插入转化按钮
		clearInterval(ref);

		self_html='<div title="此功能由[小鼎智投]提供" style="border-radius: 5px;margin-left: 20px;height: 30px;display: inline-block;" > <button id="showPage2" style="    border-radius: 5px;height: 30px;font-size: 14px;" class="button button-primary button-tiny">指数换算</button>    <button style="   background-color: #67c23a !important; border-color: #67c23a !important;    border-radius: 5px;height: 30px;font-size: 14px;margin-left:8px;" id="jkmb2" class="button button-primary button-tiny">导出数据</button>        </div>';

		html_hyjk.find(".oui-card-header").append(self_html);


		}
	}

}  





//去除字符空格
function Trim(str)

{ 

 return str.replace(/(^\s*)|(\s*$)/g, ""); 

}









function createScript(url, cb) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    script.onload = function () {
        script.remove();
        cb && cb();
    };

    (document.head || document.documentElement).appendChild(script);
}
function createCss(url, cb) {
    var script = document.createElement("link");
    script.rel="stylesheet";
    script.href = url;
    script.onload = function () {
        script.remove();
        cb && cb();
    };




    (document.head || document.documentElement).appendChild(script);

}





//请求url
function httpRequest(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            callback(xhr.responseText);
        }
    }
    xhr.send();
}








function detection(){

	if (localStorage.dzs_phone==undefined||localStorage.dzs_phone=="undefined") {
		self_time = Math.round(new Date().getTime()/1000);
		//检测是否登陆登陆
		if (self_time>localStorage.one_time&&self_time-localStorage.one_time>259200) {
			alert("未注册试用3天,请先点击用户信息登陆,该软件完全免费"); 
			return false;
		}
	}
}

