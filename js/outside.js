
   
     ((exp)=>{
        let sycm={
            l:{
                iv: CryptoJS.enc.Utf8.parse("4kYBk36s496zC82w"),
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            },
            u:CryptoJS.enc.Utf8.parse("w28Cz694s63kBYk4"),
            decrypt(data){
                let hex=CryptoJS.enc.Hex.parse(data);
                let base64=CryptoJS.enc.Base64.stringify(hex);
                let val=CryptoJS.AES.decrypt(base64,sycm.u,sycm.l);
                let json=eval('('+val.toString(CryptoJS.enc.Utf8)+')');
                return json;
            },
            encrypt(data){
                if(typeof data==='string')
                    data=JSON.stringify(data);
                return function(e) {
                    return CryptoJS.enc.Hex.stringify(CryptoJS.enc.Base64.parse(e))
                }(CryptoJS.AES.encrypt(JSON.stringify(data), sycm.u, sycm.l).toString())
            },
        };
        exp.sycm=sycm;
    })(this);


url=window.location.href;
exportData_temp=[];
ins1=[];
tab_day_data=[];
is_belong="";
time=Date.parse(new Date());
exportdata=[];
exportdata2=[];

//生意参谋全局变量
if(url.indexOf("https://sycm.taobao.com/mc/mq") >= 0||url.indexOf("https://sycm.taobao.com/mc/ci") >= 0) { 

    //获取 token
    token_content=$("meta[name='microdata']").attr("content");
    lm = token_content.indexOf('token=');
    lm_userId = token_content.indexOf('userId=');
    userId_content_two = token_content.substring(lm_userId+'userId='.length,token_content.length); 
    lm2_userId = userId_content_two.indexOf(';');
    token_content_two = token_content.substring(lm+'token='.length,token_content.length);    
    lm2 = token_content_two.indexOf(';');
    token = token_content_two.substring(0,lm2);
    userId= userId_content_two.substring(0,lm2_userId);

    var e = new JSEncrypt;
    e.setPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJ50kaClQ5XTQfzkHAW9Ehi+iXQKUwVWg1R0SC3uYIlVmneu6AfVPEj6ovMmHa2ucq0qCUlMK+ACUPejzMZbcRAMtDAM+o0XYujcGxJpcc6jHhZGO0QSRK37+i47RbCxcdsUZUB5AS0BAIQOTfRW8XUrrGzmZWtiypu/97lKVpeQIDAQAB");
    transit_id = e.encrypt("w28Cz694s63kBYk4");

    url_cateId="https://sycm.taobao.com/mc/common/getShopCate.json?_=" + (new Date).getTime() + "&token=" + token;
    $.ajax({
      type:'GET',
      url:url_cateId,  
      // data:{_:Date.parse(new Date()),token:token,leaf:"true",edition:"td,pro,vip"},
      dataType:'json',
      beforeSend: function (request) {   
        request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {
        result_data=sycm.decrypt(result["data"]);

        // console.log(result_data);

          cateId=result_data[0][6];
        }
      });


    //获取详细类目
    detail_category=getUrlParam('cateId');
    if (detail_category==null) {
      href = $(".cell-links").find("a").attr("href");
      if (href!=undefined) {
        lm = href.indexOf('cateId=');
        detail_category = href.substring(lm+'cateId='.length,href.length);   
      }
    }

    contendGoodData=[]; //竞争商品变量

  }






    layui.use(['form','laydate','element','layer','table'], function(){
        form = layui.form;
        laydate = layui.laydate;
        element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
        layer = layui.layer; 
        table = layui.table; 

        //监听行工具事件
        table.on('tool(test)', function(obj){
          var data = obj.data;
          if(obj.event === 'particular_day'){

            data_day_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > 竞品-入店来源-分日趋势 </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div id="container2" class="layui-tab-content"></div><div> 当前：30条数据 终端：无线 &nbsp; <button id="export_day" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出</button></div><table  lay-filter="test_day"  id="test_day"  class="layui-hide" ></table></div> </div></div>';

          layer.open({
          type: 1,
          title:false,
          area: ['90%', '90%'],
          shadeClose: true, //点击遮罩关闭
          content: data_day_html
         });

         order=data["order"]-1;
         pageId=tab_day_data[order]["pageId"]["value"];
         pPageId=tab_day_data[order]["pPageId"]["value"];

              // date=$("#date").val();
        source=$("#jpts_source").val();
        good_id=data.good_id;
        // date_type=$("#date_type").val();

        date=getDayTypeTwo(-1);
        date_type=1;

             loadGraph_day(date,source,good_id,date_type,data.page_name,pageId,pPageId);

          }
        });

        //监听Tab切换
        element.on('tab(test_head)', function(){
          
        date=$("#date").val();
        source=$("#jpts_source").val();
        goods_id=$("#good_id").val();
        date_type=$("#date_type").val();
        is_belong=$("#is_belong").val();

        // loadGraph

          if ($(this).text()=="关键指标分析") {
            $("#container").show();
            $("#terrace_parent").remove();
            $("#jpts_source").children().eq(0).removeAttr("disabled");

            loadGraph(date,source,goods_id,date_type,"",is_belong);

          }else if($(this).text()=="来源分析"){

        if (source==0) {
          //表单初始赋值
        form.val("jpts_form", {
          "jpts_source": 2, 
        })
        source=2;
        }

        $("#container").show();
        $("#terrace_parent").remove();
        $("#jpts_source").children().eq(0).attr("disabled","disabled");
        particular_day_html='<script type="text/html" id="barDemo"><a class="layui-btn layui-btn-xs" lay-event="particular_day">分日趋势</a> </script>';
        $("head").append(particular_day_html);

            loadGraph_two(date,source,goods_id,date_type);

          }else if($(this).text()=="关键词分析"){

            if (source==0) {
              //表单初始赋值
            form.val("jpts_form", {
              "jpts_source": 2, 
            })
            source=2;
            }

            $("#container").hide();

             terrace_html = '<div class="layui-form-item" id="terrace_parent"  ><label class="layui-form-label">平台</label><div class="layui-input-block"><select id="terrace"  lay-filter="terrace" lay-verify="terrace"><option class="layui-this"  value="0">淘宝</option><option value="1">天猫</option></select></div> </div>' ;

           if ($("#terrace").length<1) {
            $("#affirm").parent().before(terrace_html);
           }

            $("#jpts_source").children().eq(0).attr("disabled","disabled");
            $(".layui-form.layui-border-box.layui-table-view").html("");

            loadGraph_three(date,source,goods_id,date_type,0);

          }

          form.render("select"); 

        });

            

        //重置时间控件为月份
         form.on('radio(date_type)', function (data) {

                    if (data.value==1) {

                          obj = $("#datepicker").parent();
                          $("#datepicker").remove();

                          html=' <input type="text" name="datepicker" id="datepicker" lay-verify="datepicker" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">';
                          obj.append(html);

                          laydate.render({
                            elem: '#datepicker' //指定元素
                            ,min: -89 
                            ,max: -1 
                            ,value:getDayTypeTwo(-1)
                          });

                    }else if(data.value==2){

                        obj = $("#datepicker").parent();
                        $("#datepicker").remove();

                        html=' <input type="text" name="datepicker" id="datepicker" lay-verify="datepicker" placeholder="yyyy-MM" autocomplete="off" class="layui-input">';
                        obj.append(html);

                        //年月选择器
                        laydate.render({ 
                          elem: '#datepicker'
                          ,type: 'month'
                          ,value:getLastMonth()
                          // ,min: 1 
                        ,max: -1
                          ,change: function(value){
                          $('#datepicker').val(value);
                          $(".laydate-btns-confirm").click();
                          }
                         });
                    }
        });





  });




//查询曲线图
$(document).on('click',"#affirm", function(e) { 

  date=$("#date").val();
  source=$("#jpts_source").val();
  goods_id=$("#good_id").val();
  date_type=$("#date_type").val();
  is_belong=$("#is_belong").val();
   //重载曲线图数据

  test_head_val =  $("#test_head").find(".layui-this").text();

  if (test_head_val=="关键指标分析") {
    // $("#jpts_source").children().eq(0).attr("disabled",true);
    loadGraph(date,source,goods_id,date_type,"",is_belong);
  }else if(test_head_val=="来源分析"){
    // $("#jpts_source").children().eq(0).attr("disabled","disabled");
    loadGraph_two(date,source,goods_id,date_type);
  }else if(test_head_val=="关键词分析"){
     terrace=$("#terrace").val();
    loadGraph_three(date,source,goods_id,date_type,terrace);
  }
   form.render('select'); 
});


// <div>  当前：<span id="tab_count"></span> 条数据 终端：<span id="tab_source"> </span>    &nbsp;   <button   id="export"  style="background-color: #409EFF;"  class="layui-btn layui-btn-sm">导出</button></div>    

//打开弹窗
function openview(goods_id,date_text,source,date_type)
{ 
    //先检测是不是竞争配置中的商品
    isContendGood=2;
    if (contendGoodData.length>0) {
      for (var i = 0; i < contendGoodData.length; i++) {
        if (contendGoodData[i]["itemId"]==goods_id) {
          isContendGood=1;
        }
      }
    }
     

    //检测是不是同类目
    test_head="";
    url_category="https://sycm.taobao.com/mc/ci/config/rival/item/queryItem.json?firstCateId="+detail_category+"&sellerType=-1&rivalType=item&keyword="+goods_id+"&_=" + (new Date).getTime() + "&token=" + token;
    $.ajax({
      type:'GET',
      url:url_category,  
      // data:{_:Date.parse(new Date()),token:token,leaf:"true",edition:"td,pro,vip"},
      dataType:'json',
      beforeSend: function (request) {   
        request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {   
          is_belong=2;
          message = result["data"]["message"];
          if (message=="操作失败，该商品不属于所选一级类目") {
            test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li>   </ul>';

                  //查询一个月隐藏关键词分析
                    if (date_type==2) {
                       test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li><li lay-id="2" data-spm-anchor-id="a21ag.11815339.0.i2.28fa50a5NYn5cX" class="">来源分析</ul>';
                    }else{
                      test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li></ul>';
                    }
                    window_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > 竞品透视数据分析表 </p> </div><div  lay-filter="jpts_form"   class=" layui-form el-dialog__body " > <div style="display:-webkit-box;" ><span >时间</span><div   style="margin-left: 10px;" class="layui-input-inline"><input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">       <input type="text" value='+is_belong+'   id="is_belong"  name="is_belong" lay-verify="is_belong"  style="display: none;" class="layui-input">   <input type="text" value='+goods_id+'   id="good_id"  name="good_id" lay-verify="good_id"  style="display: none;" class="layui-input">  <input type="text" value='+date_type+'   id="date_type"  name="date_type" lay-verify="date_type"  style="display: none;" class="layui-input">     </div><div class="layui-form-item"><label class="layui-form-label">终端</label><div  style="margin-left:80px" class="layui-input-block"><select name="jpts_source" id="jpts_source"  lay-verify="required"><option value="0"> 所有终端 </option><option value="2">无线端</option> <option value="1">PC端</option></select></div></div><div style="margin-left: 20px;" > <button    id="affirm"   class="layui-btn layui-btn-normal layui-btn-radius">确 定</button></div>  <div  style="margin-top: 5px;" >  <span>（选择完点“确定”生效）</span>   </div>    </div>    <div class="layui-tab layui-tab-brief" id="test_head"  lay-filter="test_head">  '+test_head+'  <div  style="width:100%;height:350px"   id="container"  class="layui-tab-content"></div>            <div style="margin-top:10px;" >  当前：<span id="tab_count"></span>条数据 终端：<span id="tab_source"> </span>    &nbsp;   <button   id="export"  style="background-color: #409EFF;"  class="layui-btn layui-btn-sm">导出</button></div>       <table class="layui-hide" id="test" lay-filter="test"></table>  </div> </div></div>';
                     layer.open({
                        type: 1,
                        title:false,
                        area: ['90%', '95%'],
                        shadeClose: true, //点击遮罩关闭
                        content: window_html
                     });

                    l_index = layer.load(1, {
                      shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
                    });

                      laydate.render({
                            elem: '#date' //指定元素
                            ,min: -89 
                              ,max: -1 
                        });

                      //表单初始赋值
                        form.val("jpts_form", {
                          "jpts_source": source, 
                          "date": date_text
                        
                        })

                        //设置为月份选择
                        if (date_type==2) {

                              obj = $("#date").parent();
                                $("#date").remove();

                                html=' <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM" class="layui-input" > ';
                                obj.append(html);

                                //年月选择器
                                laydate.render({ 
                                  elem: '#date'
                                  ,type: 'month'
                                  ,value:date_text
                                   ,min: -3 
                                   ,max: -1 
                                  ,change: function(value){
                                  $('#date').val(value);
                                  $(".laydate-btns-confirm").click();
                                  }
                                 });
                        }



                       date_arr = date_text.split("-");
                       if (date_arr[2]!=undefined) {
                        test_li_html = $("#test_head").find("ul").children().eq(2);
                          test_li_html.show();
                       }

                       form.render();

                      setTimeout(function(){
                         //    //获取曲线图数据
                           loadGraph(date_text,source,goods_id,date_type,l_index,is_belong);
                      },300);

          }else{
              //提示该商品是同类目还没加入监控
              if (isContendGood==2) {
                layer.confirm('添加到竞争配置：该商品添加到[竞争配置]可以分析更多数据，添加后7日内不可取消！',
                { 
                  title:'提示',
                  btn: ['不添加继续操作', '添加到竞争配置'] //可以无限个按钮
                }, function(index, layero){
                  layer.close(index);
                  //查询一个月隐藏关键词分析
                    if (date_type==2) {
                       test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li><li lay-id="2" data-spm-anchor-id="a21ag.11815339.0.i2.28fa50a5NYn5cX" class="">来源分析</ul>';
                    }else{
                      test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li></ul>';
                    }
                    window_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > 竞品透视数据分析表 </p> </div><div  lay-filter="jpts_form"   class=" layui-form el-dialog__body " > <div style="display:-webkit-box;" ><span >时间</span><div   style="margin-left: 10px;" class="layui-input-inline"><input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">       <input type="text" value='+is_belong+'   id="is_belong"  name="is_belong" lay-verify="is_belong"  style="display: none;" class="layui-input">   <input type="text" value='+goods_id+'   id="good_id"  name="good_id" lay-verify="good_id"  style="display: none;" class="layui-input">  <input type="text" value='+date_type+'   id="date_type"  name="date_type" lay-verify="date_type"  style="display: none;" class="layui-input">     </div><div class="layui-form-item"><label class="layui-form-label">终端</label><div  style="margin-left:80px" class="layui-input-block"><select name="jpts_source" id="jpts_source"  lay-verify="required"><option value="0"> 所有终端 </option><option value="2">无线端</option> <option value="1">PC端</option></select></div></div><div style="margin-left: 20px;" > <button    id="affirm"   class="layui-btn layui-btn-normal layui-btn-radius">确 定</button></div>  <div  style="margin-top: 5px;" >  <span>（选择完点“确定”生效）</span>   </div>    </div>    <div class="layui-tab layui-tab-brief" id="test_head"  lay-filter="test_head">  '+test_head+'  <div  style="width:100%;height:350px"   id="container"  class="layui-tab-content"></div>            <div style="margin-top:10px;" >  当前：<span id="tab_count"></span>条数据 终端：<span id="tab_source"> </span>    &nbsp;   <button   id="export"  style="background-color: #409EFF;"  class="layui-btn layui-btn-sm">导出</button></div>       <table class="layui-hide" id="test" lay-filter="test"></table>  </div> </div></div>';
                     layer.open({
                        type: 1,
                        title:false,
                        area: ['90%', '95%'],
                        shadeClose: true, //点击遮罩关闭
                        content: window_html
                     });

                    l_index = layer.load(1, {
                      shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
                    });

                      laydate.render({
                            elem: '#date' //指定元素
                            ,min: -89 
                              ,max: -1 
                        });

                      //表单初始赋值
                        form.val("jpts_form", {
                          "jpts_source": source, 
                          "date": date_text
                        
                        })

                        //设置为月份选择
                        if (date_type==2) {

                              obj = $("#date").parent();
                                $("#date").remove();

                                html=' <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM" class="layui-input" > ';
                                obj.append(html);

                                //年月选择器
                                laydate.render({ 
                                  elem: '#date'
                                  ,type: 'month'
                                  ,value:date_text
                                   ,min: -3 
                                   ,max: -1 
                                  ,change: function(value){
                                  $('#date').val(value);
                                  $(".laydate-btns-confirm").click();
                                  }
                                 });
                        }



                       date_arr = date_text.split("-");
                       if (date_arr[2]!=undefined) {
                        test_li_html = $("#test_head").find("ul").children().eq(2);
                          test_li_html.show();
                       }

                       form.render();

                      setTimeout(function(){
                         //    //获取曲线图数据
                           loadGraph(date_text,source,goods_id,date_type,l_index,is_belong);
                      },300);


                }, function(index){
      
                    url_category="https://sycm.taobao.com/mc/ci/config/rival/item/addToMonitored.json";
                      $.ajax({
                        type:'GET',
                        url:url_category,  
                        data:{_:time,token:token,firstCateId:detail_category,rivalType:"item",itemId:goods_id},
                        dataType:'json',
                        beforeSend: function (request) {   
                          request.setRequestHeader("transit-id",transit_id);
                        },
                        success:function(result)
                        {   
                             message = result["data"]["message"];
                             if (message=="增加监控成功") {
                             
                                    //更新竞争配置的商品数据
                                      $.ajax({
                                              type:'GET',
                                              url:"https://sycm.taobao.com/mc/ci/config/rival/item/getMonitoredList.json",  
                                              data:{_:time,firstCateId:detail_category,token:token},
                                              dataType:'json',
                                              beforeSend: function (request) {   
                                                   request.setRequestHeader("transit-id",transit_id);
                                              },
                                              success:function(result)
                                              {  
                                                contendGoodData=sycm.decrypt(result["data"]);
                                              }
                                       });
                                    layer.msg('增加监控成功', {icon: 1}); 
                                    layer.close(index);
                             }

                        }
                    });
                });
              }else{
                 is_belong=1;

                 // console.log(date_type);

                        //查询一个月隐藏关键词分析
                          if (date_type==2) {
                             test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li><li lay-id="2" data-spm-anchor-id="a21ag.11815339.0.i2.28fa50a5NYn5cX" class="">来源分析</ul>';
                          }else{
                            test_head='<ul class="layui-tab-title"><li class="layui-this" lay-id="1" data-spm-anchor-id="a21ag.11815339.0.i3.28fa50a5NYn5cX">关键指标分析</li><li lay-id="2" data-spm-anchor-id="a21ag.11815339.0.i2.28fa50a5NYn5cX" class="">来源分析</li><li lay-id="3" data-spm-anchor-id="a21ag.11815339.0.i2.28fa50a5NYn5cX" class="">关键词分析</li></ul>';
                          }
                          window_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > 竞品透视数据分析表 </p> </div><div  lay-filter="jpts_form"   class=" layui-form el-dialog__body " > <div style="display:-webkit-box;" ><span >时间</span><div   style="margin-left: 10px;" class="layui-input-inline"><input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">       <input type="text" value='+is_belong+'   id="is_belong"  name="is_belong" lay-verify="is_belong"  style="display: none;" class="layui-input">   <input type="text" value='+goods_id+'   id="good_id"  name="good_id" lay-verify="good_id"  style="display: none;" class="layui-input">  <input type="text" value='+date_type+'   id="date_type"  name="date_type" lay-verify="date_type"  style="display: none;" class="layui-input">     </div><div class="layui-form-item"><label class="layui-form-label">终端</label><div  style="margin-left:80px" class="layui-input-block"><select name="jpts_source" id="jpts_source"  lay-verify="required"><option value="0"> 所有终端 </option><option value="2">无线端</option> <option value="1">PC端</option></select></div></div><div style="margin-left: 20px;" > <button    id="affirm"   class="layui-btn layui-btn-normal layui-btn-radius">确 定</button></div>  <div  style="margin-top: 5px;" >  <span>（选择完点“确定”生效）</span>   </div>    </div>    <div class="layui-tab layui-tab-brief" id="test_head"  lay-filter="test_head">  '+test_head+'  <div  style="width:100%;height:350px"   id="container"  class="layui-tab-content"></div>            <div style="margin-top:10px;" >  当前：<span id="tab_count"></span>条数据 终端：<span id="tab_source"> </span>    &nbsp;   <button   id="export"  style="background-color: #409EFF;"  class="layui-btn layui-btn-sm">导出</button></div>       <table class="layui-hide" id="test" lay-filter="test"></table>  </div> </div></div>';
                           layer.open({
                              type: 1,
                              title:false,
                              area: ['90%', '95%'],
                              shadeClose: true, //点击遮罩关闭
                              content: window_html
                           });

                          l_index = layer.load(1, {
                            shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
                          });

                            laydate.render({
                                  elem: '#date' //指定元素
                                  ,min: -89 
                                    ,max: -1 
                              });

                            //表单初始赋值
                              form.val("jpts_form", {
                                "jpts_source": source, 
                                "date": date_text
                              
                              })

                              //设置为月份选择
                              if (date_type==2) {

                                    obj = $("#date").parent();
                                      $("#date").remove();

                                      html=' <input type="text" name="date" id="date" lay-verify="date" placeholder="yyyy-MM" class="layui-input" > ';
                                      obj.append(html);

                                      //年月选择器
                                      laydate.render({ 
                                        elem: '#date'
                                        ,type: 'month'
                                        ,value:date_text
                                         ,min: -3 
                                         ,max: -1 
                                        ,change: function(value){
                                        $('#date').val(value);
                                        $(".laydate-btns-confirm").click();
                                        }
                                       });
                              }



                             date_arr = date_text.split("-");
                             if (date_arr[2]!=undefined) {
                              test_li_html = $("#test_head").find("ul").children().eq(2);
                                test_li_html.show();
                             }

                             form.render();

                            setTimeout(function(){
                               //    //获取曲线图数据
                                 loadGraph(date_text,source,goods_id,date_type,l_index,is_belong);
                            },300);
              }
          }

       
      }
    });

}






$(document).on('click',"#inserGoodsHelpBtn", function(e) { 
//点击任意竞品查询
popups = '<div id="myModal" class="reveal-modal"><div class="el-dialog__header"><span class="el-dialog__title">竞品透视</span></div><div class="layui-form " lay-filter="jpts_form" ><div class="layui-form-item"><div class="layui-input-block" style="margin-left: 18px; margin-bottom: 10px;"><input type="radio" name="date_type" lay-filter="date_type" value="1" title="天数" checked=""><div class="layui-unselect layui-form-radio layui-form-radioed"><i class="layui-anim layui-icon"></i><div>天数</div></div><input type="radio" name="date_type" lay-filter="date_type" value="2" title="月份"><div class="layui-unselect layui-form-radio"><i class="layui-anim layui-icon"></i><div>月份</div></div></div><div class="layui-form-item"><div class="layui-input-block" style="margin-left: 18px; margin-bottom: 10px;margin-right: 18px; " ><input type="text" name="datepicker" id="datepicker" lay-verify="datepicker" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input"></div></div><div class="layui-form-item" ><div class="layui-input-block" style="margin-left: 18px; margin-bottom: 10px;margin-right: 18px;" ><select id="source" name="source" lay-verify="required"><option value="0"> 所有终端 </option><option class="layui-this" value="2">无线端</option><option value="1">pc端</option></select></div></div><div class="layui-form-item"><div class="layui-input-block" style="margin-left: 18px; margin-bottom: 10px; margin-right: 18px;" ><input type="text" name="tag_good_id" id="tag_good_id" lay-verify="tag_good_id" placeholder="请输入商品的ID或商品链接" autocomplete="off" class="layui-input"></div></div><button style="margin-left: 18px;" id="yjfx_bnt" class="layui-btn layui-btn-sm layui-btn-normal">一键分析</button>      </div></div></div>';

 layer.open({
      type: 1,
      title:false,
      area: ['320px', '319px'],
      shadeClose: true, //点击遮罩关闭
      content: popups 
    });


    //查询竞争配置的商品
    $.ajax({
            type:'GET',
            url:"https://sycm.taobao.com/mc/ci/config/rival/item/getMonitoredList.json",  
            data:{_:time,firstCateId:detail_category,token:token},
            dataType:'json',
            beforeSend: function (request) {   
                 request.setRequestHeader("transit-id",transit_id);
            },
            success:function(result)
            {  
              contendGoodData=sycm.decrypt(result["data"]);
            }

     });


  //执行一个laydate实例
  laydate.render({
     elem: '#datepicker' //指定元素
     ,min: getDayTypeTwo(-90)
   ,max: getDayTypeTwo(-1)
   ,value:getDayTypeTwo(-1)
  });

  //表单初始赋值
    form.val("jpts_form", {
      "source": "2" // "name": "value"
      // ,  "tag_good_id": 558340801616
    })

});




function getLastMonth(){//获取上个月日期
    var date = new Date; 
    var year = date.getFullYear();
    var month = date.getMonth();
    if(month == 0){
         year = year -1;
         month = 12; 
    } 

    return year+"-"+month;
}


function loadGraph_two(date,source,good_id,date_type){    

    gap_day=DateDiff(getDayTypeTwo(-1),date);

 

    

    url='https://sycm.taobao.com/mc/rivalItem/analysis/getFlowSource.json';
    dateType_title="day";
    date_start = date;
    date_end = date;

    if (date_type==1) {
      dateType_title="day";
    }else{
      dateType_title="month";

      var nowdays = new Date();
        var year = nowdays.getFullYear();
        var month = nowdays.getMonth();
        if(month==0)
        {
            month=12;
            year=year-1;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var firstDay = year + "-" + month + "-" + "01";//上个月的第一天

        var myDate = new Date(year, month, 0);
        var lastDay = year + "-" + month + "-" + myDate.getDate();//上个月的最后一天

      date_start=firstDay;
      date_end=lastDay;
    }




    $.ajax({
            type:'GET',
            url:url,  
            data:{_:time,device:source,cateId:cateId,selfItemId:good_id,dateType:dateType_title,dateRange:date_start+'|'+date_end,indexCode:'',token:token,indexCode:"uv",orderBy:"uv",order:"desc"},
            dataType:'json',
            beforeSend: function (request) {   
                 request.setRequestHeader("transit-id",transit_id);
            },
            success:function(result)
            {  

              if (result.hasOwnProperty("data")) 
              {

  
                // data=result["data"];
                data=sycm.decrypt(result["data"]);


                // console.log(data);

              
                    $.ajax({
                      type:'post',
                      url:"https://work.dinghaiec.com/set_save.php",  
                      data:{state:17,data:JSON.stringify(data)},
                      dataType:'json',
                      async: false,
                      beforeSend: function (request) {     
                      },
                      success:function(data)
                      {  

               
                        // rivalItem1PayByrCntIndex 买家数
                        // rivalItem1PayRateIndex   支付转化率
                        // rivalItem1TradeIndex     支付金额


                        tab_day_data=data;



                          tata_title=[];
                          tata_uv=[];
                          tata_PayByrCntIndex=[];
                          tata_PayRateIndex=[];
                          tata_TradeIndex=[];


                          xtime_arr=[];
                          tab_data=[];


                          source_tyitle="所有";

                          if (source==0) {
                            source_tyitle="所有";
                          }else if(source==1){
                            source_tyitle="pc";
                          }else if(source==2){
                            source_tyitle="无线";
                          }


                          $("#tab_count").text(data.length);
                          $("#tab_source").text(source_tyitle);


                            //计算出流量最高的渠道
                            for (var i = 0; i < data.length; i++) {
                                tata_title.push(data[i]["pageName"]["value"]+"");
                                tata_uv.push(parseInt(data[i]["selfItemUv"]["value"]));
                                tata_PayByrCntIndex.push(Math.round(parseInt(data[i]["selfItemPayByrCnt"]["value"])));  //图买家数
                                rivalItem1PayRateIndex=data[i]["selfItemPayRate"]["value"]==null?0:data[i]["selfItemPayRate"]["value"];  //图支付转化率
                                tata_PayRateIndex.push(decimal(rivalItem1PayRateIndex*100,2));
                                tata_TradeIndex.push(parseInt(data[i]["selfItemPayAmt"]["value"]));
                                xtime_arr.push(getDayTypeTwo(-i));
                                
                                temp_arr={};

                                temp_arr["order"]=i+1;
                                temp_arr["day_tendency"]="分日趋势";

                                if (date_type==2) {
                                  str_time=date_start==date_end?date_start:date_start+'|'+date_end;
                                  temp_arr["time"]=str_time;
                                }else{
                                  temp_arr["time"]=date;
                                }

                                
                                temp_arr["page_name"]=data[i]["pageName"]["value"];
                                temp_arr["source"]= source_tyitle;  //来源
                                temp_arr["good_id"]=good_id;
                                temp_arr["uv"]=data[i]["uv"]["value"]; //访客
                                temp_arr["rivalItem1PayByrCntIndex"]=decimal(data[i]["selfItemPayByrCnt"]["value"],0);// 买家数
                                temp_arr["rivalItem1PayRateIndex"]=decimal(rivalItem1PayRateIndex*100,2);   
                                temp_arr["rivalItem1TradeIndex"]= decimal(data[i]["selfItemPayAmt"]["value"],0);  //交易金额
                                temp_arr["price"]=data[i]["selfItemPayByrCnt"]["value"]==0?0:decimal(data[i]["selfItemPayAmt"]["value"]/data[i]["selfItemPayByrCnt"]["value"],2); // 客单价
                                temp_arr["cost_uv"]=decimal(data[i]["selfItemPayAmt"]["value"]/data[i]["selfItemUv"]["value"],2); // uv 价值

                                tab_data.push(temp_arr);
                            }                           


                                  $('#test').html("");


                                          var ins1= table.render({
                                              elem: '#test'
                                              ,data:tab_data
                                              ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                              ,limit:30
                                              ,height:332
                                              ,cols: [[
                                                {field:'order', width:"3%", title: '#'}
                                                ,{field:'day_tendency', width:"7%", title: '分日趋势',toolbar: '#barDemo',}
                                                ,{field:'time', width:"12%", title: '日期', sort: true}
                                                ,{field:'page_name', width:"13%", title: '流量来源'}
                                                ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                                ,{field:'good_id', title: '商品ID',width: "10%" }
                                                ,{field:'uv', title: '访客数', sort: true,width: "10%"}
                                                ,{field:'rivalItem1PayRateIndex', title: '转化率(%)',width: "8%"}
                                                ,{field:'rivalItem1TradeIndex', width: "9%", title: '交易金额', sort: true}
                                                ,{field:'rivalItem1PayByrCntIndex', width: "10%", title: '买家数', sort: true}
                                                ,{field:'price', width: "8%", title: '客单价', sort: true}
                                                ,{field:'cost_uv', width: "5%", title: 'UV价值', sort: true}
                                              ]],
                                             done: function (res, curr, count) {

                                             
                                                exportData_temp=res.data;

                                                
                                                // layer.close(index);

                                            }
                                            });

                                      


                            var chart = Highcharts.chart('container',{
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: ""
                            },
                            xAxis: {
                                categories: tata_title,
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: ''
                                }
                            },
                            tooltip: {
                                // head + 每个 point + footer 拼接成完整的 table
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    borderWidth: 0
                                }
                            },
                            series: [{
                                name: '访客数',
                                data: tata_uv
                            }
                            ,{
                                name: '买家数',
                                data: tata_PayByrCntIndex
                            }
                            , 
                            // {
                            //     name: '支付转化率(%)',
                            //     data: tata_PayRateIndex
                            // }, 
                            
                            {
                                name: '支付金额',
                                data: tata_TradeIndex
                            }
                            ]
                        });
                      }
                   })
          } 
            }
        });

}





function loadGraph_day(date,source,good_id,date_type,page_name,pageId,pPageId){    


    time=Date.parse(new Date());
    
    url='https://sycm.taobao.com/mc/rivalItem/analysis/getSourceTrend.json';
    dateType_title="day";
    date_start = date;
    date_end = date;

    if (date_type==1) {
      dateType_title="day";
    }else{
      dateType_title="month";

      var nowdays = new Date();
        var year = nowdays.getFullYear();
        var month = nowdays.getMonth();
        if(month==0)
        {
            month=12;
            year=year-1;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var firstDay = year + "-" + month + "-" + "01";//上个月的第一天

        var myDate = new Date(year, month, 0);
        var lastDay = year + "-" + month + "-" + myDate.getDate();//上个月的最后一天

      date_start=firstDay;
      date_end=lastDay;
    }






    $.ajax({
            type:'GET',
            url:url,  
            data:{_:time,dateType:dateType_title
              ,dateRange:date_start+'|'+date_end,indexCode:'uv',orderBy:"uv",order:"desc",device:source,cateId:cateId,selfItemId:good_id
              ,rivalItem1Id:''
              ,rivalItem2Id:'',pageId:pageId,pPageId:pPageId ,token:token

            },
            dataType:'json',
            beforeSend: function (request) {   
                 request.setRequestHeader("transit-id",transit_id);
            },
            success:function(result)
            {  

            

              if (result.hasOwnProperty("data")) 
              {

                 data=result["data"];
                 // data=sycm.decrypt(result["data"]);
                
                   $.ajax({
                        type:'post',
                        url:"https://work.dinghaiec.com/set_save.php",  
                        data:{state:19,data:JSON.stringify(data)},
                        dataType:'json',
                        async: false,
                        beforeSend: function (request) {     
                        },
                        success:function(result)
                        {  

                                   // rivalItem1PayByrCntIndex 买家数
                                  // rivalItem1PayRateIndex   支付转化率
                                  // rivalItem1TradeIndex     支付金额

                                        source_tyitle="所有";

                                        if (source==0) {
                                          source_tyitle="所有";
                                        }else if(source==1){
                                          source_tyitle="pc";
                                        }else if(source==2){
                                          source_tyitle="无线";
                                        }


                                        $("#tab_count").text(data.length);
                                        $("#tab_source").text(source_tyitle);

                                        selfItemUv_arr=[];
                                        selfItemTradeIndex_arr=[];
                                        selfItemPayByrCntIndex_arr=[];
                                        rate_lu_arr=[];
                                        price_arr=[];
                                        uv_arr=[];
                                          //计算出流量最高的渠道
                                     

                                            xtime_arr=[];
                                            tab_data=[];


                                          for (var i = 0; i < result["selfItemUv"].length; i++) {

                                               xtime_arr.push(getDayTypeTwo(-result["selfItemUv"].length+i));

                                               selfItemUv_arr.push(result["selfItemUv"][i]);  //访客
                                               selfItemTradeIndex_arr.push(result["selfItemTradeIndex"][i]);  //支付金额
                                               selfItemPayByrCntIndex_arr.push(decimal(result["selfItemPayByrCntIndex"][i][0],0));  //买家数
                                               rate_lu_arr.push(decimal(result["selfItemPayRateIndex"][i][0]*100,2));  //转化率
                                               price_arr.push(decimal((result["selfItemTradeIndex"][i][0]/result["selfItemPayByrCntIndex"][i][0]),2));  //客单价
                                               uv_arr.push(decimal((result["selfItemTradeIndex"][i][0]/result["selfItemUv"][i]),2));   //uv价值
                                                  
                                                temp_arr={};
                                                temp_arr["order"]=i+1;
                                                temp_arr["time"]=getDayTypeTwo(-result["selfItemUv"].length+i);
                                                temp_arr["page_name"]=page_name;
                                                temp_arr["source"]= source_tyitle;  //来源
                                                temp_arr["good_id"]=good_id;
                                                temp_arr["uv"]=result["selfItemUv"][i]; //访客
                                                temp_arr["selfItemPayByrCntIndex"]=decimal(result["selfItemPayByrCntIndex"][i][0],0);// 买家数
                                                temp_arr["rivalItem1PayRateIndex"]=result["selfItemPayByrCntIndex"][i][0]==0?0:decimal(((result["selfItemPayByrCntIndex"][i][0]/result["selfItemUv"][i])*100),2);   
                                                temp_arr["selfItemTradeIndex"]=decimal(result["selfItemTradeIndex"][i],0); 
                                                temp_arr["price"]=result["selfItemPayByrCntIndex"][i][0]==0?0:decimal((result["selfItemTradeIndex"][i][0]/result["selfItemPayByrCntIndex"][i][0]),2); // 客单价
                                                temp_arr["cost_uv"]=decimal((result["selfItemTradeIndex"][i][0]/result["selfItemUv"][i]),2); // uv 价值
                                                temp_arr["cost_uv"]=result["selfItemTradeIndex"][i][0]==0?0:decimal((result["selfItemTradeIndex"][i][0]/result["selfItemUv"][i]),2); // uv 价值

                                                tab_data.push(temp_arr);

                                          }                           

                                                   exportData_day=[];
                                                    var ins_day= table.render({
                                                        elem: '#test_day'
                                                        ,data:tab_data
                                                        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                                        ,limit:30
                                                        ,height:332
                                                        ,cols: [[
                                                          {field:'order', width:"3%", title: '#'}
                                                          ,{field:'time', width:"7%", title: '日期', sort: true}
                                                          ,{field:'page_name', width:"13%", title: '流量来源'}
                                                          ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                                          ,{field:'good_id', title: '商品ID',width: "10%" }
                                                          ,{field:'uv', title: '访客数', sort: true,width: "10%"}
                                                          ,{field:'rivalItem1PayRateIndex', title: '转化率(%)',width: "8%"}
                                                          ,{field:'selfItemTradeIndex', width: "12%", title: '交易金额', sort: true}
                                                          ,{field:'selfItemPayByrCntIndex', width: "12%", title: '买家数', sort: true}
                                                          ,{field:'price', width: "8%", title: '客单价', sort: true}
                                                          ,{field:'cost_uv', width: "12%", title: 'UV价值', sort: true}
                                                        ]],
                                                       done: function (res, curr, count) {
                                                          exportData_day=res.data;
                                                      }
                                                      });

                                                    $("#export_day").click(function(){

                                                  
                                                        table.exportFile(ins_day.config.id,exportData_day,'xls');
                                                     })

                                          //加载曲线图
                                            r_data=[];

                                            uvIndex=[];
                                            uvIndex["name"]="访客数";
                                            uvIndex["data"]=selfItemUv_arr;

                                            tradeIndex=[];
                                            tradeIndex["name"]="交易金额";
                                            tradeIndex["data"]=selfItemTradeIndex_arr;

                                            cartHits=[];
                                            cartHits["name"]="买家数";
                                            cartHits["data"]=selfItemPayByrCntIndex_arr;


                                            payItemCnt=[];
                                            payItemCnt["name"]="转化率(%)";
                                            payItemCnt["data"]=rate_lu_arr;

                                            tradeIndex=[];
                                            tradeIndex["name"]="客单价";
                                            tradeIndex["data"]=price_arr;

                                            seIpvUvHits=[];
                                            seIpvUvHits["name"]="UV价值";
                                            seIpvUvHits["data"]=uv_arr;



                                            r_data[0]=uvIndex;
                                            r_data[1]=tradeIndex;
                                            r_data[2]=payItemCnt;
                                            r_data[3]=tradeIndex;
                                            r_data[4]=seIpvUvHits;
                                            r_data[5]=cartHits;




                                          var chart = Highcharts.chart('container2', {
                                          title: {
                                              text: ''
                                          },
                                          subtitle: {
                                              text: ''
                                          },
                                          yAxis: {
                                              title: {
                                                  text: ""
                                              }
                                          },
                                           xAxis:{
                                              categories:xtime_arr
                                          },
                                          legend: {
                                              layout: 'vertical',
                                              align: 'right',
                                              verticalAlign: 'middle'
                                          },
                                          plotOptions: {
                                              series: {
                                                  lineWidth: 2,
                                                  label: {
                                                      connectorAllowed: false
                                                  }
                                              },
                                               line: {
                                                  dataLabels: {
                                                      enabled: true
                                                  },
                                              }
                                          },
                                          legend: {
                                              itemStyle: {
                                                  color: '#666',
                                                  fontWeight: 'normal'
                                              },
                                              lineHeight: 30
                                          },
                                          series:r_data,
                                          responsive: {
                                              rules: [{
                                                  condition: {
                                                      maxWidth: 500
                                                  },
                                                  chartOptions: {
                                                      legend: {
                                                          layout: 'horizontal',
                                                          align: 'center',
                                                          verticalAlign: 'bottom'
                                                      }
                                                  }
                                              }]
                                          },
                                          tooltip: {
                                              crosshairs: [{
                                                  width: 2,
                                                  color: '#96C6F5',
                                                  dashStyle:'Dot'
                                              }, {
                                                  width: 2,
                                                  color: '#FAA',
                                                  dashStyle:'Dot'
                                              }],
                                                  shared: true
                                              },

                                      });
                        }
                    });

          } 
            }
        });

}










function loadGraph_three(date,source,good_id,date_type,sellerType){    

    gap_day=DateDiff(getDayTypeTwo(-1),date);
    time=Date.parse(new Date());
    
    url='https://sycm.taobao.com/mc/rivalItem/analysis/getKeywords.json';
    dateType_title="day";
    date_start = date;
    date_end = date;

    if (date_type==1) {
      dateType_title="day";
    }else{
      dateType_title="month";

        var nowdays = new Date();
        var year = nowdays.getFullYear();
        var month = nowdays.getMonth();
        if(month==0)
        {
            month=12;
            year=year-1;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var firstDay = year + "-" + month + "-" + "01";//上个月的第一天

        var myDate = new Date(year, month, 0);
        var lastDay = year + "-" + month + "-" + myDate.getDate();//上个月的最后一天

      date_start=firstDay;
      date_end=lastDay;
    }


    $.ajax({
            type:'GET',
            url:url,  
            data:{dateRange:date_start+'|'+date_end,indexCode:'',_:time,dateType:dateType_title,pageSize:"100",page:"1",device:source,cateId:cateId,sellerType:sellerType,itemId:good_id,topType:"flow",indexCode:"uv",token:token},
            dataType:'json',
            beforeSend: function (request) {   
                 request.setRequestHeader("transit-id",transit_id);
            },
            success:function(result)
            {  

              if (result.hasOwnProperty("data")) 
              {
                // data=result["data"];
                data=sycm.decrypt(result["data"]);

                $.ajax({
                        type:'GET',
                        url:url,  
                        data:{dateRange:date_start+'|'+date_end,indexCode:'',_:time,dateType:dateType_title,pageSize:"100",page:"1",device:source,cateId:cateId,
                        sellerType:"0",itemId:good_id,topType:"trade",indexCode:"tradeIndex",token:token},
                        dataType:'json',
                        beforeSend: function (request) {   
                             request.setRequestHeader("transit-id",transit_id);
                        },
                        success:function(result2)
                        {

                            if (result2.hasOwnProperty("data")) 
                          {

                            // data2=result2["data"];
                            data2=sycm.decrypt(result2["data"]);
                          

                            $.ajax({
                                        type:'post',
                                        url:"https://work.dinghaiec.com/set_save.php",  
                                        data:{state:18,data:JSON.stringify(data2)},
                                        dataType:'json',
                                        async: false,
                                        beforeSend: function (request) {     
                                        },
                                        success:function(result)
                                        {  

                                            source_tyitle="所有";

                                            if (source==0) {
                                              source_tyitle="所有";
                                            }else if(source==1){
                                              source_tyitle="pc";
                                            }else if(source==2){
                                              source_tyitle="无线";
                                            }

                                          tab_data=[];
                                          for (var i = 0; i < data.length; i++) {
                                           temp_arr={};
                                           temp_arr["order"]=i+1;


                                           temp_arr["terrace"]=sellerType==0?"淘宝":"天猫";
                                           temp_arr["time"]=date;
                                           temp_arr["source"]=source_tyitle;
                                           temp_arr["good_id"]=good_id;
                                           temp_arr["keyword"]=data[i]["keyword"]["value"];
                                           temp_arr["uv"]=data[i]["uv"]["value"];

                                             for (var j = 0; j < result.length; j++) {

                                                if (data[i]["keyword"]["value"]==result[j]["keyword"]["value"]) {
                                                  temp_arr["tradeIndex"]=decimal(result[j]["tradeIndex"]["value"][0],0);
                                                }
                                             }


                                             if (temp_arr["tradeIndex"]==undefined) {
                                              temp_arr["payrate"]=0;
                                              temp_arr["tradeIndex"]=0;
                                             }else{
                                              temp_arr["payrate"]= decimal((temp_arr["tradeIndex"]/temp_arr["uv"])*100,2);
                                             }

                                            tab_data.push(temp_arr);
                                          }

                                             $("#tab_count").text(tab_data.length);
                                             $("#tab_source").text(source_tyitle);

                                                          var ins1= table.render({
                                                              elem: '#test'
                                                              ,data:tab_data
                                                              ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                                              ,limit:30
                                                              ,height:632
                                                              ,cols: [[
                                                                {field:'order', width:"3%", title: '#'}
                                                                ,{field:'time', width:"7%", title: '日期', sort: true}
                                                                ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 
                                                                ,{field:'terrace', title: '平台', width: "5%", minWidth: 30} 
                                                                ,{field:'good_id', title: '商品ID',width: "10%" }
                                                                ,{field:'keyword', title: '关键词', width: "10%"}
                                                                ,{field:'uv', title: '访客数',width: "20%",sort: true}

                                                                ,{field:'tradeIndex',width: "20%", title: '支付笔数', sort: true}
                                                                ,{field:'payrate',  width: "20%",  title: '支付转化率(%)', sort: true}
                                                              ]],
                                                             done: function (res, curr, count) {
                                                                exportData_temp=res.data;

                                                            }
                                                            });      
                                        }

                                     });
                          }
                        }
                     });
          } 
            }
        });

}




function loadGraph(date,source,good_id,date_type,l_index="",is_belong=""){    


    gap_day=DateDiff(getDayTypeTwo(-1),date);
    time=Date.parse(new Date());
    
    url='https://sycm.taobao.com/mc/rivalItem/analysis/getCoreTrend.json';
    dateType_title="day";
    date_start = date;
    date_end = date;

    if (date_type==1) {
      dateType_title="day";
    }else{
      dateType_title="month";

      var nowdays = new Date();
        var year = nowdays.getFullYear();
        var month = nowdays.getMonth();
        if(month==0)
        {
            month=12;
            year=year-1;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var firstDay = year + "-" + month + "-" + "01";//上个月的第一天

        var myDate = new Date(year, month, 0);
        var lastDay = year + "-" + month + "-" + myDate.getDate();//上个月的最后一天

      date_start=firstDay;
      date_end=lastDay;
    }

     // console.log(is_belong,123);

        if (is_belong==2) {

            url="https://sycm.taobao.com/mc/ci/item/trend.json";

            $.ajax({
              type:'GET',
              url:url,  
              data:{_:time,dateType:dateType_title,dateRange:date_start+'|'+date_end,indexCode:'uvIndex,payRateIndex,tradeIndex,payByrCntIndex',device:source,
              cateId:cateId,itemId:good_id,sellerType:'-1',token:token},
              dataType:'json',
              beforeSend: function (request) {   
                   request.setRequestHeader("transit-id",transit_id);
              },
              success:function(result)
              {

                    if (l_index) {

                      layer.close(l_index);

                    }

                    if (result.hasOwnProperty("data")) 
                    {
                           // data=result["data"];
                           data=sycm.decrypt(result["data"]);



                             $.ajax({
                                      type:'post',
                                      url:"https://work.dinghaiec.com/set_save.php",  
                                      data:{state:24,data:JSON.stringify(data)},
                                      dataType:'json',
                                      async: false,
                                      beforeSend: function (request) {     
                                      },
                                      success:function(data)
                                      {   
                                        //加载曲线图
                                        r_data=[];

                                        uvIndex=[];
                                        uvIndex["name"]="访客数";
                                        uvIndex["data"]=data["uvIndex"];

                                        tradeIndex=[];
                                        tradeIndex["name"]="交易金额";
                                        tradeIndex["data"]=data["tradeIndex"];


                                        // payItemCnt=[];
                                        // payItemCnt["name"]="支付件数";
                                        // payItemCnt["data"]=data["payItemCnt"];

                                        price_arr=[];
                                        price_arr["name"]="客单价";
                                        price_arr["data"]=data["price"];

                                        // seIpvUvHits=[];
                                        // seIpvUvHits["name"]="搜索人数";
                                        // seIpvUvHits["data"]=data["seIpvUvHits"];

                                        cartHits=[];
                                        cartHits["name"]="买家数";
                                        cartHits["data"]=data["buyer_number"];

                                        // console.log(data["buyer_number"]);

                                        r_data[0]=uvIndex;
                                        r_data[1]=price_arr;
                                        // r_data[2]=payItemCnt;
                                        r_data[2]=tradeIndex;
                                        // r_data[4]=seIpvUvHits;
                                        r_data[3]=cartHits;




                                        xtime_arr=[];

                                        tab_data=[];

                                        if (source==0) {
                                          source_tyitle="所有";
                                        }else if(source==1){
                                          source_tyitle="pc";
                                        }else if(source==2){
                                          source_tyitle="无线";
                                        }


                                        $("#tab_count").text(data["uvIndex"].length);
                                        $("#tab_source").text(source_tyitle);


                                        j=0;
                                        zhl_number_temp=[];
                                        for (var i = data["uvIndex"].length ; i >= 1; i--) {

                                          gap_day_two=gap_day+i;
                                          temp_arr={};

                                          if (date_type==2) {
                                             xtime_arr.push(getSelfMonth(date,i-1));
                                             temp_arr["time"]=getSelfMonth(date,i-1);
                                          }else{
                                             xtime_arr.push(getDayTypeTwo(-gap_day_two));
                                             temp_arr["time"]=getDayTypeTwo(-gap_day_two);
                                          }

                                          temp_arr["terminal"]=source_tyitle;
                                          temp_arr["goods_id"]=good_id;
                                          temp_arr["uvIndex"]=uvIndex["data"][j];
                                          temp_arr["tradeIndex"]= decimal(data["tradeIndex"][j],0);  //交易金额
                                          // temp_arr["payItemCnt"]=payItemCnt["data"][j];
                                          temp_arr["payRate"]= data["change_lu"][j]==undefined?0:decimal(data["change_lu"][j]*100,2);//转化率
                                          zhl_number_temp.push(data["change_lu"][j]==undefined?0:decimal(data["change_lu"][j]*100,2)) //转化率

                                          temp_arr["price"]=data["price"][j];//客单价  
                                          // temp_arr["seIpvUvHits"]=seIpvUvHits["data"][j];   //搜索人数
                                          // temp_arr["cltHits"]=data["cltHits"][j]; //收藏人数
                                          // temp_arr["cartHits"]=data["cartHits"][j]; //加购人数
                                          // temp_arr["cltHits"]=data["cltHits"][j]; //收藏人数
                                          // temp_arr["cartHits"]=data["cartHits"][j]; //加购人数
                                          temp_arr["uvcost"]=data["uvcost"][j]; // uv价值
                                          temp_arr["buyer_number"]=data["buyer_number"][j]; // 买家数
                                          // temp_arr["cltHits_lu"]=data["cltHits_lu"][j]; // 收藏率
                                          // temp_arr["cartHits_lv"]=data["cartHits_lv"][j]; // 加购率
                                          // temp_arr["search_lv"]=data["search"][j]; // 搜索占比

                                          tab_data.push(temp_arr);

                                          j++;
                                        }

                                        zhl_number=[];
                                        zhl_number["name"]="转化率(%)";
                                        zhl_number["data"]=zhl_number_temp;
                                        r_data[4]=zhl_number;




                                        tab_data_all={};
                                        tab_data_all["data"]=tab_data;

                                        left_title="";

                                      var chart = Highcharts.chart('container', {
                                                title: {
                                                    text: ''
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: left_title
                                                    }
                                                },
                                                 xAxis:{
                                                    categories:xtime_arr
                                                },
                                                legend: {
                                                    layout: 'vertical',
                                                    align: 'right',
                                                    verticalAlign: 'middle'
                                                },
                                                plotOptions: {
                                                    series: {
                                                        lineWidth: 2,
                                                        label: {
                                                            connectorAllowed: false
                                                        }
                                                    },
                                                     line: {
                                                        dataLabels: {
                                                            enabled: true
                                                        },
                                                    }
                                                },
                                                legend: {
                                                    itemStyle: {
                                                        color: '#666',
                                                        fontWeight: 'normal'
                                                    },
                                                    lineHeight: 30
                                                },
                                                series:r_data,
                                                responsive: {
                                                    rules: [{
                                                        condition: {
                                                            maxWidth: 500
                                                        },
                                                        chartOptions: {
                                                            legend: {
                                                                layout: 'horizontal',
                                                                align: 'center',
                                                                verticalAlign: 'bottom'
                                                            }
                                                        }
                                                    }]
                                                },
                                                tooltip: {
                                                    crosshairs: [{
                                                        width: 2,
                                                        color: '#96C6F5',
                                                        dashStyle:'Dot'
                                                    }, {
                                                        width: 2,
                                                        color: '#FAA',
                                                        dashStyle:'Dot'
                                                    }],
                                                        shared: true
                                                    },

                                            });

                                                
                                                      var ins1= table.render({
                                                          elem: '#test'
                                                          ,data:tab_data
                                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                                          ,limit:30
                                                          ,height:332
                                                          ,cols: [[
                                                            {field:'time', width:106, title: '日期', sort: true}
                                                            ,{field:'terminal', width:80, title: '终端'}
                                                            ,{field:'goods_id', width:180, title: '商品id', sort: true}
                                                            ,{field:'uvIndex', width:180, title: '访客数'}
                                                            ,{field:'tradeIndex', title: '交易金额', width: 100, minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                                            ,{field:'payRate', title: '转化率(%)', sort: true}
                                                            ,{field:'price', title: '客单价'}
                                                            ,{field:'uvcost', width:180, title: 'UV价值', sort: true}
                                                            ,{field:'buyer_number', width:180, title: '买家数', sort: true}
                                      
                                                          ]],
                                                           done: function (res, curr, count) {
                                                              exportData_temp=res.data;
                                                          }
                                                        });


                                                   $("#export").click(function(){
                                                      table.exportFile(ins1.config.id,exportData_temp,'xls');
                                                   })
                                  }
                              });
                     }else{
                                if (result["message"]=="用户无权限") {
                                  layer.msg("该商品是同类目商品，请现在竞争配置里面配置之后再来查询哦", {icon: 2,  time:3500,});
                                }else{
                                  layer.msg(result["message"], {icon: 2,  time:2500,});
                                }
                     }








              }
            });


        }else{


            $.ajax({
              type:'GET',
              url:url,  
              data:{_:time,dateType:dateType_title,dateRange:date_start+'|'+date_end,indexCode:'',device:source,cateId:cateId,selfItemId:'',rivalItem1Id:good_id,rivalItem2Id:'',token:token},
              dataType:'json',
              beforeSend: function (request) {   
                   request.setRequestHeader("transit-id",transit_id);
              },
              success:function(result)
              {  

                    if (l_index) {

                      layer.close(l_index);

                    }

                    if (result.hasOwnProperty("data")) 
                    {
                           // data=result["data"];
                           data=sycm.decrypt(result["data"]);




                             $.ajax({
                                      type:'post',
                                      url:"https://work.dinghaiec.com/set_save.php",  
                                      data:{state:16,data:JSON.stringify(data)},
                                      dataType:'json',
                                      async: false,
                                      beforeSend: function (request) {     
                                      },
                                      success:function(data)
                                      {   
                                        //加载曲线图
                                        r_data=[];

                                        uvIndex=[];
                                        uvIndex["name"]="访客数";
                                        uvIndex["data"]=data["uvIndex"];

                                        tradeIndex=[];
                                        tradeIndex["name"]="交易金额";
                                        tradeIndex["data"]=data["tradeIndex"];


                                        payItemCnt=[];
                                        payItemCnt["name"]="支付件数";
                                        payItemCnt["data"]=data["payItemCnt"];

                                        tradeIndex=[];
                                        tradeIndex["name"]="客单价";
                                        tradeIndex["data"]=data["price"];

                                        seIpvUvHits=[];
                                        seIpvUvHits["name"]="搜索人数";
                                        seIpvUvHits["data"]=data["seIpvUvHits"];

                                        cartHits=[];
                                        cartHits["name"]="买家数";
                                        cartHits["data"]=data["buyer_number"];



                                        r_data[0]=uvIndex;
                                        r_data[1]=tradeIndex;
                                        r_data[2]=payItemCnt;
                                        r_data[3]=tradeIndex;
                                        r_data[4]=seIpvUvHits;
                                        r_data[5]=cartHits;


                                        xtime_arr=[];

                                        tab_data=[];

                                        if (source==0) {
                                          source_tyitle="所有";
                                        }else if(source==1){
                                          source_tyitle="pc";
                                        }else if(source==2){
                                          source_tyitle="无线";
                                        }


                                        $("#tab_count").text(data["uvIndex"].length);
                                        $("#tab_source").text(source_tyitle);


                                        j=0;
                                        for (var i = data["uvIndex"].length ; i >= 1; i--) {

                                          gap_day_two=gap_day+i;
                                          temp_arr={};

                                          if (date_type==2) {
                                             xtime_arr.push(getSelfMonth(date,i-1));
                                             temp_arr["time"]=getSelfMonth(date,i-1);
                                          }else{
                                             xtime_arr.push(getDayTypeTwo(-gap_day_two));
                                             temp_arr["time"]=getDayTypeTwo(-gap_day_two);
                                          }

                                          temp_arr["terminal"]=source_tyitle;
                                          temp_arr["goods_id"]=good_id;
                                          temp_arr["uvIndex"]=uvIndex["data"][j];
                                          temp_arr["tradeIndex"]= decimal(data["tradeIndex"][j],0);  //交易金额
                                          temp_arr["payItemCnt"]=payItemCnt["data"][j];
                                          temp_arr["payRate"]= data["change_lu"][j]==undefined?0:decimal(data["change_lu"][j]*100,2);//转化率
                                          temp_arr["price"]=data["price"][j];//客单价  
                                          temp_arr["seIpvUvHits"]=seIpvUvHits["data"][j];   //搜索人数
                                          temp_arr["cltHits"]=data["cltHits"][j]; //收藏人数
                                          temp_arr["cartHits"]=data["cartHits"][j]; //加购人数
                                          temp_arr["cltHits"]=data["cltHits"][j]; //收藏人数
                                          temp_arr["cartHits"]=data["cartHits"][j]; //加购人数
                                          temp_arr["uvcost"]=data["uvcost"][j]; // uv价值
                                          temp_arr["buyer_number"]=data["buyer_number"][j]; // 买家数
                                          temp_arr["cltHits_lu"]=data["cltHits_lu"][j]; // 收藏率
                                          temp_arr["cartHits_lv"]=data["cartHits_lv"][j]; // 加购率
                                          temp_arr["search_lv"]=data["search"][j]; // 搜索占比

                                          tab_data.push(temp_arr);

                                          j++;
                                        }





                                        tab_data_all={};
                                        tab_data_all["data"]=tab_data;

                                        left_title="";

                                      var chart = Highcharts.chart('container', {
                                                title: {
                                                    text: ''
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: left_title
                                                    }
                                                },
                                                 xAxis:{
                                                    categories:xtime_arr
                                                },
                                                legend: {
                                                    layout: 'vertical',
                                                    align: 'right',
                                                    verticalAlign: 'middle'
                                                },
                                                plotOptions: {
                                                    series: {
                                                        lineWidth: 2,
                                                        label: {
                                                            connectorAllowed: false
                                                        }
                                                    },
                                                     line: {
                                                        dataLabels: {
                                                            enabled: true
                                                        },
                                                    }
                                                },
                                                legend: {
                                                    itemStyle: {
                                                        color: '#666',
                                                        fontWeight: 'normal'
                                                    },
                                                    lineHeight: 30
                                                },
                                                series:r_data,
                                                responsive: {
                                                    rules: [{
                                                        condition: {
                                                            maxWidth: 500
                                                        },
                                                        chartOptions: {
                                                            legend: {
                                                                layout: 'horizontal',
                                                                align: 'center',
                                                                verticalAlign: 'bottom'
                                                            }
                                                        }
                                                    }]
                                                },
                                                tooltip: {
                                                    crosshairs: [{
                                                        width: 2,
                                                        color: '#96C6F5',
                                                        dashStyle:'Dot'
                                                    }, {
                                                        width: 2,
                                                        color: '#FAA',
                                                        dashStyle:'Dot'
                                                    }],
                                                        shared: true
                                                    },

                                            });

                                                
                                                      var ins1= table.render({
                                                          elem: '#test'
                                                          ,data:tab_data
                                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                                          ,limit:30
                                                          ,height:332
                                                          ,cols: [[
                                                            {field:'time', width:106, title: '日期', sort: true}
                                                            ,{field:'terminal', width:80, title: '终端'}
                                                            ,{field:'goods_id', width:130, title: '商品id', sort: true}
                                                            ,{field:'uvIndex', width:80, title: '访客数'}
                                                            ,{field:'tradeIndex', title: '交易金额', width: 100, minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                                            ,{field:'payItemCnt', title: '支付件数',width: 100, sort: true}
                                                            ,{field:'payRate', title: '转化率(%)', sort: true}
                                                            ,{field:'price', title: '客单价'}
                                                            ,{field:'seIpvUvHits', width:100, title: '搜索人数', sort: true}
                                                            ,{field:'cltHits', width:100, title: '收藏人数', sort: true}
                                                            ,{field:'cartHits', width:100, title: '加购人数', sort: true}
                                                            
                                                            ,{field:'uvcost', width:100, title: 'UV价值', sort: true}
                                                            ,{field:'buyer_number', width:100, title: '买家数', sort: true}
                                                            ,{field:'search_lv', width:120, title: '搜索占比(%)', sort: true}
                                                            ,{field:'cltHits_lu', width:100, title: '收藏率(%)', sort: true}
                                                            ,{field:'cartHits_lv', width:100, title: '加购率(%)', sort: true}
                                                          ]],
                                                           done: function (res, curr, count) {

                                                              exportData_temp=res.data;

                                                          }
                                                        });


                                                   $("#export").click(function(){
                                                      table.exportFile(ins1.config.id,exportData_temp,'xls');
                                                   })
                                  }
                              });
                    }else
                    {
                                if (result["message"]=="用户无权限") {
                                  layer.msg("该商品是同类目商品，请现在竞争配置里面配置之后再来查询哦", {icon: 2,  time:3500,});
                                }else{
                                  layer.msg(result["message"], {icon: 2,  time:2500,});
                                }
                     }


              }
            });
        }




     



    
}






//计算两个日期天数差的函数，通用
function DateDiff(sDate1, sDate2) {  //sDate1和sDate2是yyyy-MM-dd格式

    var aDate, oDate1, oDate2, iDays;
    aDate = sDate1.split("-");
    oDate1 = new Date(aDate[1] + '-' + aDate[2] + '-' + aDate[0]);  //转换为yyyy-MM-dd格式
    aDate = sDate2.split("-");
    oDate2 = new Date(aDate[1] + '-' + aDate[2] + '-' + aDate[0]);
    iDays = parseInt(Math.abs(oDate1 - oDate2) / 1000 / 60 / 60 / 24); //把相差的毫秒数转换为天数
  
    return iDays;  //返回相差天数
}




$(document).on('click',"#yjfx_bnt", function(e) { 

  date_text = $("#datepicker").val();

  goods_id = $("#tag_good_id").val();

  tag_source =$('#source') .val();//选中的值

  date_type = $("input[name=date_type]:checked").val();

  if (date_text=="") {
    layer.msg("请选择日期", {icon: 2,  time:1500,});
  }

     if(goods_id.indexOf("id") >= 0) {

               position=goods_id.indexOf("id");
               newgoods_id=goods_id.substring(position+3,goods_id.length);

               if (newgoods_id.indexOf("&") >= 0) {
                newgoods_id=newgoods_id.substring(0,newgoods_id.indexOf("&"));
               }

               openview(newgoods_id,date_text,tag_source,date_type);

      }else{

        if (goods_id=="") {

  
        layer.msg("请输入商品链接或id", {icon: 2,  time:1500,});

        }else{

          if (isNaN(goods_id)) {

            layer.msg("商品id必须是数字", {icon: 2,  time:1500,});

          }else{
             openview(goods_id,date_text,tag_source,date_type);
          }

        }
      }

});



$(document).on('click','#export_data89',function(e){

  layer.msg("该功能正在开发中");

});



//监控面板
$(document).on('click','#jkmb',function(e){
  //监控看板导出
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }



  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = $(".oui-card-link").find("a").attr("href");

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);

  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();

  item_type2 = ($(".oui-tab-switch-item.oui-tab-switch-item-active.default").text()).substring(0, 2);
  id="jkmbExport1";
  orderBy="orderBy";
  if (item_type2=="店铺") {
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/live/ci/shop/monitor/list.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex,uvIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/ci/shop/monitor/list.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex,uvIndex",token:token};
    }
  }else if(item_type2=="商品"){
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/live/ci/item/monitor/list.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex,uvIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/ci/item/monitor/list.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex,uvIndex",token:token};
    }
  }else if(item_type2=="品牌"){
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/live/ci/brand/monitor/list.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/ci/brand/monitor/list.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex,uvIndex",token:token};
    }
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+'-'+item_type2+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if (dateType=="today"){
          exportdata=exportdata1['data']['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{  
          exportdata=exportdata1['data'];
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }

        // console.log(exportdata);
        // return false;
        //利用冒泡排序进行排序按照店铺排名
        var len = exportdata.length;  
        for(var i=0;i<len;i++){
            for(var j=i;j<len;j++){
            if(exportdata[i]>exportdata[j]){
              var tmp=exportdata[i];
              exportdata[i]=exportdata[j];
              exportdata[j]=tmp;
            }
          }
        }



        $("#device_title").text(len);
        if(item_type2=="品牌"&&dateType=='today'){
 
          arr_all =formattingData(exportdata,["tradeIndex"],''); 
        }else{
          arr_all =formattingData(exportdata,["seIpvUvHits","tradeIndex","uvIndex","cltHits","cartHits"],"payRateIndex"); 
        }
        // console.log(exportdata,arr_all);
        
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(item_type2=="店铺"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["page_name"]=sellerType_title;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["shop_title"]=data[i]["shop"]["title"];
                  temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                  temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                  temp_arr["id"]=c++;// 序号
                  temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

                  temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                  temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
                  temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

                  temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                  temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                  temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                  temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                  temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                  if(temp_arr["payNum"]==0){
                    temp_arr["kdj"]=0;
                  }else{
                    temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                  }
                  temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                }else if(item_type2=="商品"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["page_name"]=sellerType_title;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["shop_title"]=data[i]["item"]["title"];
                  temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
                  temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
                  temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                  temp_arr["id"]=c++;// 序号
                  temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

                  temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                  temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
                  temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

                  // console.log(data2);

                  temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                  temp_arr["searchProportion"]= data2["uvIndex"][i]["0"]==0?0:decimal((temp_arr["seIpvUvHits"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                  temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额


                  temp_arr["searchCltHits"]= data2["uvIndex"][i]["0"]==0?0:decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                  temp_arr["searchCartHits"]=data2["uvIndex"][i]["0"]==0?0:decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                  temp_arr["uvCost"]=data2["uvIndex"][i]["0"]==0?0:decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                  if(temp_arr["payNum"]==0){
                    temp_arr["kdj"]=0;
                  }else{
                    temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                  }
                  temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                }else if(item_type2=="品牌"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                    temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                    temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID
                    temp_arr["id"]=c++;// 序号
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                    temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                    temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID
                    temp_arr["id"]=c++;// 序号
                    temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

                    temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                    temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
                    temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

                    temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                    temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                    temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                    temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                    if(temp_arr["payNum"]==0){
                      temp_arr["kdj"]=0;
                    }else{
                      temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                    }
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }
                tab_data.push(temp_arr);
              }                           
  
              exportData=[];
              if(item_type2=="店铺"){
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控面板-店铺'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'page_name', width:"4%", title: '平台'}
                      ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'shop_title', title: '店铺名称',width: "8%" }
                      ,{field: 'pictureUrl', width: "5%", title: '店铺图片',  sort: false,templet:function (d) { 
                        if (d.pictureUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'shop_userId', title: '店铺ID', sort: true,width: "4%"}
                      ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                      ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                      ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                      ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                      ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                      ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                      ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                      ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                      ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                      ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                      ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                      ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    // initSort: {
                    //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
                    //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                    // },
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                });
              }else if(item_type2=="商品"){
                var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '监控面板-商品'
                  ,data:tab_data
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                    {field:'order', width:"2%", title: '#' ,sort: false}
                    ,{field:'time', width:"5%", title: '日期', sort: true}
                    ,{field:'page_name', width:"4%", title: '平台'}
                    ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'shop_title', title: '商品名称',width: "8%",templet:function (d) { 
                        return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
                      }}

                    ,{field: 'pictUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
                      if (d.pictUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'itemId', title: '商品ID', sort: true,width: "4%"}
                    ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                    ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                    ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                    ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                    ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                    ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                    ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                    ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                    ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                    ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                    ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                    ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  // initSort: {
                  //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
                  //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                  // },
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
                });
              }else if(item_type2=="品牌"){
                if(dateType=='today'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控面板-品牌'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"10%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"10%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'brandName', title: '品牌名称',width: "15%"}
                      ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
                        if (d.logo!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'brandId', title: '品牌ID', sort: true,width: "15%"}
                      ,{field:'tradeIndex', width: "15%", title: '交易金额', sort: true}
                      
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控面板-品牌'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'page_name', width:"4%", title: '平台'}
                      ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'brandName', title: '品牌名称',width: "8%"}
                      ,{field: 'logo', width: "5%", title: '品牌LOGO',  sort: false,templet:function (d) { 
                        if (d.logo!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'brandId', title: '品牌ID', sort: true,width: "4%"}
                      ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                      ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                      ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                      ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                      ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                      ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                      ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                      ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                      ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                      ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                      ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                      ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//监控面板2
$(document).on('click','#jkmb2',function(e){
  //监控看板导出
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }

  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = $(".oui-card-link").find("a").attr("href");

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);

  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  // item_type2 = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();
  var short_name=$('.short-name').text();//类目名称
  id="jkmbExport2";
  orderBy="orderBy";
  item_type2 = ($(".oui-tab-switch-item.oui-tab-switch-item-active.default").text()).substring(($(".oui-tab-switch-item.oui-tab-switch-item-active.default").text()).length-4);
  if (item_type2=="热门店铺") {
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/mq/monitor/cate/live/showTopShops.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"tradeIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/mq/monitor/cate/offline/showTopShops.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"tradeIndex,tradeGrowthRange",token:token};
    }
  }else if(item_type2=="热门商品"){
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/mq/monitor/cate/live/showTopItems.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"tradeIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/mq/monitor/cate/offline/showTopItems.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"tradeIndex,tradeGrowthRange",token:token};
    }
  }else if(item_type2=="热门品牌"){
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/mq/monitor/cate/live/showTopBrands.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"tradeIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/mq/monitor/cate/offline/showTopBrands.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"tradeIndex,tradeGrowthRange",token:token};
    }
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+'-'+item_type2+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> <button  id="jkmbExport2" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if (dateType=="today"){
          exportdata=exportdata1['data']['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{  
          exportdata=exportdata1['data'];
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }

        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
         for(var i=0;i<len;i++){
            for(var j=i;j<len;j++){
            if(exportdata[i]>exportdata[j]){
              var tmp=exportdata[i];
              exportdata[i]=exportdata[j];
              exportdata[j]=tmp;
            }
          }
        }
          // for (var i = 0; i < len; i++) {  
          //     for(var j = 0; j < len - i -1; j++){  
          //         if(exportdata[j]["cateRankId"]["value"] >exportdata[j+1]["cateRankId"]["value"]){  //相邻元素进行对比  
          //             var temp = exportdata[j+1];//交换元素  
          //             exportdata[j+1] = exportdata[j];  
          //             exportdata[j] = temp;  
          //         }  
          //     }  
          // }   
        $("#device_title").text(len);
        if(dateType=='today'){
          arr_all =formattingData(exportdata,["tradeIndex"],'');
        }else{
          arr_all =formattingData(exportdata,["tradeIndex"],'payRateIndex');
        }
        
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(item_type2=="热门店铺"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["shop"]["title"];
                    temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                    temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["shop"]["title"];
                    temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                    temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }else if(item_type2=="热门商品"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["item"]["title"];
                    temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
                    temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
                    temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["item"]["title"];
                    temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
                    temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
                    temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }else if(item_type2=="热门品牌"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                    temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                    temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID

                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=now_date;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                    temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                    temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID

                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }
                tab_data.push(temp_arr);
              }                           
  
              exportData=[];
              if(item_type2=="热门店铺"){
                if(dateType=='today'){
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门店铺'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'page_name', width:"8%", title: '平台'}
                        ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
                        ,{field:'shop_title', title: '店铺名称',width: "10%" }
                        ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'shop_userId', title: '店铺ID', sort: true,width: "10%"}
                        ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
                        ,{field:'cateRankId', width: "7%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }else{
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门店铺'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'page_name', width:"6%", title: '平台'}
                        ,{field:'source', title: '终端', width: "12%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
                        ,{field:'shop_title', title: '店铺名称',width: "10%" }
                        ,{field: 'pictureUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'shop_userId', title: '店铺ID', sort: true,width: "5%"}
                        ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
                        ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
                        ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
                        ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }else if(item_type2=="热门商品"){
                if(dateType=='today'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '行业监控-热门商品'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"8%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
                      ,{field:'shop_title', title: '商品名称',width: "12%",templet:function (d) { 
                        return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
                      }}
                      ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                        if (d.pictUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'itemId', title: '商品ID', sort: true,width: "10%"}
                      ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
                      ,{field:'cateRankId', width: "8%", title: '行业排名', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门商品'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"13%", title: '日期', sort: true}
                        ,{field:'page_name', width:"6%", title: '平台'}
                        ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
                        ,{field:'shop_title', title: '商品名称',width: "12%",templet:function (d) { 
                          return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
                        }}
                        ,{field: 'pictUrl', width: "8%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.pictUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'itemId', title: '商品ID', sort: true,width: "5%"}
                        ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
                        ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
                        ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
                        ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }else if(item_type2=="热门品牌"){
                if(dateType=='today'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '行业监控-热门商品'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"8%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
                      ,{field:'brandName', title: '品牌名称',width: "12%"}
                      ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
                        if (d.logo!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'brandId', title: '商品ID', sort: true,width: "10%"}
                      ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
                      ,{field:'cateRankId', width: "8%", title: '行业排名', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门品牌'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"10%", title: '日期', sort: true}
                        ,{field:'page_name', width:"6%", title: '平台'}
                        ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
                        ,{field:'brandName', title: '品牌名称',width: "12%"}
                        ,{field: 'logo', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.logo!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'brandId', title: '商品ID', sort: true,width: "10%"}
                        ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
                        ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
                        ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
                        ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//搜索排行
$(document).on('click','#ssph',function(e){
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();
  
  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();
  dateType="day";
  if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }
  device_title = $(".oui-select-container-value").text();
  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }
  sellerType_title = $(".ebase-Switch__item.ebase-Switch__activeItem").text();//搜索词类别
  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();//飙升、热搜
  //请求数据接口
  //打开导出的表格
    
  // time_chinese_type =  $(".oui-card-link").text();
  // href = $(".oui-card-link").find("a").attr("href");
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  

  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  
  id="ssphExport";
  orderBy="orderBy";
  if (item_type=="搜索词") {
    if(default_title=='热搜'){
      url="https://sycm.taobao.com/mc/industry/searchWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,indexCode:"tradeIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/industry/searchWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"seRiseRate",cateId:cateId,device:device,indexCode:"soarRank,seRiseRate,seIpvUvHits,clickHits,clickRate,payRate",token:token};
    }
  }else if(item_type=="长尾词"){
    if(default_title=='热搜'){
      url="https://sycm.taobao.com/mc/industry/tailWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"seIpvUvHits",cateId:cateId,device:device,indexCode:"hotSearchRank,seIpvUvHits,clickHits,clickRate,payRate",token:token};
    }else{
      url="https://sycm.taobao.com/mc/industry/tailWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"seRiseRate",cateId:cateId,device:device,indexCode:"soarRank,seRiseRate,seIpvUvHits,clickHits,clickRate,payRate",token:token};
    }
  }else if(item_type=="品牌词"){
    if(default_title=='热搜'){
      url="https://sycm.taobao.com/mc/industry/brandWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"avgWordSeIpvUvHits",cateId:cateId,device:device,indexCode:"hotSearchRank,relSeWordCnt,avgWordSeIpvUvHits,avgWordClickHits,avgWordClickRate,avgWordPayRate",token:token};
    }else{
      url="https://sycm.taobao.com/mc/industry/brandWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"avgWordSeRiseRate",cateId:cateId,device:device,indexCode:"soarRank,avgWordSeRiseRate,relSeWordCnt,avgWordSeIpvUvHits,avgWordClickHits,avgWordPayRate",token:token};
    }
  }else if(item_type=="核心词"){
    if(default_title=='热搜'){
      url="https://sycm.taobao.com/mc/industry/coreWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"avgWordSeIpvUvHits",cateId:cateId,device:device,indexCode:"hotSearchRank,relSeWordCnt,avgWordSeIpvUvHits,avgWordClickHits,avgWordClickRate,avgWordPayRate",token:token};
    }else{
      url="https://sycm.taobao.com/mc/industry/coreWord.json";
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"avgWordSeRiseRate",cateId:cateId,device:device,indexCode:"soarRank,avgWordSeRiseRate,relSeWordCnt,avgWordSeIpvUvHits,avgWordClickHits,avgWordPayRate",token:token};
    }
  }else if(item_type=="修饰词"){
    if(default_title=='热搜'){
      url="https://sycm.taobao.com/mc/industry/attrWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"avgWordSeIpvUvHits",cateId:cateId,device:device,indexCode:"hotSearchRank,relSeWordCnt,avgWordSeIpvUvHits,avgWordClickHits,avgWordClickRate,avgWordPayRate",token:token};
    }else{
      url="https://sycm.taobao.com/mc/industry/attrWord.json";  //搜索排行
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10000",page:"1",order:"desc",orderBy:"avgWordSeRiseRate",cateId:cateId,device:device,indexCode:"soarRank,avgWordSeRiseRate,relSeWordCnt,avgWordSeIpvUvHits,avgWordClickHits,avgWordPayRate",token:token};
    }
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+'-'+default_title+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>   &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入搜索词" class="layui-input"> <button  id="ssphExport" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if(default_title=='热搜'){
          exportdata=exportdata1['hotList'];
          short_id='hotSearchRank';
          if(item_type=="搜索词"||item_type=="长尾词"){
            arr_all =formattingData01(exportdata,["clickHits",'seIpvUvHits'],'');
          }else{
            arr_all =formattingData01(exportdata,["avgWordSeIpvUvHits",'avgWordClickHits'],'');
          }
        }else{
          exportdata=exportdata1['soarList'];
          short_id='soarRank';
          if(item_type=="搜索词"||item_type=="长尾词"){
            arr_all =formattingData01(exportdata,["clickHits",'seIpvUvHits'],'');
          }else{
            arr_all =formattingData01(exportdata,["avgWordSeIpvUvHits",'avgWordClickHits'],'');
          }
        }


        var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        
        // console.log(exportdata);
        // return false;
        //利用冒泡排序进行排序搜索排名
         var len = exportdata.length;  
          // for (var i = 0; i < len; i++) {  
          //     for(var j = 0; j < len - i -1; j++){  
          //         if(exportdata[j][short_id] >exportdata[j+1][short_id]){  //相邻元素进行对比  
          //             var temp = exportdata[j+1];//交换元素  
          //             exportdata[j+1] = exportdata[j];  
          //             exportdata[j] = temp;  
          //         }  
          //     }  
          // }   
        for(var i=0;i<len;i++){
            for(var j=i;j<len;j++){
            if(exportdata[i][short_id]>exportdata[j][short_id]){
              var tmp=exportdata[i];
              exportdata[i]=exportdata[j];
              exportdata[j]=tmp;
            }
          }
        }
        $("#device_title").text(len);
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(item_type=="搜索词"||item_type=="长尾词"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["searchWord"]=data[i]["searchWord"];//搜索词
                  if(data2["seIpvUvHits"][i]){
                    temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                  }else{
                    temp_arr["seIpvUvHits"]=0;// 搜索人数
                  }
                  
                  temp_arr["clickHits"]=decimal(data2["clickHits"][i]["0"],0);// 点击人数
                  
                  temp_arr["clickRate"]=decimal((data[i]["clickRate"]*100),0);// 点击率
                  if(data[i]["payRate"]){
                    temp_arr["payRate"]=decimal((data[i]["payRate"]*100),0);// 支付转化率
                  }else{
                    temp_arr["payRate"]=0;// 支付转化率
                  }
                  temp_arr["sszb"]=100;// 搜索占比
                  if(default_title=='热搜'){
                    if(data[i]["p4pRefPrice"]){
                      temp_arr["p4pRefPrice"]=decimal(data[i]["p4pRefPrice"],0);// 参考价格
                    }else{
                      temp_arr["p4pRefPrice"]=0;// 参考价格
                    }
                    
                    temp_arr["hotSearchRank"]=data[i]["hotSearchRank"];// 排名
                  }else{
                    temp_arr["soarRank"]=data[i]["soarRank"];// 排名
                    temp_arr["seRiseRate"]=decimal((data[i]["seRiseRate"]*100),0);// 涨幅增长率
                  }
                }else if(item_type=="品牌词"||item_type=="核心词"||item_type=="修饰词"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["searchWord"]=data[i]["searchWord"];//搜索词
                  temp_arr["avgWordSeIpvUvHits"]=decimal(data2["avgWordSeIpvUvHits"][i]["0"],0);// 搜索人数
                  temp_arr["avgWordClickHits"]=decimal(data2["avgWordClickHits"][i]["0"],0);// 点击人数
                  if(data[i]["avgWordPayRate"]){
                    temp_arr["avgWordPayRate"]=decimal((data[i]["avgWordPayRate"]*100),0);// 支付转化率
                  }else{
                    temp_arr["avgWordPayRate"]=0;// 支付转化率
                  }
                  if(data[i]["p4pRefPrice"]){
                    temp_arr["p4pRefPrice"]=decimal(data[i]["p4pRefPrice"],0);// 参考价格
                  }else{
                    temp_arr["p4pRefPrice"]=0;// 参考价格
                  }

                  
                  temp_arr["sszb"]=100;// 搜索占比
                  if(item_type=='品牌词'){
                    temp_arr["relSeWordCnt"]=data[i]["relSeWordCnt"];// 相关词搜索数
                    if(default_title=='热搜'){
                      temp_arr["hotSearchRank"]=data[i]["hotSearchRank"];// 排名
                      temp_arr["avgWordClickRate"]=decimal((data[i]["avgWordClickRate"]*100),0);// 点击率
                    }else{
                      temp_arr["soarRank"]=data[i]["soarRank"];// 排名
                      
                      temp_arr["avgWordSeRiseRate"]=decimal((data[i]["avgWordSeRiseRate"]*100),0);// 涨幅增长率
                    }
                  }else if(item_type=="核心词"||item_type=="修饰词"){
                    temp_arr["relSeWordCnt"]=data[i]["relSeWordCnt"];// 相关词搜索数
                    if(default_title=='热搜'){
                      temp_arr["hotSearchRank"]=data[i]["hotSearchRank"];// 排名
                      temp_arr["avgWordClickRate"]=decimal((data[i]["avgWordClickRate"]*100),0);// 点击率
                    }else{
                      temp_arr["soarRank"]=data[i]["soarRank"];// 排名
                      temp_arr["avgWordSeRiseRate"]=decimal((data[i]["avgWordSeRiseRate"]*100),0);// 涨幅增长率
                    }
                  }
                }
                tab_data.push(temp_arr);
              }                           
  
              exportData=[];
              if(item_type=="搜索词"||item_type=="长尾词"){
                if(default_title=='热搜'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '搜索排行-'+item_type+'-热词'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'hotSearchRank', width:"5%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30}
                      ,{field:'searchWord', title: '搜索词', width: "10%", minWidth: 30}
                      ,{field:'hotSearchRank', width: "7%", title: '搜索词排名', sort: true}
                      ,{field:'seIpvUvHits', title: '搜索人数',width: "10%" }
                      ,{field:'clickHits', title: '点击人数', sort: true,width: "10%"}
                      ,{field:'clickRate', title: '点击率', sort: true,width: "7%"}
                      ,{field:'payRate', width: "10%", title: '支付转化率', sort: true}
                      ,{field:'p4pRefPrice', width: "10%", title: '参考价格', sort: true}
                      ,{field:'sszb', width: "6%", title: '搜索占比', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '搜索排行-'+item_type+'-飙升'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'soarRank', width:"5%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30}
                      ,{field:'searchWord', title: '搜索词', width: "10%", minWidth: 30}
                      ,{field:'soarRank', width: "7%", title: '飙升排名', sort: true}
                      ,{field:'seRiseRate', width: "10%", title: '搜索增长幅度', sort: true}
                      ,{field:'seIpvUvHits', title: '搜索人数',width: "10%" }
                      ,{field:'clickHits', title: '点击人数', sort: true,width: "10%"}
                      ,{field:'clickRate', title: '点击率', sort: true,width: "7%"}
                      ,{field:'payRate', width: "10%", title: '支付转化率', sort: true}
                      ,{field:'sszb', width: "6%", title: '搜索占比', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }
              }else if(item_type=="品牌词"||item_type=="核心词"||item_type=="修饰词"){
                if(default_title=='热搜'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '搜索排行-'+item_type+'-热词'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'hotSearchRank', width:"3%", title: '#' ,sort: false}
                      ,{field:'time', width:"11%", title: '日期', sort: true}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30}
                      ,{field:'searchWord', title: '搜索词', width: "6%", minWidth: 30}
                      ,{field:'hotSearchRank', width: "7%", title: '热度排名', sort: true}
                      ,{field:'relSeWordCnt', width: "10%", title: '相关搜索词数', sort: true}
                      ,{field:'avgWordSeIpvUvHits', title: '相关词搜索人数',width: "10%" }
                      ,{field:'avgWordClickHits', title: '相关词点击人数', sort: true,width: "8%"}
                      ,{field:'avgWordClickRate', title: '词均点击率', sort: true,width: "9%"}
                      ,{field:'avgWordPayRate', width: "10%", title: '词均支付转化率', sort: true}
                      ,{field:'p4pRefPrice', width: "8%", title: '参考价格', sort: true}
                      ,{field:'sszb', width: "8%", title: '搜索占比', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '搜索排行-'+item_type+'-热词'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'soarRank', width:"3%", title: '#' ,sort: false}
                      ,{field:'time', width:"13%", title: '日期', sort: true}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30}
                      ,{field:'searchWord', title: '搜索词', width: "6%", minWidth: 30}
                      ,{field:'soarRank', width: "8%", title: '飙升排名', sort: true}
                      ,{field:'avgWordSeRiseRate', width: "14%", title: '词汇搜索增长幅度', sort: true}
                      ,{field:'relSeWordCnt', width: "10%", title: '相关搜索词数', sort: true}
                      ,{field:'avgWordSeIpvUvHits', title: '相关词搜索人数',width: "10%" }
                     
                      ,{field:'avgWordClickHits', title: '相关词点击人数', sort: true,width: "8%"}
                      ,{field:'avgWordPayRate', width: "10%", title: '词均支付转化率', sort: true}
                      
                      ,{field:'sszb', width: "8%", title: '搜索占比', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})

//客群趋势
$(document).on('click','#kqqs',function(e){
  //客群趋势导出
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }

  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);

  item_type = '客群趋势';
  id="kqqsExport2";
  //类目
  var short_name=$('.common-picker-header').attr('title');

  url="https://sycm.taobao.com/mc/mq/industryCustomer/customerTrend.json";  //监控店铺
  data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,order:"desc",cateId:cateId,device:device,sellerType:sellerType,indexCode:"",token:token};


  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> </div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  // l_index = layer.load(1, {
  //   shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  // });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        exportdata=exportdata1['payRateIndex'];
        // console.log(exportdata);
        // return false;
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
          for (var i = 0; i < len; i++) {  
              for(var j = 0; j < len - i -1; j++){  
                  if(exportdata[j] >exportdata[j+1]){  //相邻元素进行对比  
                      var temp = exportdata[j+1];//交换元素  
                      exportdata[j+1] = exportdata[j];  
                      exportdata[j] = temp;  
                  }  
              }  
          }   
        $("#device_title").text(len);
        // arr_all =formattingData2(exportdata);
        // console.log(arr_all);
        // return false;
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(item_type2=="热门店铺"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["shop"]["title"];
                    temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                    temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["shop"]["title"];
                    temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                    temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }else if(item_type2=="热门商品"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["item"]["title"];
                    temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
                    temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
                    temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["shop_title"]=data[i]["item"]["title"];
                    temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
                    temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
                    temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }else if(item_type2=="热门品牌"){
                  if(dateType=='today'){
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                    temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                    temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID

                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["order"]=i+1;
                    temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    temp_arr["page_name"]=sellerType_title;
                    temp_arr["source"]= device_title;  //来源
                    temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                    temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                    temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID

                    temp_arr["short_name"]=short_name;// 类目名称
                    temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                    temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                    temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  }
                }
                tab_data.push(temp_arr);
              }                           
  
              exportData=[];
              if(item_type2=="热门店铺"){
                if(dateType=='today'){
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门店铺'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'cateRankId', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'page_name', width:"8%", title: '平台'}
                        ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
                        ,{field:'shop_title', title: '店铺名称',width: "10%" }
                        ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'shop_userId', title: '店铺ID', sort: true,width: "10%"}
                        ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
                        ,{field:'cateRankId', width: "7%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }else{
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门店铺'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'cateRankId', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'page_name', width:"6%", title: '平台'}
                        ,{field:'source', title: '终端', width: "12%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
                        ,{field:'shop_title', title: '店铺名称',width: "10%" }
                        ,{field: 'pictureUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'shop_userId', title: '店铺ID', sort: true,width: "5%"}
                        ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
                        ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
                        ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
                        ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }else if(item_type2=="热门商品"){
                if(dateType=='today'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '行业监控-热门商品'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'cateRankId', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"8%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
                      ,{field:'shop_title', title: '商品名称',width: "12%",templet:function (d) { 
                        return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
                      }}
                      ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                        if (d.pictUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'itemId', title: '商品ID', sort: true,width: "10%"}
                      ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
                      ,{field:'cateRankId', width: "8%", title: '行业排名', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门商品'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'cateRankId', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"13%", title: '日期', sort: true}
                        ,{field:'page_name', width:"6%", title: '平台'}
                        ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
                        ,{field:'shop_title', title: '商品名称',width: "12%",templet:function (d) { 
                          return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
                        }}
                        ,{field: 'pictUrl', width: "8%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.pictUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'itemId', title: '商品ID', sort: true,width: "5%"}
                        ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
                        ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
                        ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
                        ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }else if(item_type2=="热门品牌"){
                if(dateType=='today'){
                  var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '行业监控-热门商品'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'cateRankId', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"8%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
                      ,{field:'brandName', title: '品牌名称',width: "12%"}
                      ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
                        if (d.logo!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'brandId', title: '商品ID', sort: true,width: "10%"}
                      ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
                      ,{field:'cateRankId', width: "8%", title: '行业排名', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
                }else{
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '行业监控-热门品牌'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'cateRankId', width:"5%", title: '#' ,sort: false}
                        ,{field:'time', width:"10%", title: '日期', sort: true}
                        ,{field:'page_name', width:"6%", title: '平台'}
                        ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                        ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
                        ,{field:'brandName', title: '品牌名称',width: "12%"}
                        ,{field: 'logo', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.logo!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'brandId', title: '商品ID', sort: true,width: "10%"}
                        ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
                        ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
                        ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
                        ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//监控店铺
$(document).on('click','#export_data8',function(e){
  //监控看板导出
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }

  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);
  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  // item_type2 = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();
  var short_name=$('.short-name').text();//类目名称
  id="jdlbExport";
  item_type2 = ($(".oui-tab-switch-item.oui-tab-switch-item-active.default").text()).substring(($(".oui-tab-switch-item.oui-tab-switch-item-active.default").text()).length-4);

    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/live/ci/shop/monitor/listShop.json";
      data_info={dateType:dateType,dateRange:date_start+'|'+date_end,orderType:'shop',order:'desc',orderBy:'uvIndex',device:device,cateId:cateId,sellerType:sellerType,indexCode:'uvIndex,tradeIndex,cateRankId',type:'all',_:time,token:token};
      
    }else{
      url="https://sycm.taobao.com/mc/ci/shop/monitor/listShop.json";  //监控店铺
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,device:device,order:"desc",orderBy:"uvIndex",cateId:cateId,sellerType:sellerType,indexCode:"uvIndex,tradeIndex,cateRankId",type:'all',token:token,orderType:'shop'};
    }
 

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+'-'+"竟店列表"+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);

        if (dateType=="today"){
          exportdata=exportdata1['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{  
          exportdata=exportdata1;
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }

        // return false;
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
          // for (var i = 0; i < len; i++) {  
          //     for(var j = 0; j < len - i -1; j++){  
          //         if(exportdata[j]["shop_tradeIndex"]["value"] >exportdata[j+1]["shop_tradeIndex"]["value"]){  //相邻元素进行对比  
          //             var temp = exportdata[j+1];//交换元素  
          //             exportdata[j+1] = exportdata[j];  
          //             exportdata[j] = temp;  
          //         }  
          //     }  
          // }   
          for(var i=0;i<len;i++){
            for(var j=i;j<len;j++){
            if(exportdata[i]>exportdata[j]){
              var tmp=exportdata[i];
              exportdata[i]=exportdata[j];
              exportdata[j]=tmp;
            }
          }
        }
        // for(var i=0;i<len;i++){
        //   for(var j=i;j<len;j++){
        //     if(exportdata[i]["shop_tradeIndex"]["value"]>exportdata[j]["shop_tradeIndex"]["value"]){
        //       var tmp=exportdata[i];
        //       exportdata[i]=exportdata[j];
        //       exportdata[j]=tmp;
        //     }
        //   }
        // }
        // return false;
        $("#device_title").text(len);
        if(dateType=='today'){
          arr_all =formattingData(exportdata,["shop_uvIndex","shop_seIpvUvHits","shop_cltHits","shop_cartHits","shop_tradeIndex","cate_tradeIndex"],'shop_payRateIndex');
        }else{
          arr_all =formattingData00(exportdata,["shop_uvIndex","shop_seIpvUvHits","shop_cltHits","shop_cartHits","shop_tradeIndex","cate_tradeIndex","cate_cartHits","cate_uvIndex","cate_seIpvUvHits","cate_cltHits"],'shop_payRateIndex','cate_payRateIndex');
        }
        // console.log(arr_all);
        // return false;
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",    
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              // return false;
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(dateType=='today'){
                  // if(data.hasOwnProperty(data[i]["cate_cateRankId"])==true){
                  if(data[i]["cate_cateRankId"]!= undefined){
                      temp_arr["cate_cateRankId"]=data[i]["cate_cateRankId"]["value"];// 类目-行业排名
                  }else{
                      temp_arr["cate_cateRankId"]='';// 类目-行业排名
                  }
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["page_name"]=sellerType_title;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["shop_title"]=data[i]["shop"]["title"];
                  temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                  temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                  temp_arr["shop_uvIndex"]=decimal(data2["shop_uvIndex"][i]["0"],0);// 全店-访客数
                  temp_arr["shop_seIpvUvHits"]=decimal(data2["shop_seIpvUvHits"][i]["0"],0);// 全店-搜索人数
                  temp_arr["shop_cltHits"]=decimal(data2["shop_cltHits"][i]["0"],0);// 全店-收藏人数
                  temp_arr["shop_cartHits"]=decimal(data2["shop_cartHits"][i]["0"],0);// 全店-加购人数
                  temp_arr["shop_payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 全店-转化率
                  // temp_arr["cate_payRateIndex"]=decimal(data2["zhl2"][i]["0"]*100,2);// 类目-转化率
                  temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-交易额
                  // temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-预售定金金额
                  // temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-预售定金商品件数
                  if(data2["cate_tradeIndex"][i]){
                    temp_arr["cate_tradeIndex"]=decimal(data2["cate_tradeIndex"][i]["0"],0);// 类目-交易额
                  }else{
                    temp_arr["cate_tradeIndex"]=0;// 类目-交易额
                  }
                  
                  temp_arr["shop_payNum"]=decimal(decimal(data2["shop_uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 全店-买家数
                  // temp_arr["cate_payNum"]=decimal(decimal(data2["shop_uvIndex"][i]["0"],0)*decimal(temp_arr["cate_payRateIndex"],2),0);// 类目-买家数

                  
                  // temp_arr["shop_fstOnsItmCnt"]=data[i]["shop_fstOnsItmCnt"]["value"];// 上新商品件数
                }else{
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["page_name"]=sellerType_title;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["shop_title"]=data[i]["shop"]["title"];
                  temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                  temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                  temp_arr["shop_uvIndex"]=decimal(data2["shop_uvIndex"][i]["0"],0);// 全店-访客数
                  temp_arr["shop_seIpvUvHits"]=decimal(data2["shop_seIpvUvHits"][i]["0"],0);// 全店-搜索人数
                  temp_arr["shop_cltHits"]=decimal(data2["shop_cltHits"][i]["0"],0);// 全店-收藏人数
                  temp_arr["shop_cartHits"]=decimal(data2["shop_cartHits"][i]["0"],0);// 全店-加购人数
                  temp_arr["shop_payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 全店-转化率
                  temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-交易额
                  // temp_arr["shop_payByrCntIndex"]=decimal(data2["shop_payByrCntIndex"][i]["0"],0);// 全店-预售定金金额
                  // temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-预售定金商品件数
                  temp_arr["cate_tradeIndex"]=decimal(data2["cate_tradeIndex"][i]["0"],0);// 类目-交易额

                  temp_arr["cate_cartHits"]=decimal(data2["cate_cartHits"][i]["0"],0);// 类目-加购人数
                  temp_arr["cate_uvIndex"]=decimal(data2["cate_uvIndex"][i]["0"],0);// 类目-访客数
                  temp_arr["cate_seIpvUvHits"]=decimal(data2["cate_seIpvUvHits"][i]["0"],0);// 类目-搜索人数
                  temp_arr["cate_cltHits"]=decimal(data2["cate_cltHits"][i]["0"],0);// 类目-搜藏人数
                  // temp_arr["cate_payRateIndex"]=decimal(data2["zhl2"][i]["0"]*100,2);// 类目-转化率
                  temp_arr["shop_payNum"]=decimal(decimal(data2["shop_uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 全店-买家数
                  // temp_arr["cate_payNum"]=decimal(decimal(data2["cate_uvIndex"][i]["0"],0)*decimal(data2["zhl2"][i]["0"],2),0);// 类目-买家数

                  temp_arr["cate_cateRankId"]=data[i]["cate_cateRankId"]["value"];// 类目-行业排名
                  // temp_arr["shop_fstOnsItmCnt"]=data[i]["shop_fstOnsItmCnt"]["value"];// 全店-上新商品件数
                }
                
                tab_data.push(temp_arr);
              }                           
              // console.log(tab_data);
              // return false;
              exportData=[];
              if(dateType=='today'){
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控店铺-竞店列表'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"5%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'page_name', width:"5%", title: '平台'}
                      ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      
                      ,{field:'shop_title', title: '店铺名称',width: "6%" }
                      ,{field: 'pictureUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
                        if (d.pictureUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'shop_userId', title: '店铺ID', sort: true,width: "5%"}
                      ,{field:'shop_uvIndex', width: "7%", title: '全店-访客数', sort: true}
                      ,{field:'shop_seIpvUvHits', width: "7%", title: '全店-搜索人数', sort: true}
                      ,{field:'shop_cltHits', width: "7%", title: '全店-收藏人数', sort: true}
                      ,{field:'shop_cartHits', width: "7%", title: '全店-加购人数', sort: true}
                      ,{field:'shop_payRateIndex', width: "7%", title: '全店-转化率', sort: true}
                      ,{field:'shop_tradeIndex', width: "7%", title: '全店-交易额', sort: true}
                      ,{field:'shop_payNum', width: "7%", title: '全店-买家数', sort: true}
                      ,{field:'cate_tradeIndex', width: "7%", title: '类目-交易额', sort: true}
                      ,{field:'cate_cateRankId', width: "7%", title: '类目-行业排名', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                });
              }else{
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控店铺-竞店列表'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"5%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"8%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      
                      ,{field:'shop_title', title: '店铺名称',width: "10%" }
                      ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                        if (d.pictureUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'shop_userId', title: '店铺ID', sort: true,width: "10%"}
                      ,{field:'shop_uvIndex', width: "10%", title: '全店-访客数', sort: true}
                      ,{field:'shop_seIpvUvHits', width: "7%", title: '全店-搜索人数', sort: true}
                      ,{field:'shop_cltHits', width: "7%", title: '全店-收藏人数', sort: true}
                      ,{field:'shop_cartHits', width: "7%", title: '全店-加购人数', sort: true}
                      ,{field:'shop_payRateIndex', width: "7%", title: '全店-转化率', sort: true}
                      ,{field:'shop_tradeIndex', width: "7%", title: '全店-交易额', sort: true}
                      ,{field:'shop_payNum', width: "7%", title: '全店-买家数', sort: true}
                      ,{field:'cate_uvIndex', width: "7%", title: '类目-访客数', sort: true}
                      ,{field:'cate_seIpvUvHits', width: "7%", title: '类目-搜索人数', sort: true}
                      ,{field:'cate_cartHits', width: "7%", title: '类目-加购人数', sort: true}
                      ,{field:'cate_cltHits', width: "7%", title: '类目-收藏人数', sort: true}
                      ,{field:'cate_payRateIndex', width: "7%", title: '类目-转化率', sort: true}
                      ,{field:'cate_tradeIndex', width: "7%", title: '类目-交易额', sort: true}
                      ,{field:'cate_payNum', width: "7%", title: '类目-买家数', sort: true}
                      ,{field:'cate_cateRankId', width: "7%", title: '类目-行业排名', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                });
              }
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//竟店识别 
$(document).on('click','#jdsb_export',function(e){
  //监控看板导出
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");


    date_start=dateRange_text;
    date_end=dateRange_text;
  


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC"){
    device=1;
  }else if(device_title=="无线"){
    device=2;
  }

  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);
  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();

  id="lsdpExport";

  url="https://sycm.taobao.com/mc/ci/shop/recognition/getTopDrainList.json";  //TOP流失店铺
  data_info={dateRange:date_start+'|'+date_end,dateType:dateType,order:"desc",orderBy:"lostIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"lostIndex,lostHits,uvIndex,seIpvUvHits,tradeIndex",_:time,token:token};
  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+'-'+"TOP流失店铺列表"+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        exportdata=exportdata1['data'];
        var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
         //  for (var i = 0; i < len; i++) {  
         //      for(var j = 0; j < len - i -1; j++){  
         //          if(exportdata[j]["cate_cateRankId"]["value"] >exportdata[j+1]["cate_cateRankId"]["value"]){  //相邻元素进行对比  
         //              var temp = exportdata[j+1];//交换元素  
         //              exportdata[j+1] = exportdata[j];  
         //              exportdata[j] = temp;  
         //          }  
         //      }  
         //  }   
        $("#device_title").text(len);

        arr_all =formattingData01(exportdata,["uvIndex","lostHits","lostIndex","tradeIndex","seIpvUvHits"],'');

        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",    
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              // return false;
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                
                temp_arr["order"]=i+1;
                temp_arr["time"]=now_date;
                temp_arr["page_name"]=sellerType_title;
                temp_arr["source"]= device_title;  //来源
                temp_arr["shop_title"]=data[i]["shop"]["title"];
                temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
                temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
                temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
                temp_arr["lostHits"]=decimal(data2["lostHits"][i]["0"],0);// 流失人数
                temp_arr["lostIndex"]=decimal(data2["lostIndex"][i]["0"],0);// 点击次数
                temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                
                tab_data.push(temp_arr);
              }                           
              // console.log(tab_data);
              // return false;
              exportData=[];
              
              var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '竞店识别-TOP流失店铺列表'
                  ,data:tab_data
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                    {field:'order', width:"5%", title: '#' ,sort: false}
                    ,{field:'time', width:"8%", title: '日期', sort: true}
                    ,{field:'page_name', width:"8%", title: '平台'}
                    ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'shop_title', title: '店铺名称',width: "8%" }
                    ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                      if (d.pictureUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'shop_userId', title: '店铺ID', sort: true,width: "7%"}
                    ,{field:'lostIndex', width: "7%", title: '点击次数', sort: true}
                    ,{field:'lostHits', width: "7%", title: '流失人数', sort: true}
                    ,{field:'uvIndex', width: "7%", title: '访客数', sort: true}
                    ,{field:'seIpvUvHits', width: "7%", title: '搜索人数', sort: true}
                    ,{field:'tradeIndex', width: "7%", title: '交易金额', sort: true}
                    ,{field:'searchProportion', width: "7%", title: '搜索占比(%)', sort: true}
                    ,{field:'uvCost', width: "6%", title: 'UV价值', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
              });

              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//关键指数对比 
$(document).on('click','#gjzsdb',function(e){
  //关键指数对比
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }



  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);

  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();

  id="jgjzsdbExport";
  orderBy="orderBy";
  if(dateType=="today"){
    url="https://sycm.taobao.com/mc/rivalShop/analysis/getLiveCoreIndexes.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,cateId:cateId,device:device,selfUserId:userId,token:token};
  }else{
    url="https://sycm.taobao.com/mc/rivalShop/analysis/getCoreIndexes.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,cateId:cateId,device:device,token:token,selfUserId:userId};
  }
  

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-关键指数对比'+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp;        &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  // l_index = layer.load(1, {
  //   shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  // });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if (dateType=="today"){
          exportdata=exportdata1['data']['selfShop'];
        }else{  
          exportdata=exportdata1['selfShop'];
        }
        len=exportdata.length;
        // console.log(exportdata);
        // return false;
         
        $("#device_title").text(len);
        

        arr_all =formattingDataOne(exportdata,["cartHits","cltHits","payByrCntIndex","seIpvUvHits","tradeIndex","uvIndex"],"payRateIndex"); 
        
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                
                temp_arr["order"]=i+1;
                temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                
                temp_arr["source"]= device_title;  //来源
                temp_arr["shop_title"]=1;
                temp_arr["pictureUrl"]=1;  //店铺图片
                temp_arr["shop_userId"]=1; //店铺ID

                temp_arr["id"]=c++;// 序号
                temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

                temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
                temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

                temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                if(temp_arr["payNum"]==0){
                  temp_arr["kdj"]=0;
                }else{
                  temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                }
                temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                
                tab_data.push(temp_arr);
              }                           
  
              console.log(tab_data);

              exportData=[];
              var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '店铺分析-关键指标对比'
                  ,data:tab_data
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                    {field:'order', width:"4%", title: '#' ,sort: false}
                    ,{field:'time', width:"7%", title: '日期', sort: true}
                    ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'shop_title', title: '店铺名称',width: "8%" }
                    ,{field: 'pictureUrl', width: "5%", title: '店铺图片',  sort: false,templet:function (d) { 
                      if (d.pictureUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'shop_userId', title: '店铺ID', sort: true,width: "4%"}
                    ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                    ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                    ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                    ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                    ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                    ,{field:'cartHits', width: "6%", title: '预售定金交易金额', sort: true}
                    ,{field:'cartHits', width: "6%", title: '预售支付商品件数', sort: true}
                    ,{field:'cartHits', width: "6%", title: '上新商品数', sort: true}
                    ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                    ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                    ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                    ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                    ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                    ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                    ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  // initSort: {
                  //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
                  //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                  // },
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
              });

              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//TOP商品榜
$(document).on('click','#topsp',function(e){

  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  // device_title = $(".oui-select-container-value").text();
  device_title = $(".ant-select-selection-selected-value").eq(1).text();
  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }



  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);
  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  id="topspExport";
  orderBy="orderBy";
  if(default_title=='热销'){
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/rivalShop/analysis/getLiveTopItems.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",userId:userId,topType:'trade',cateId:cateId,device:device,indexCode:"tradeIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/rivalShop/analysis/getTopItems.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",userId:userId,topType:'trade',cateId:cateId,device:device,indexCode:"tradeIndex",token:token};
    }
  }else{
    if(dateType=="today"){
      url="https://sycm.taobao.com/mc/rivalShop/analysis/getLiveTopItems.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",userId:userId,topType:'flow',cateId:cateId,device:device,indexCode:"uvIndex",token:token};
    }else{
      url="https://sycm.taobao.com/mc/rivalShop/analysis/getTopItems.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",userId:userId,topType:'flow',cateId:cateId,device:device,indexCode:"uvIndex",token:token};
    }
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-TOP商品榜-'+default_title+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>      &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if (dateType=="today"){
          exportdata=exportdata1['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{  
          exportdata=exportdata1;
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }
        // statDate
        // console.log(exportdata1);
        // return false;
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
         //  for (var i = 0; i < len; i++) {  
         //      for(var j = 0; j < len - i -1; j++){  
         //          if(exportdata[j]["tradeIndex"]["value"] >exportdata[j+1]["tradeIndex"]["value"]){  //相邻元素进行对比  
         //              var temp = exportdata[j+1];//交换元素  
         //              exportdata[j+1] = exportdata[j];  
         //              exportdata[j] = temp;  
         //          }  
         //      }  
         //  }   

        $("#device_title").text(len);
        
        if(default_title=='热销'){
 
          arr_all =formattingData(exportdata,["tradeIndex"],''); 
        }else{
          arr_all =formattingData(exportdata,["uvIndex"],""); 
        }
        // console.log(arr_all);
        
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              // console.log(data2);
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(default_title=='热销'){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
             
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["item_title"]=data[i]["item"]["title"];

                  shopId=data[i]["item"]["detailUrl"].split('id=');
                  temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //商品图片
                  temp_arr["item_userId"]=shopId[1]; //商品ID
                  temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                  temp_arr["id"]=c++;// 序号
                  temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                }else{
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  shopId=data[i]["item"]["detailUrl"].split('id=');
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["item_title"]=data[i]["item"]["title"];
                  sp_id = data[i]["item"]["detailUrl"].indexOf('id=');
                  temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //商品图片
                  temp_arr["item_userId"]=shopId[1]; //商品ID
                  temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
                  temp_arr["id"]=c++;// 序号
                  temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
                }
                tab_data.push(temp_arr);
              }                           
  
              exportData=[];
              if(default_title=='热销'){
                var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '竞店分析-TOP店铺榜-热销'
                  ,data:tab_data
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                    {field:'order', width:"10%", title: '#' ,sort: false}
                    ,{field:'time', width:"20%", title: '日期', sort: true}
                    ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'item_title', title: '商品名称',width: "20%",templet:function (d) { 
                        return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';
                      }}
                    ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                      if (d.pictUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'item_userId', title: '商品ID', sort: true,width: "15%"}
                    ,{field:'tradeIndex', width: "15%", title: '交易金额', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
                });
              }else{
                var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '竞店分析-TOP店铺榜-流量'
                  ,data:tab_data
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                   {field:'order', width:"10%", title: '#' ,sort: false}
                    ,{field:'time', width:"20%", title: '日期', sort: true}
                    ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                   ,{field:'item_title', title: '商品名称',width: "20%",templet:function (d) { 
                        return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';
                      }}
                    ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                      if (d.pictUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'item_userId', title: '商品ID', sort: true,width: "15%"}
                    ,{field:'uvIndex', width: "15%", title: '访客数', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
                });
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//入店来源
$(document).on('click','#rdly',function(e){
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }
  device_title = $(".ant-select-selection-selected-value").eq(2).text();


  if(device_title=="PC端"){
    device=1;
  }else{
    device=2;
  }
  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);
  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  id="rdlyExport";
  orderBy="orderBy";

  if(dateType=="today"){
    url="https://sycm.taobao.com/mc/rivalShop/analysis/getLiveFlowSource.json";  //
 
    data_info={cateId:cateId,selfUserId:userId,device:device,dateType:dateType,dateRange:date_start+'|'+date_end,indexCode:'uvIndex',orderBy:'uvIndex',order:'desc',_:time,token:token};
  }else{
    url="https://sycm.taobao.com/mc/rivalShop/analysis/getFlowSource.json";  //
    data_info={cateId:cateId,selfUserId:userId,device:device,dateType:dateType,dateRange:date_start+'|'+date_end,indexCode:'uvIndex',orderBy:'uvIndex',order:'desc',_:time,token:token};
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-流量来源-'+default_title+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>      &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入流量来源" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if (dateType=="today"){
          exportdata=exportdata1['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{  
          exportdata=exportdata1;
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }
        var len = exportdata.length;  

        $("#device_title").text(len);
        

        arr_all =formattingData(exportdata,["selfShopUvIndex"],""); 
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              tab_arr=[];
              tab_arr1=[];
              tab_data1=[];
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                temp_arr={};
                if(dateType=="today"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["pageName"]=data[i]["pageName"]["value"];//流量来源
                  for(var j=0;j<data[i]['children'].length;j++){
                    arr={};
                    arr["order"]=j+i;
                    arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                    arr["source"]= device_title;  //来源
                    arr['pageName']=data[i]['children'][j]["pageName"]["value"];//流量来源
                    arr['selfShopUv']=data[i]['children'][j]["selfShopUv"]["value"];//访客数
                    arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
                    arr["shop_userId"]=userId; //店铺ID
                    arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
                    tab_arr.push(arr);
                  }  
                  temp_arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
                  temp_arr["shop_userId"]=userId; //店铺ID
                  temp_arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
                  temp_arr["selfShopUv"]=data[i]["selfShopUv"]["value"]; //访客数
                  temp_arr["id"]=c++;// 序号
                }else{
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["pageName"]=data[i]["pageName"]["value"];//流量来源
                  if(data[i]['children']){
                    for(var j=0;j<data[i]['children'].length;j++){
                      arr={};
                      arr["order"]=j+i;
                      arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                      arr["source"]= device_title;  //来源
                      arr['pageName']=data[i]['children'][j]["pageName"]["value"];//流量来源
                      arr['selfShopUv']=data[i]['children'][j]["selfShopUv"]["value"];//访客数
                      arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
                      arr["shop_userId"]=userId; //店铺ID
                      arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称

                      // arr["selfShopPayByrCnt"]=data[i]['children'][j]["selfShopPayByrCnt"]["value"]; //买家数
                      arr["selfShopPayAmt"]=decimal(data[i]['children'][j]["selfShopPayAmt"]["value"],2); //交易金额
                      // arr["selfShopPayByrCnt"]=decimal(arr["selfShopPayAmt"]/arr["selfShopPayByrCnt"],2); //客单价
                      arr["selfShopPayRate"]=decimal((data[i]['children'][j]["selfShopPayRate"]["value"])*100,2); //转化率
                      if(data[i]['children'][j]["selfShopPayByrCnt"]["value"]!=NaN){
                        arr["selfShopPayByrCnt"]=decimal(data[i]['children'][j]["selfShopPayByrCnt"]["value"],0); //买家数
                      }else{
                        arr["selfShopPayByrCnt"]=0;
                      }
                      if(data[i]['children'][j]["selfShopPayByrCnt"]["value"]!=NaN&&data[i]['children'][j]["selfShopPayByrCnt"]["value"]>0){
                        arr["kdj"]=decimal(arr["selfShopPayAmt"]/arr["selfShopPayByrCnt"],2); //客单价
                      }else{
                        arr["selfShopPayByrCnt"]=0;
                      }
                      arr["uvCost"]=decimal(arr["selfShopPayAmt"]/arr["selfShopUv"],2);// UV价值
                      tab_arr.push(arr);
                    }  
                  }else{
                    arr={};
                  }
                  temp_arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
                  temp_arr["shop_userId"]=userId; //店铺ID
                  temp_arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
                  temp_arr["selfShopUv"]=data[i]["selfShopUv"]["value"]; //访客数
                  
                  temp_arr["selfShopPayAmt"]=decimal(data[i]["selfShopPayAmt"]["value"],2); //交易金额
                  if(data[i]["selfShopPayByrCnt"]["value"]!=NaN){
                    temp_arr["selfShopPayByrCnt"]=decimal(data[i]["selfShopPayByrCnt"]["value"],0); //买家数
                  }else{
                    temp_arr["selfShopPayByrCnt"]=0;
                  }
                  if(data[i]["selfShopPayByrCnt"]["value"]!=NaN&&data[i]["selfShopPayByrCnt"]["value"]>0){
                    temp_arr["kdj"]=decimal(temp_arr["selfShopPayAmt"]/temp_arr["selfShopPayByrCnt"],2); //客单价
                  }else{
                    temp_arr["selfShopPayByrCnt"]=0;
                  }
                  

                  temp_arr["selfShopPayRate"]=decimal((data[i]["selfShopPayRate"]["value"])*100,2); //转化率
                  temp_arr["uvCost"]=decimal(temp_arr["selfShopPayAmt"]/temp_arr["selfShopUv"],2);// UV价值
                  temp_arr["id"]=c++;// 序号
                }
                tab_data.push(temp_arr);
              }  
              
              Array.prototype.push.apply(tab_data,tab_arr)
              for (var i = 0; i < tab_data.length; i++) {
                temp_arr={};
                if(dateType=="today"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["pageName"]=tab_data[i]["pageName"];//流量来源 
                  temp_arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
                  temp_arr["shop_userId"]=userId; //店铺ID
                  temp_arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
                  temp_arr["selfShopUv"]=tab_data[i]["selfShopUv"]; //访客数
                  temp_arr["id"]=c++;// 序号
                }else{
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["pageName"]=tab_data[i]["pageName"]["value"];//流量来源
                  temp_arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
                  temp_arr["shop_userId"]=userId; //店铺ID
                  temp_arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
                  temp_arr["selfShopUv"]=tab_data[i]["selfShopUv"]; //访客数
                  
                  temp_arr["selfShopPayAmt"]=decimal(tab_data[i]["selfShopPayAmt"],2); //交易金额
                  if(tab_data[i]["selfShopPayByrCnt"]["value"]!=NaN){
                    temp_arr["selfShopPayByrCnt"]=decimal(tab_data[i]["selfShopPayByrCnt"],0); //买家数
                  }else{
                    temp_arr["selfShopPayByrCnt"]=0;
                  }
                  if(tab_data[i]["selfShopPayByrCnt"]["value"]!=NaN&&tab_data[i]["selfShopPayByrCnt"]>0){
                    temp_arr["kdj"]=decimal(temp_arr["selfShopPayAmt"]/temp_arr["selfShopPayByrCnt"],2); //客单价
                  }else{
                    temp_arr["selfShopPayByrCnt"]=0;
                  }
                  temp_arr["selfShopPayRate"]=decimal((tab_data[i]["selfShopPayRate"])*100,2); //转化率
                  temp_arr["uvCost"]=decimal(temp_arr["selfShopPayAmt"]/temp_arr["selfShopUv"],2);// UV价值
                  temp_arr["id"]=c++;// 序号
                }
                tab_data1.push(temp_arr);
              }  
              $("#device_title").text(tab_data.length);
              exportData=[];
              if(dateType=="today"){
                var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '竞店分析-流量来源'
                  ,data:tab_data1
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                    {field:'order', width:"10%", title: '#' ,sort: false}
                    ,{field:'time', width:"10%", title: '日期', sort: true}
                    ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'pageName', title: '流量来源', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'shop_name', title: '店铺名称',width: "20%"}
                    ,{field: 'pictUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                      if (d.pictUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'shop_userId', title: '店铺ID', sort: true,width: "15%"}
                    ,{field:'selfShopUv', width: "15%", title: '访客数', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
                });
              }else{
                var ins1= table.render({
                  elem: '#test'
                  ,id: 'testReload'
                  ,title: '竞店分析-流量来源'
                  ,data:tab_data
                  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                  ,limit:10
                  ,height:472
                  ,cols: [[
                    {field:'ids', width:"5%", title: '#' ,sort: false}
                    ,{field:'time', width:"10%", title: '日期', sort: true}
                    ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'pageName', title: '流量来源', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'shop_name', title: '店铺名称',width: "10%"}
                    ,{field: 'pictUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
                      if (d.pictUrl!=undefined) {
                        return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                      }else{
                        return '<div style="text-align: center;" ></div>'; 
                      }
                    }}
                    ,{field:'shop_userId', title: '店铺ID', sort: true,width: "6%"}
                    ,{field:'selfShopUv', width: "8%", title: '访客数', sort: true}
                    ,{field:'selfShopPayRate', width: "8%", title: '转化率(%)', sort: true}
                    ,{field:'selfShopPayAmt', width: "8%", title: '交易金额', sort: true}
                    ,{field:'selfShopPayByrCnt', width: "8%", title: '买家数', sort: true}
                    ,{field:'kdj', width: "8%", title: '客单价', sort: true}
                    ,{field:'selfShopUv', width: "8%", title: 'UV价值', sort: true}
                  ]],  
                  page:  {limits: [10, 20, 50, 100, 300,500]},
                  done: function (res, curr, count) {
                    exportData=res.data;
                  }
                });
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//监控商品
$(document).on('click','#jksp_export',function(e){
  //关键指数对比
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }



  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  // console.log(cateId);

  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();

  id="jkspExport";
  orderBy="orderBy";
  token_content=$("meta[name='microdata']").attr("content");
  lm = token_content.indexOf('token=');
  lm_userId = token_content.indexOf('userId=');
  userId_content_two = token_content.substring(lm_userId+'userId='.length,token_content.length); 
  lm2_userId = userId_content_two.indexOf(';');
  token_content_two = token_content.substring(lm+'token='.length,token_content.length);    
  lm2 = token_content_two.indexOf(';');
  token = token_content_two.substring(0,lm2);
  userId= userId_content_two.substring(0,lm2_userId);
  // console.log(token);
  // return false;
  if(dateType=="today"){
    url="https://sycm.taobao.com/mc/live/ci/item/monitor/list.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex",token:token};
  }else{
    url="https://sycm.taobao.com/mc/ci/item/monitor/list.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex",token:token,type:'all'};
  }
  

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+"竞品列表"+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入商品名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        if (dateType=="today"){
          exportdata=exportdata1['data']['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{  
          exportdata=exportdata1['data'];
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
         //  for (var i = 0; i < len; i++) {  
         //      for(var j = 0; j < len - i -1; j++){  
         //          if(exportdata[j]["tradeIndex"]["value"] >exportdata[j+1]["tradeIndex"]["value"]){  //相邻元素进行对比  
         //              var temp = exportdata[j+1];//交换元素  
         //              exportdata[j+1] = exportdata[j];  
         //              exportdata[j] = temp;  
         //          }  
         //      }  
         //  }   
        $("#device_title").text(len);
        arr_all =formattingData(exportdata,["seIpvUvHits","tradeIndex","uvIndex","cltHits","cartHits"],"payRateIndex"); 

        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                 
                temp_arr={};
                if(dateType=="today"){
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["page_name"]=sellerType_title;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["item_title"]=data[i]["item"]["title"];
                  temp_arr["pictureUrl"]=data[i]["item"]["pictUrl"];  //商品图片
                  temp_arr["item_userId"]=data[i]["item"]["userId"]; //商品ID
                  temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品url
                  temp_arr["id"]=c++;// 序号
                  temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

                  temp_arr["payNum"]=decimal(decimal(temp_arr["uvIndex"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                  temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
                  temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数
                  
                  if(data2["seIpvUvHits"][i]){
                    temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i],0);// 搜索人数
                    temp_arr["searchProportion"]=decimal((temp_arr["seIpvUvHits"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                  }else{
                    temp_arr["seIpvUvHits"]=0;// 搜索人数
                    temp_arr["searchProportion"]=0;// 搜索占比
                  }
                  temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  
                  temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                  temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                  temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                  if(temp_arr["payNum"]==0){
                    temp_arr["kdj"]=0;
                  }else{
                    temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                  }
                  temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                }else{
                  temp_arr["order"]=i+1;
                  temp_arr["time"]=now_date;
                  temp_arr["page_name"]=sellerType_title;
                  temp_arr["source"]= device_title;  //来源
                  temp_arr["item_title"]=data[i]["item"]["title"];
                  temp_arr["pictureUrl"]=data[i]["item"]["pictUrl"];  //商品图片
                  temp_arr["item_userId"]=data[i]["item"]["userId"]; //商品ID
                  temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品url
                  temp_arr["id"]=c++;// 序号
                  temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
                  temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
                  temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数
                  if(data2["seIpvUvHits"]!=undefined){
                    temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i],0);// 搜索人数
                  }else{
                    temp_arr["seIpvUvHits"]=0;// 搜索人数
                  }
                  

                  temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                  
                  temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
                  temp_arr["searchProportion"]=decimal((temp_arr["seIpvUvHits"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                  temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                  temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                  
                  if(temp_arr["payNum"]==0){
                    temp_arr["kdj"]=0;
                  }else{
                    temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                  }
                  temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                }
                tab_data.push(temp_arr);
              }                           

              exportData=[];
              if(dateType=="today"){
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控商品-竞品列表'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'page_name', width:"4%", title: '平台'}
                      ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'item_title', title: '商品名称',width: "8%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                      ,{field: 'pictureUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
                        if (d.pictureUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'item_userId', title: '商品ID', sort: true,width: "4%"}

                      ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                      ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                      ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                      ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                      ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                      ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                      ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                      ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                      ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                      ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                      ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                      ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    // initSort: {
                    //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
                    //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                    // },
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                });
              }else{
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控商品-竞品列表'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'page_name', width:"4%", title: '平台'}
                      ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'item_title', title: '商品名称',width: "8%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                      ,{field: 'pictureUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
                        if (d.pictureUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'item_userId', title: '商品ID', sort: true,width: "4%"}
                      ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                      ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                      ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                      ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                      ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                      ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                      ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                      ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                      ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                      ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                      ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                      ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    // initSort: {
                    //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
                    //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                    // },
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                });
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//竞品识别
$(document).on('click','#jpsb',function(e){
  //关键指数对比
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-card-switch-item.oui-card-switch-item-active").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC端"){
    device=1;
  }else if(device_title=="无线端"){
    device=2;
  }



  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-card-switch-item.oui-card-switch-item-active").text();


  //请求数据接口
  //打开导出的表格
  
  var db_chick=$("input[class='ant-radio-input']:checked").parent().next().text();
  // var db=$('.ant-radio-input').value();
  // if(db==){

  // }
 // console.log(db_chick)
 // return false;
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  click_on=$('.oui-radio.ant-radio-wrapper.ant-radio-wrapper-checked').eq(1).find('span');

  id="jpsbExport";
  orderBy="orderBy";
  if(item_type=='顾客流失竞品推荐'){
    url="https://sycm.taobao.com/mc/ci/item/recognition/getCrmDrainList.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"100",page:"1",order:"desc",orderBy:"payLostAmt",cateId:cateId,device:device,sellerType:sellerType,indexCode:"payLostAmt,losByrCnt,losRate,cltLosByrCnt,cartLosByrCnt",token:token};
  }else{
    if(db_chick=='搜索引导访客数'){
      url="https://sycm.taobao.com/mc/ci/item/recognition/getSeDrainList.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"100",page:"1",order:"desc",orderBy:"seRivalItmCnt",cateId:cateId,device:device,sellerType:sellerType,indexCode:"seRivalItmCnt,seGuideUv,rivalItmAvgSeGuideUv",token:token};
    }else if(db_chick=='搜索引导加购人数'){
      url="https://sycm.taobao.com/mc/ci/item/recognition/getSeDrainList.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"100",page:"1",order:"desc",orderBy:"seRivalItmCnt",cateId:cateId,device:device,sellerType:sellerType,indexCode:"seRivalItmCnt,seGuideCartByrCnt,rivalItmAvgSeGuideCartByrCnt",token:token};
    }else if(db_chick=='搜索引导支付买家数'){
      url="https://sycm.taobao.com/mc/ci/item/recognition/getSeDrainList.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"100",page:"1",order:"desc",orderBy:"seRivalItmCnt",cateId:cateId,device:device,sellerType:sellerType,indexCode:"seRivalItmCnt,seGuidePayByrCnt,rivalItmAvgSeGuidePayByrCnt",token:token};
    }else if(db_chick=='搜索引导支付转化率'){
      url="https://sycm.taobao.com/mc/ci/item/recognition/getSeDrainList.json";  //
      data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"100",page:"1",order:"desc",orderBy:"seRivalItmCnt",cateId:cateId,device:device,sellerType:sellerType,indexCode:"seRivalItmCnt,seGuidePayRate,rivalItmAvgSeGuidePayRate",token:token};
    }
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>         &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入商品名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        exportdata=exportdata1['data'];
        var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
  
        $("#device_title").text(len);
        if(item_type=='顾客流失竞品推荐'){
          arr_all =formattingData(exportdata,["cartLosByrCnt"],""); 
        }else{
          arr_all =formattingData(exportdata,["seRivalItmCnt"],""); 
        }
        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                 
                temp_arr={};
                temp_arr["order"]=i+1;
                temp_arr["time"]=now_date;
                temp_arr["item_title"]=data[i]["item"]["title"];
                temp_arr["pictureUrl"]=data[i]["item"]["pictUrl"];  //商品图片
                temp_arr["item_userId"]=data[i]["item"]["itemId"]; //商品ID
                temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品url
                if(item_type=='顾客流失竞品推荐'){
                  temp_arr["cartLosByrCnt"]=data[i]["cartLosByrCnt"]["value"]; //加购后流失人数
                  temp_arr["cltLosByrCnt"]=data[i]["cltLosByrCnt"]["value"]; //收藏后流失人数
                  temp_arr["cltJmpByrCnt"]=data[i]["cltJmpByrCnt"]["value"]; //收藏后跳失人数
                  temp_arr["cartJmpByrCnt"]=data[i]["cartJmpByrCnt"]["value"]; //加购后跳失人数
                  temp_arr["losItmCnt"]=data[i]["losItmCnt"]["value"]; //引起流失的商品数
                  temp_arr["directLosCnt"]=data[i]["directLosCnt"]["value"]; //直接跳失人数
                  temp_arr["losShopCnt"]=data[i]["directLosCnt"]["value"]; //引起流失的店铺数
                  temp_arr["cartLosByrCnt"]=data[i]["payLostAmt"]["value"]; //流失金额
                  temp_arr["losByrCnt"]=data[i]["losByrCnt"]["value"]; //流失人数
                  temp_arr["losRate"]=decimal((data[i]["losRate"]["value"])*100,2); //流失率
                }else{
                    temp_arr["seRivalItmCnt"]=data[i]["seRivalItmCnt"]["value"]; //搜索竞争商品数
                  if(db_chick=='搜索引导访客数'){
                    temp_arr["seGuideUv"]=data[i]["seGuideUv"]["value"]; //本店商品搜索引导访客数
                    temp_arr["rivalItmAvgSeGuideUv"]=data[i]["rivalItmAvgSeGuideUv"]["value"]; //竞品平均搜索引导访客数
                  }else if(db_chick=='搜索引导加购人数'){
                    temp_arr["rivalItmAvgSeGuideCartByrCnt"]=data[i]["rivalItmAvgSeGuideCartByrCnt"]["value"]; //竞品平均搜索引导加购人数
                    temp_arr["seGuideCartByrCnt"]=data[i]["seGuideCartByrCnt"]["value"]; //本店商品搜索引导加购人数
                  }else if(db_chick=='搜索引导支付买家数'){
                    temp_arr["rivalItmAvgSeGuidePayByrCnt"]=data[i]["rivalItmAvgSeGuidePayByrCnt"]["value"]; //竞品平均搜索引导支付买家数
                    temp_arr["seGuidePayByrCnt"]=data[i]["seGuidePayByrCnt"]["value"]; //本店商品搜索引导支付买家数
                  }else if(db_chick=='搜索引导支付转化率'){
                    temp_arr["rivalItmAvgSeGuidePayRate"]=decimal((data[i]["rivalItmAvgSeGuidePayRate"]["value"])*100,2); //竞品平均搜索引导支付转化率
                    temp_arr["seGuidePayRate"]=decimal((data[i]["seGuidePayRate"]["value"])*100,2); //本店商品搜索引导支付转化率
                  }
                  
                }
                tab_data.push(temp_arr);
              }                           

              exportData=[];
              if(item_type=='顾客流失竞品推荐'){
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '竞品识别-顾客流失竞品推荐'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'item_title', title: '商品名称',width: "8%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                      ,{field: 'pictureUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
                        if (d.pictureUrl!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'item_userId', title: '商品ID', sort: true,width: "4%"}

                      ,{field:'cartLosByrCnt', width: "7%", title: '流失金额', sort: true}
                      ,{field:'losByrCnt', width: "7%", title: '流失人数', sort: true}
                      ,{field:'losRate', width: "7%", title: '流失率(%)', sort: true}

                      ,{field:'cartLosByrCnt', width: "8%", title: '收藏后流失人数', sort: true}
                      ,{field:'cltLosByrCnt', width: "8%", title: '加购后流失人数', sort: true}
                      ,{field:'cltJmpByrCnt', width: "8%", title: '收藏后跳失人数', sort: true}
                      ,{field:'cartJmpByrCnt', width: "8%", title: '加购后跳失人数', sort: true}
                      ,{field:'directLosCnt', width: "8%", title: '直接跳失人数', sort: true}
                      ,{field:'losItmCnt', width: "8%", title: '引起流失的商品数', sort: true}
                      ,{field:'losShopCnt', width: "8%", title: '引起流失的店铺数', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                });
              }else{
                if(db_chick=='搜索引导访客数'){
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '竞品识别-搜索流失竞品推荐'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"10%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                        ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
                        ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
                        ,{field:'seGuideUv', width: "15%", title: '本店商品搜索引导访客数', sort: true}
                        ,{field:'rivalItmAvgSeGuideUv', width: "15%", title: '竞品平均搜索引导访客数', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }else if(db_chick=='搜索引导加购人数'){
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '竞品识别-搜索流失竞品推荐'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"10%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                        ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
                        ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
                        ,{field:'rivalItmAvgSeGuideCartByrCnt', width: "15%", title: '竞品平均搜索引导加购人数', sort: true}
                        ,{field:'seGuideCartByrCnt', width: "15%", title: '本店商品搜索引导加购人数', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }else if(db_chick=='搜索引导支付买家数'){
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '竞品识别-搜索流失竞品推荐'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"10%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                        ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
                        ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
                        ,{field:'rivalItmAvgSeGuidePayByrCnt', width: "15%", title: '竞品平均搜索引导支付买家数', sort: true}
                        ,{field:'seGuidePayByrCnt', width: "15%", title: '本店商品搜索引导支付买家数', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }else if(db_chick=='搜索引导支付转化率'){
                  var ins1= table.render({
                      elem: '#test'
                      ,id: 'testReload'
                      ,title: '竞品识别-搜索流失竞品推荐'
                      ,data:tab_data
                      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                      ,limit:10
                      ,height:472
                      ,cols: [[
                        {field:'order', width:"10%", title: '#' ,sort: false}
                        ,{field:'time', width:"15%", title: '日期', sort: true}
                        ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
                        ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                          if (d.pictureUrl!=undefined) {
                            return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                          }else{
                            return '<div style="text-align: center;" ></div>'; 
                          }
                        }}
                        ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
                        ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
                        ,{field:'rivalItmAvgSeGuidePayRate', width: "15%", title: '竞品平均搜索引导支付转化率(%)', sort: true}
                        ,{field:'seGuidePayRate', width: "15%", title: '本店商品搜索引导支付转化率(%)', sort: true}
                      ]],  
                      page:  {limits: [10, 20, 50, 100, 300,500]},
                      done: function (res, curr, count) {
                        exportData=res.data;
                      }
                  });
                }
              }
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
//监控品牌
$(document).on('click','#jkpp',function(e){
  //关键指数对比
  //先获取类型
  selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

  time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

  dateType="day";
  if (time_chinese_type=="月") {
   dateType="month";
  }else if(time_chinese_type=="周"){
   dateType="week";
  }else if(time_chinese_type=="日"){
   dateType="day";
  }else if(time_chinese_type=="30天"){
   dateType="recent30";
  }else if(time_chinese_type=="7天"){
   dateType="recent7";
  }else if(time_chinese_type=="实 时"){
    dateType="today";
  }

  dateRange_text =  $(".oui-date-picker-current-date").text();
  dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

  if (time_chinese_type=="日") {
    // console.log(dateRange_text);
    date_start=dateRange_text;
    date_end=dateRange_text;
  }else if(time_chinese_type=="实 时"){
    var myDate = new Date;
    var year = myDate.getFullYear(); //获取当前年
    var mon = myDate.getMonth() + 1; //获取当前月
    var date = myDate.getDate();
    date_start=year+'-'+mon+'-'+date;
    date_end=year+'-'+mon+'-'+date;
    dateRange_text=date_end;
  }else{
    dateRange_arr= dateRange_text.split("~");
    date_start=dateRange_arr[0];
    date_end=dateRange_arr[1];
  }


  device_title = $(".oui-select-container-value").text();

  device=0;
  if (device_title=="所有终端") {
    device=0;
  }else if(device_title=="PC"){
    device=1;
  }else if(device_title=="无线"){
    device=2;
  }



  sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

  sellerType="-1";
  if (sellerType_title=="全部") {
  sellerType="-1";
  }else if(sellerType_title=="天猫"){
  sellerType="1";
  }else if(sellerType_title=="淘宝"){
  sellerType="0";
  }   

  // item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  

  default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


  //请求数据接口
  //打开导出的表格
    
  time_chinese_type =  $(".oui-card-link").text();
  href = window.location.href;

  lm = href.indexOf('cateId=');
  href_two = href.substring(lm+'cateId='.length,href.length);   
  lm2 = href_two.indexOf('&');
  cateId = href_two.substring(0,lm2);
  item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
  click_on=$('.oui-radio.ant-radio-wrapper.ant-radio-wrapper-checked').eq(1).find('span');

  id="jkppExport";
  orderBy="orderBy";
  if(dateType=='today'){
    url="https://sycm.taobao.com/mc/live/ci/brand/monitor/list.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex",token:token};
  }else{
    url="https://sycm.taobao.com/mc/ci/brand/monitor/list.json";  //
    data_info={_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"1000",page:"1",order:"desc",orderBy:"tradeIndex",cateId:cateId,device:device,sellerType:sellerType,indexCode:"cateRankId,tradeIndex",token:token,type:'all'};
  }

  data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="exportdc" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入品牌名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
  layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
  });

  l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
  });
   $.ajax({
      type:'GET',
      url:url,  
      data:data_info,
      dataType:'json',
      beforeSend: function (request) {   
           request.setRequestHeader("transit-id",transit_id);
      },
      success:function(result)
      {  
        exportdata1=sycm.decrypt(result["data"]);
        
        if(dateType=='today'){
          exportdata=exportdata1['data']['data'];
          $('#dateRange_text').text(exportdata1['updateTime']);
          var now_date=date_start=exportdata1['updateTime'];
        }else{
          exportdata=exportdata1['data'];
          var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
        }
        // console.log(exportdata);
        // return false;
        //利用冒泡排序进行排序按照店铺排名
         var len = exportdata.length;  
         //  for (var i = 0; i < len; i++) {  
         //      for(var j = 0; j < len - i -1; j++){  
         //          if(exportdata[j]["tradeIndex"]["value"] >exportdata[j+1]["tradeIndex"]["value"]){  //相邻元素进行对比  
         //              var temp = exportdata[j+1];//交换元素  
         //              exportdata[j+1] = exportdata[j];  
         //              exportdata[j] = temp;  
         //          }  
         //      }  
         //  }   
        $("#device_title").text(len);
        if(dateType=='today'){
          arr_all =formattingData(exportdata,["tradeIndex"],""); 
        }else{
          
          arr_all =formattingData(exportdata,["tradeIndex",'cartHits','seIpvUvHits','uvIndex','cltHits'],"payRateIndex"); 
        }
        

        $.ajax({
            type:'post',
            url:"https://work.dinghaiec.com/set_save.php",  
            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
            dataType:'json',
            async: false,
            beforeSend: function (request) {     
            },
            success:function(result)
            {  
              data=exportdata;
              data2 = result["arr_all"];
              xtime_arr=[];
              tab_data=[];
              c=1;
              for (var i = 0; i < data.length; i++) 
              {
                 
                temp_arr={};
                temp_arr["order"]=i+1;
                temp_arr["time"]=now_date;
                temp_arr["page_name"]=sellerType_title;
                temp_arr["source"]= device_title;  //来源
                temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
                temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
                temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID
                if(dateType=='today'){
                  if(data2["tradeIndex"][i]){
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["tradeIndex"]=0;// 交易额
                  }
                  
                }else{
                  if(data2["tradeIndex"][i]){
                    temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
                  }else{
                    temp_arr["tradeIndex"]=0;// 交易额
                  }
                  
                  temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
                  temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
                  temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
                  temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏人数
                  temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购人数
                  temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
                  temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
                  temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
                  temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值

                  temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
                  if(temp_arr["payNum"]==0){
                    temp_arr["kdj"]=0;
                  }else{
                    temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
                  }
                }
                tab_data.push(temp_arr);
              }                           

              exportData=[];
              if(dateType=='today'){
                 var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控品牌-品牌列表'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"10%", title: '#' ,sort: false}
                      ,{field:'time', width:"15%", title: '日期', sort: true}
                      ,{field:'page_name', width:"10%", title: '平台'}
                      ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'brandName', title: '品牌名称',width: "15%"}
                      ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
                        if (d.logo!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'brandId', title: '品牌ID', sort: true,width: "15%"}
                      ,{field:'tradeIndex', width: "15%", title: '交易金额', sort: true}
                      
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
              }else{
                var ins1= table.render({
                    elem: '#test'
                    ,id: 'testReload'
                    ,title: '监控品牌-品牌列表'
                    ,data:tab_data
                    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                    ,limit:10
                    ,height:472
                    ,cols: [[
                      {field:'order', width:"2%", title: '#' ,sort: false}
                      ,{field:'time', width:"5%", title: '日期', sort: true}
                      ,{field:'page_name', width:"4%", title: '平台'}
                      ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                      ,{field:'brandName', title: '品牌名称',width: "8%"}
                      ,{field: 'logo', width: "5%", title: '品牌LOGO',  sort: false,templet:function (d) { 
                        if (d.logo!=undefined) {
                          return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                        }else{
                          return '<div style="text-align: center;" ></div>'; 
                        }
                      }}
                      ,{field:'brandId', title: '品牌ID', sort: true,width: "4%"}
                      ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
                      ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
                      ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
                      ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
                      ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
                      ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
                      ,{field:'payNum', width: "5%", title: '买家数', sort: true}
                      ,{field:'kdj', width: "5%", title: '客单价', sort: true}
                      ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
                      ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
                      ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
                      ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
                    ]],  
                    page:  {limits: [10, 20, 50, 100, 300,500]},
                    done: function (res, curr, count) {
                      exportData=res.data;
                    }
                  });
              }
              
              document.getElementById("exportdc").onclick = function(){table.exportFile(ins1.config.id,exportData,'xls')};

              layer.close(l_index);
            }
        });    
      }
   });
})
$(document).on('click',"#export1", function(e) { 

//先获取类型

selectedTitle = $(".menuItem.level-leaf.selected.false.leaf").text();

time_chinese_type =  $(".ant-btn.oui-canary-btn.ant-btn-primary.ant-btn-sm").find("span").text();

dateType="day";
if (time_chinese_type=="月") {
 dateType="month";
}else if(time_chinese_type=="周"){
 dateType="week";
}else if(time_chinese_type=="日"){
 dateType="day";
}else if(time_chinese_type=="30天"){
 dateType="recent30";
}else if(time_chinese_type=="7天"){
 dateType="recent7";
}

dateRange_text =  $(".oui-date-picker-current-date").text();
dateRange_text = Trim(dateRange_text.substring(4,dateRange_text.length),"g");

if (time_chinese_type=="日") {
// console.log(dateRange_text);
date_start=dateRange_text;
date_end=dateRange_text;
}else{
dateRange_arr= dateRange_text.split("~");
date_start=dateRange_arr[0];
date_end=dateRange_arr[1];
}


device_title = $(".oui-select-container-value").text();

device=0;
if (device_title=="所有终端") {
  device=0;
}else if(device_title=="PC"){
  device=1;
}else if(device_title=="无线"){
  device=2;
}



sellerType_title = $(".ebase-FaCommonFilter__right").find(".ant-select-selection-selected-value").text();

sellerType="-1";
if (sellerType_title=="全部") {
sellerType="-1";
}else if(sellerType_title=="天猫"){
sellerType="1";
}else if(sellerType_title=="淘宝"){
sellerType="0";
}   



default_title = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();


//请求数据接口
//打开导出的表格
  
time_chinese_type =  $(".oui-card-link").text();
href = $(".oui-card-link").find("a").attr("href");

lm = href.indexOf('cateId=');
href_two = href.substring(lm+'cateId='.length,href.length);   
lm2 = href_two.indexOf('&');
cateId = href_two.substring(0,lm2);
// console.log(cateId);

item_type = $(".ebase-Switch__item.ebase-Switch__activeItem").text();
item_type2 = $(".oui-tab-switch-item.oui-tab-switch-item-active.default").text();

  id="czExport1";
  orderBy="orderBy";
  if (item_type=="店铺"&&item_type2=="高交易") {
    url="https://sycm.taobao.com/mc/mq/mkt/rank/shop/hotsale.json";  //店铺  高交易
    orderBy="tradeIndex";
    indexCode="tradeIndex,tradeGrowthRange,payRateIndex";
    id="czExport1";
  }else if(item_type=="店铺"&&item_type2=="高流量"){
    url="https://sycm.taobao.com/mc/mq/mkt/rank/shop/hotsearch.json"  //店铺  //高流量
    orderBy="uvIndex";
    indexCode="uvIndex,seIpvUvHits,tradeIndex";
    id="czExport2";
  }else if(item_type=="商品"&&item_type2=="高交易"){
    url="https://sycm.taobao.com/mc/mq/mkt/rank/item/hotsale.json";  //商品
    indexCode="tradeIndex,tradeGrowthRange,payRateIndex";
    id="czExport3";
  }else if(item_type=="商品"&&item_type2=="高流量"){
    url="https://sycm.taobao.com/mc/mq/mkt/rank/item/hotsearch.json";  //商品
    indexCode="uvIndex,seIpvUvHits,tradeIndex";
    id="czExport4";
  }else if(item_type=="商品"&&item_type2=="高意向"){
    url="https://sycm.taobao.com/mc/mq/mkt/rank/item/hotpurpose.json";  //商品
    indexCode="cltHits,cartHits,tradeIndex";
    id="czExport5";
  }else if(item_type=="品牌"&&item_type2=="高交易"){
    url="https://sycm.taobao.com/mc/mq/mkt/rank/brand/hotsale.json";  //商品
    indexCode="tradeIndex,tradeGrowthRange,payRateIndex";
    id="czExport6";
  }else if(item_type=="品牌"&&item_type2=="高流量"){
    url="https://sycm.taobao.com/mc/mq/mkt/rank/brand/hotsearch.json";  //商品
    indexCode="uvIndex,seIpvUvHits,tradeIndex";
    id="czExport7";
    orderBy="uvIndex";
  }


 data_html='<div><div class="el-dialog__header" > <p class="el-dialog__title" > '+selectedTitle+'-'+item_type+'-'+item_type2+' </p> </div><div class=" layui-form el-dialog__body " > <div class="layui-tab layui-tab-brief" ><div> 当前：<span id="device_title" >0</span>条数据    &nbsp;   统计时间：<span id="dateRange_text" >'+dateRange_text+'</span>   &nbsp;   终端：<span id="device_title" >'+device_title+'</span>  &nbsp; 平台：<span id="sellerType_title" >'+sellerType_title+'</span>       &nbsp; <button id="export" style="background-color: #409EFF;" class="layui-btn layui-btn-sm">导出数据</button><div class="demoTable" style="float: right; margin-right: 10px;"><input style="width: 180px;display: -webkit-inline-box;" type="text" name="shop_title" id="demoReload" autocomplete="off" placeholder="请输入店铺名称" class="layui-input"> <button  id="'+id+'" class="layui-btn stortSearch" data-type="reload">搜索</button></div></div></div> <table style=" margin-top: 10px;" class="layui-hide" id="test" lay-filter="test"></table> </div></div>';
 layer.open({
    type: 1,
    title:false,
    area: ['90%', '90%'],
    shadeClose: true, //点击遮罩关闭
    content: data_html
});

   l_index = layer.load(1, {
    shade: [0.5,'#DBDBDB'] //0.1透明度的白色背景
   });

     $.ajax({
              type:'GET',
              url:url,  
              data:{_:time,dateRange:date_start+'|'+date_end,dateType:dateType,pageSize:"10",page:"1",order:"desc",orderBy:orderBy,    
              cateId:cateId,device:device,sellerType:sellerType,indexCode:indexCode,token:token},
              dataType:'json',
              beforeSend: function (request) {   
                   request.setRequestHeader("transit-id",transit_id);
              },
              success:function(result)
              {  

                exportdata=sycm.decrypt(result["data"]);

                 //利用冒泡排序进行排序按照店铺排名
                   var len = exportdata.length;  
                    for (var i = 0; i < len; i++) {  
                        for(var j = 0; j < len - i -1; j++){  
                            if(exportdata[j]["cateRankId"]["value"] >exportdata[j+1]["cateRankId"]["value"]){  //相邻元素进行对比  
                                var temp = exportdata[j+1];//交换元素  
                                exportdata[j+1] = exportdata[j];  
                                exportdata[j] = temp;  
                            }  
                        }  
                    }  

                    $("#device_title").text(len);

                    if (item_type=="店铺"&&default_title=="高交易") 
                    {
                        arr_all =formattingData(exportdata,["tradeIndex"],"payRateIndex");

                          $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {  
                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];

                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["shop_title"]=exportdata[i]["shop"]["title"];
                                      temp_arr["pictureUrl"]=exportdata[i]["shop"]["pictureUrl"];  //店铺图片
                                      temp_arr["shop_userId"]=exportdata[i]["shop"]["userId"]; //访客
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                                      temp_arr["payRateIndex"]=decimal(exportdata2["zhl"][i]["0"]*100,2);// 转化率
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"3%", title: '#' ,sort: false}
                                            ,{field:'time', width:"7%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"8%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "8%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'shop_title', title: '店铺名称',width: "10%" }
                                            ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                                              if (d.pictureUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'shop_userId', title: '店铺ID', sort: true,width: "9%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'tradeIndex', width: "12%", title: '交易金额', sort: true}
                                            ,{field:'tradeGrowthRange', width: "12%", title: '交易增值幅度(%)', sort: true}
                                            ,{field:'payRateIndex', width: "12%", title: '转化率(%)', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                        layer.close(l_index);

                                          $("#export").click(function(){
                                            table.exportFile(ins1.config.id,exportData_temp,'xls');
                                         })

                                  }
                              });
                    }else if(item_type=="店铺"&&default_title=="高流量")
                    {
                      arr_all =formattingData(exportdata,["seIpvUvHits","tradeIndex","uvIndex"],"");

                         $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {  
                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];
                                    // console.log(data2);
                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["shop_title"]=exportdata[i]["shop"]["title"];
                                      temp_arr["pictureUrl"]=exportdata[i]["shop"]["pictureUrl"];  //店铺图片
                                      temp_arr["shop_userId"]=exportdata[i]["shop"]["userId"]; //店铺ID
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["uvIndex"]=decimal(exportdata2["uvIndex"][i]["0"],0);// 访客数
                                      temp_arr["seIpvUvHits"]=decimal(exportdata2["seIpvUvHits"][i]["0"],0);// 搜索人数
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      temp_arr["searchProportion"]=decimal((exportdata2["seIpvUvHits"][i]["0"]/exportdata2["uvIndex"][i]["0"])*100,2);// 搜索占比
                                      temp_arr["uvCost"]=decimal(exportdata2["tradeIndex"][i]["0"]/exportdata2["uvIndex"][i]["0"],2);// UV价值
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"3%", title: '#' ,sort: false}
                                            ,{field:'time', width:"8%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"6%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'shop_title', title: '店铺名称',width: "11%" }
                                            ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                                              if (d.pictureUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'shop_userId', title: '店铺ID', sort: true,width: "8%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'uvIndex', width: "8%", title: '访客数', sort: true}
                                            ,{field:'seIpvUvHits', width: "8%", title: '搜索人数', sort: true}
                                            ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
                                            ,{field:'searchProportion', width: "8%", title: '搜索占比(%)', sort: true}
                                            ,{field:'uvCost', width: "8%", title: 'UV价值', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                       $("#export").click(function(){
                                              table.exportFile(ins1.config.id,exportData_temp,'xls');
                                           })
                                        layer.close(l_index);
                                  }
                              });

                    }else if(item_type=="商品"&&default_title=="高交易")
                    {
                        arr_all =formattingData(exportdata,["tradeIndex"],"payRateIndex");

                          $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {  

                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];

                                    // console.log(exportdata);

                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["item_title"]=exportdata[i]["item"]["title"];
                                      temp_arr["pictUrl"]=exportdata[i]["item"]["pictUrl"];  //商品图片
                                      temp_arr["itemId"]=exportdata[i]["item"]["itemId"]; //商品id
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                                      temp_arr["payRateIndex"]=decimal(exportdata2["zhl"][i]["0"]*100,2);// 转化率
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"3%", title: '#' ,sort: false}
                                            ,{field:'time', width:"8%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"6%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "8%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'item_title', title: '商品名称',width: "12%",templet:function (d) { 
                                              if (d.item_title!=undefined) {
                                                return '<div style="text-align: left;"> <a  style="    outline: none; text-decoration: none; color: #72afd2;" target="view_window"   href="https://item.taobao.com/item.htm?id='+d.itemId+'" > '+d.item_title+' </a> </div>';
                                              }else{
                                                return '<div style="text-align: left;" ></div>'; 
                                              }
                                            } }
                                            ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                                              if (d.pictUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'itemId', title: '商品ID', sort: true,width: "9%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'tradeIndex', width: "12%", title: '交易金额', sort: true}
                                            ,{field:'tradeGrowthRange', width: "12%", title: '交易增值幅度(%)', sort: true}
                                            ,{field:'payRateIndex', width: "12%", title: '转化率(%)', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                        layer.close(l_index);

                                          $("#export").click(function(){
                                            table.exportFile(ins1.config.id,exportData_temp,'xls');
                                         })

                                  }
                              });
                    
                    }else if(item_type=="商品"&&default_title=="高流量")
                    {
                      arr_all =formattingData(exportdata,["seIpvUvHits","tradeIndex","uvIndex"],"");

                         $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {  
                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];
                                    // console.log(data2);
                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["item_title"]=exportdata[i]["item"]["title"];  //商品标题
                                      temp_arr["pictUrl"]=exportdata[i]["item"]["pictUrl"];  //商品图片
                                      temp_arr["itemId"]=exportdata[i]["item"]["itemId"]; //商品id
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["uvIndex"]=decimal(exportdata2["uvIndex"][i]["0"],0);// 访客数
                                      temp_arr["seIpvUvHits"]=decimal(exportdata2["seIpvUvHits"][i]["0"],0);// 搜索人数
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      temp_arr["searchProportion"]=decimal((exportdata2["seIpvUvHits"][i]["0"]/exportdata2["uvIndex"][i]["0"])*100,2);// 搜索占比
                                      temp_arr["uvCost"]=decimal(exportdata2["tradeIndex"][i]["0"]/exportdata2["uvIndex"][i]["0"],2);// UV价值
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"4%", title: '#' ,sort: false}
                                            ,{field:'time', width:"7%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"4%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'item_title', title: '商品名称',width: "12%",templet:function (d) { 
                                              if (d.item_title!=undefined) {
                                                return '<div style="text-align: left;"> <a  style="    outline: none; text-decoration: none; color: #72afd2;" target="view_window"   href="https://item.taobao.com/item.htm?id='+d.itemId+'" > '+d.item_title+' </a> </div>';
                                              }else{
                                                return '<div style="text-align: left;" ></div>'; 
                                              }
                                            }}
                                            ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                                              if (d.pictUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'itemId', title: '商品ID', sort: true,width: "9%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'uvIndex', width: "8%", title: '访客数', sort: true}
                                            ,{field:'seIpvUvHits', width: "8%", title: '搜索人数', sort: true}
                                            ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
                                            ,{field:'searchProportion', width: "8%", title: '搜索占比(%)', sort: true}
                                            ,{field:'uvCost', width: "8%", title: 'UV价值', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                       $("#export").click(function(){
                                              table.exportFile(ins1.config.id,exportData_temp,'xls');
                                           })
                                        layer.close(l_index);
                                  }
                              });

                    }else if(item_type=="商品"&&default_title=="高意向")
                    {
                      arr_all =formattingData(exportdata,["cartHits","cltHits","tradeIndex"],"");

                         $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {  
                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];
                                    // console.log(data2);
                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["item_title"]=exportdata[i]["item"]["title"];  //商品标题
                                      temp_arr["pictUrl"]=exportdata[i]["item"]["pictUrl"];  //商品图片
                                      temp_arr["itemId"]=exportdata[i]["item"]["itemId"]; //商品id
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["cltHits"]=decimal(exportdata2["cltHits"][i]["0"],0);// 收藏人数
                                      temp_arr["cartHits"]=decimal(exportdata2["cartHits"][i]["0"],0);// 加购人数
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"4%", title: '#' ,sort: false}
                                            ,{field:'time', width:"7%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"4%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'item_title', title: '商品名称',width: "28%",templet:function (d) { 
                                              if (d.item_title!=undefined) {
                                                return '<div style="text-align: left;"> <a  style="    outline: none; text-decoration: none; color: #72afd2;" target="view_window"   href="https://item.taobao.com/item.htm?id='+d.itemId+'" > '+d.item_title+' </a> </div>';
                                              }else{
                                                return '<div style="text-align: left;" ></div>'; 
                                              }
                                            }}
                                            ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                                              if (d.pictUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'itemId', title: '商品ID', sort: true,width: "9%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'cltHits', width: "8%", title: '收藏人数', sort: true}
                                            ,{field:'cartHits', width: "8%", title: '加购人数', sort: true}
                                            ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                       $("#export").click(function(){
                                              table.exportFile(ins1.config.id,exportData_temp,'xls');
                                           })
                                        layer.close(l_index);
                                  }
                              });

                    }else if (item_type=="品牌"&&default_title=="高交易") 
                    {


                        arr_all =formattingData(exportdata,["tradeIndex"],"payRateIndex");

                          $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {   
                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];

                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["shop_title"]=exportdata[i]["brandModel"]["brandName"];
                                      temp_arr["pictureUrl"]=exportdata[i]["brandModel"]["logo"];  //店铺图片
                                      temp_arr["shop_userId"]=exportdata[i]["brandModel"]["brandId"]; //访客
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
                                      temp_arr["payRateIndex"]=decimal(exportdata2["zhl"][i]["0"]*100,2);// 转化率
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"3%", title: '#' ,sort: false}
                                            ,{field:'time', width:"7%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"8%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "8%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'shop_title', title: '品牌名称',width: "10%" }
                                            ,{field: 'pictureUrl', width: "10%", title: '品牌图片',  sort: false,templet:function (d) { 
                                              if (d.pictureUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.pictureUrl+'_120x120.jpg"></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'shop_userId', title: '品牌ID', sort: true,width: "9%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'tradeIndex', width: "12%", title: '交易金额', sort: true}
                                            ,{field:'tradeGrowthRange', width: "12%", title: '交易增值幅度(%)', sort: true}
                                            ,{field:'payRateIndex', width: "12%", title: '转化率(%)', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                        layer.close(l_index);

                                          $("#export").click(function(){
                                            table.exportFile(ins1.config.id,exportData_temp,'xls');
                                         })

                                  }
                              });
                    }else if(item_type=="品牌"&&default_title=="高流量")
                    {
                      arr_all =formattingData(exportdata,["seIpvUvHits","tradeIndex","uvIndex"],"");

                         $.ajax({
                            type:'post',
                            url:"https://work.dinghaiec.com/set_save.php",  
                            data:{state:15,arr:JSON.stringify(arr_all),method:"decode"},
                            dataType:'json',
                            async: false,
                            beforeSend: function (request) {     
                            },
                            success:function(result)
                            {  
                                    exportdata2 = result["arr_all"];
                                    xtime_arr=[];
                                    tab_data=[];
                                    // console.log(data2);
                                    for (var i = 0; i < exportdata.length; i++) 
                                    {
                                      temp_arr={};
                                      temp_arr["order"]=i+1;
                                      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
                                      temp_arr["page_name"]=sellerType_title;
                                      temp_arr["source"]= device_title;  //来源
                                      temp_arr["shop_title"]=exportdata[i]["brandModel"]["brandName"];
                                      temp_arr["pictureUrl"]=exportdata[i]["brandModel"]["logo"];  //店铺图片
                                      temp_arr["shop_userId"]=exportdata[i]["brandModel"]["brandId"]; //访客
                                      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
                                      temp_arr["uvIndex"]=decimal(exportdata2["uvIndex"][i]["0"],0);// 访客数
                                      temp_arr["seIpvUvHits"]=decimal(exportdata2["seIpvUvHits"][i]["0"],0);// 搜索人数
                                      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
                                      temp_arr["searchProportion"]=decimal((exportdata2["seIpvUvHits"][i]["0"]/exportdata2["uvIndex"][i]["0"])*100,2);// 搜索占比
                                      temp_arr["uvCost"]=decimal(exportdata2["tradeIndex"][i]["0"]/exportdata2["uvIndex"][i]["0"],2);// UV价值
                                      tab_data.push(temp_arr);
                                    }                           

                                      exportData_temp=[];
                                      var ins1= table.render({
                                          elem: '#test'
                                          ,id: 'testReload'
                                          // ,initSort: {field:'tradeIndex', type:'desc'}
                                          ,data:tab_data
                                          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                                          ,limit:10
                                          ,height:472
                                          ,cols: [[
                                            {field:'cateRankId', width:"3%", title: '#' ,sort: false}
                                            ,{field:'time', width:"8%", title: '日期', sort: true}
                                            ,{field:'page_name', width:"6%", title: '平台'}
                                            ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                                            ,{field:'shop_title', title: '品牌名称',width: "10%" }
                                            ,{field: 'pictureUrl', width: "10%", title: '品牌图片',  sort: false,templet:function (d) { 
                                              if (d.pictureUrl!=undefined) {
                                                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.pictureUrl+'_120x120.jpg"></div>';
                                              }else{
                                                return '<div style="text-align: center;" ></div>'; 
                                              }
                                            }}
                                            ,{field:'shop_userId', title: '品牌ID', sort: true,width: "9%"}
                                            ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
                                            ,{field:'uvIndex', width: "8%", title: '访客数', sort: true}
                                            ,{field:'seIpvUvHits', width: "8%", title: '搜索人数', sort: true}
                                            ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
                                            ,{field:'searchProportion', width: "8%", title: '搜索占比(%)', sort: true}
                                            ,{field:'uvCost', width: "8%", title: 'UV价值', sort: true}
                                          ]],  
                                          page:  {limits: [10, 20, 50, 100, 300,500]},
                                         done: function (res, curr, count) {
                                            exportData_temp=res.data;
                                        }
                                        });

                                       $("#export").click(function(){
                                              table.exportFile(ins1.config.id,exportData_temp,'xls');
                                           })
                                        layer.close(l_index);
                                  }
                              });

                    }   


              }
     });




});



$(document).on('click',".stortSearch", function(e) { 

 demoReload =  $("#demoReload").val();

id=$(this).attr("id");

//高交易
if (id=="czExport1") {

  //重新加载
  tab_data2=[];
  for (var i = 0; i < exportdata.length; i++) {
  if (exportdata[i]["shop"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
  temp_arr={};
  temp_arr["order"]=i+1;
  temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
  temp_arr["page_name"]=sellerType_title;
  temp_arr["source"]= device_title;  //来源
  temp_arr["shop_title"]=exportdata[i]["shop"]["title"];
  temp_arr["pictureUrl"]=exportdata[i]["shop"]["pictureUrl"];  //店铺图片
  temp_arr["shop_userId"]=exportdata[i]["shop"]["userId"]; //访客
  temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
  temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
  temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
  temp_arr["payRateIndex"]=decimal(exportdata2["zhl"][i]["0"]*100,2);// 转化率
  tab_data2.push(temp_arr);
  }
  }                    


    table.render
    ({
    elem: '#test'
    ,id: 'testReload'
    // ,initSort: {field:'tradeIndex', type:'desc'}
    ,data:tab_data2
    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
    ,limit:10
    ,height:472
    ,cols: [[
    {field:'cateRankId', width:"4%", title: '#' ,sort: false}
    ,{field:'time', width:"7%", title: '日期', sort: true}
    ,{field:'page_name', width:"8%", title: '平台'}
    ,{field:'source', title: '终端', width: "8%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
    ,{field:'shop_title', title: '店铺名称',width: "10%" }
    ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
    if (d.pictureUrl!=undefined) {
    return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
    }else
    {
    return '<div style="text-align: center;" ></div>'; 
    }
    }}
    ,{field:'shop_userId', title: '店铺ID', sort: true,width: "9%"}
    ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
    ,{field:'tradeIndex', width: "12%", title: '交易金额', sort: true}
    ,{field:'tradeGrowthRange', width: "12%", title: '交易增值幅度(%)', sort: true}
    ,{field:'payRateIndex', width: "12%", title: '转化率(%)', sort: true}
    ]],  
    page:  {limits: [10, 20, 50, 100, 300,500]},
      done: function (res, curr, count) 
      {
      exportData_temp=res.data;
      }
    });


//高流量
}else if(id=="czExport2"){

  //重新加载
  tab_data=[];
  for (var i = 0; i < exportdata.length; i++) {
    if (exportdata[i]["shop"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
      temp_arr={};
      temp_arr["order"]=i+1;
      temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
      temp_arr["page_name"]=sellerType_title;
      temp_arr["source"]= device_title;  //来源
      temp_arr["shop_title"]=exportdata[i]["shop"]["title"];
      temp_arr["pictureUrl"]=exportdata[i]["shop"]["pictureUrl"];  //店铺图片
      temp_arr["shop_userId"]=exportdata[i]["shop"]["userId"]; //店铺ID
      temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
      temp_arr["uvIndex"]=decimal(exportdata2["uvIndex"][i]["0"],0);// 访客数
      temp_arr["seIpvUvHits"]=decimal(exportdata2["seIpvUvHits"][i]["0"],0);// 搜索人数
      temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
      temp_arr["searchProportion"]=decimal((exportdata2["seIpvUvHits"][i]["0"]/exportdata2["uvIndex"][i]["0"])*100,2);// 搜索占比
      temp_arr["uvCost"]=decimal(exportdata2["tradeIndex"][i]["0"]/exportdata2["uvIndex"][i]["0"],2);// UV价值
      tab_data.push(temp_arr);
    }
  }                    

  table.render({
  elem: '#test'
  ,id: 'testReload'
  // ,initSort: {field:'tradeIndex', type:'desc'}
  ,data:tab_data
  ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
  ,limit:10
  ,height:472
  ,cols: [[
  {field:'cateRankId', width:"3%", title: '#' ,sort: false}
  ,{field:'time', width:"8%", title: '日期', sort: true}
  ,{field:'page_name', width:"6%", title: '平台'}
  ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
  ,{field:'shop_title', title: '店铺名称',width: "11%" }
  ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
  if (d.pictureUrl!=undefined) {
    return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
  }else{
    return '<div style="text-align: center;" ></div>'; 
  }
  }}
  ,{field:'shop_userId', title: '店铺ID', sort: true,width: "8%"}
  ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
  ,{field:'uvIndex', width: "8%", title: '访客数', sort: true}
  ,{field:'seIpvUvHits', width: "8%", title: '搜索人数', sort: true}
  ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
  ,{field:'searchProportion', width: "8%", title: '搜索占比(%)', sort: true}
  ,{field:'uvCost', width: "8%", title: 'UV价值', sort: true}
  ]],  
  page:  {limits: [10, 20, 50, 100, 300,500]},
  done: function (res, curr, count) {
  exportData_temp=res.data;
  }
  });

}else if(id=="czExport3"){

  //重新加载
  tab_data=[];
  for (var i = 0; i < exportdata.length; i++) {
    if (exportdata[i]["shop"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
          temp_arr={};
          temp_arr["order"]=i+1;
          temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["item_title"]=exportdata[i]["item"]["title"];
          temp_arr["pictUrl"]=exportdata[i]["item"]["pictUrl"];  //商品图片
          temp_arr["itemId"]=exportdata[i]["item"]["itemId"]; //商品id
          temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
          temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
          temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
          temp_arr["payRateIndex"]=decimal(exportdata2["zhl"][i]["0"]*100,2);// 转化率
          tab_data.push(temp_arr);
    }
  }                    

    table.render({
    elem: '#test'
    ,id: 'testReload'
    // ,initSort: {field:'tradeIndex', type:'desc'}
    ,data:tab_data
    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
    ,limit:10
    ,height:472
    ,cols: [[
    {field:'cateRankId', width:"3%", title: '#' ,sort: false}
    ,{field:'time', width:"8%", title: '日期', sort: true}
    ,{field:'page_name', width:"6%", title: '平台'}
    ,{field:'source', title: '终端', width: "8%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
    ,{field:'item_title', title: '商品名称',width: "12%",templet:function (d) { 
    if (d.item_title!=undefined) {
      return '<div style="text-align: left;"> <a  style="    outline: none; text-decoration: none; color: #72afd2;" target="view_window"   href="https://item.taobao.com/item.htm?id='+d.itemId+'" > '+d.item_title+' </a> </div>';
    }else{
      return '<div style="text-align: left;" ></div>'; 
    }
    } }
    ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
    if (d.pictUrl!=undefined) {
      return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
    }else{
      return '<div style="text-align: center;" ></div>'; 
    }
    }}
    ,{field:'itemId', title: '商品ID', sort: true,width: "9%"}
    ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
    ,{field:'tradeIndex', width: "12%", title: '交易金额', sort: true}
    ,{field:'tradeGrowthRange', width: "12%", title: '交易增值幅度(%)', sort: true}
    ,{field:'payRateIndex', width: "12%", title: '转化率(%)', sort: true}
    ]],  
    page:  {limits: [10, 20, 50, 100, 300,500]},
    done: function (res, curr, count) {
    exportData_temp=res.data;
    }
    });
  }else if(id=="czExport4"){

    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]["shop"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
            temp_arr={};
            temp_arr["order"]=i+1;
            temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["item_title"]=exportdata[i]["item"]["title"];  //商品标题
            temp_arr["pictUrl"]=exportdata[i]["item"]["pictUrl"];  //商品图片
            temp_arr["itemId"]=exportdata[i]["item"]["itemId"]; //商品id
            temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
            temp_arr["uvIndex"]=decimal(exportdata2["uvIndex"][i]["0"],0);// 访客数
            temp_arr["seIpvUvHits"]=decimal(exportdata2["seIpvUvHits"][i]["0"],0);// 搜索人数
            temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
            temp_arr["searchProportion"]=decimal((exportdata2["seIpvUvHits"][i]["0"]/exportdata2["uvIndex"][i]["0"])*100,2);// 搜索占比
            temp_arr["uvCost"]=decimal(exportdata2["tradeIndex"][i]["0"]/exportdata2["uvIndex"][i]["0"],2);// UV价值
            tab_data.push(temp_arr);
      }
  }                    

    table.render({
    elem: '#test'
    ,id: 'testReload'
    // ,initSort: {field:'tradeIndex', type:'desc'}
    ,data:tab_data
    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
    ,limit:10
    ,height:472
    ,cols: [[
    {field:'cateRankId', width:"4%", title: '#' ,sort: false}
    ,{field:'time', width:"7%", title: '日期', sort: true}
    ,{field:'page_name', width:"4%", title: '平台'}
    ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
    ,{field:'item_title', title: '商品名称',width: "12%",templet:function (d) { 
    if (d.item_title!=undefined) {
    return '<div style="text-align: left;"> <a  style="    outline: none; text-decoration: none; color: #72afd2;" target="view_window"   href="https://item.taobao.com/item.htm?id='+d.itemId+'" > '+d.item_title+' </a> </div>';
    }else{
    return '<div style="text-align: left;" ></div>'; 
    }
    }}
    ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
    if (d.pictUrl!=undefined) {
    return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
    }else{
    return '<div style="text-align: center;" ></div>'; 
    }
    }}
    ,{field:'itemId', title: '商品ID', sort: true,width: "9%"}
    ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
    ,{field:'uvIndex', width: "8%", title: '访客数', sort: true}
    ,{field:'seIpvUvHits', width: "8%", title: '搜索人数', sort: true}
    ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
    ,{field:'searchProportion', width: "8%", title: '搜索占比(%)', sort: true}
    ,{field:'uvCost', width: "8%", title: 'UV价值', sort: true}
    ]],  
    page:  {limits: [10, 20, 50, 100, 300,500]},
    done: function (res, curr, count) {
    exportData_temp=res.data;
    }
    });
  }else if(id=="czExport5"){

      //重新加载
      tab_data=[];
      for (var i = 0; i < exportdata.length; i++) {
        if (exportdata[i]["shop"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
              temp_arr={};
              temp_arr["order"]=i+1;
              temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
              temp_arr["page_name"]=sellerType_title;
              temp_arr["source"]= device_title;  //来源
              temp_arr["item_title"]=exportdata[i]["item"]["title"];  //商品标题
              temp_arr["pictUrl"]=exportdata[i]["item"]["pictUrl"];  //商品图片
              temp_arr["itemId"]=exportdata[i]["item"]["itemId"]; //商品id
              temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
              temp_arr["cltHits"]=decimal(exportdata2["cltHits"][i]["0"],0);// 收藏人数
              temp_arr["cartHits"]=decimal(exportdata2["cartHits"][i]["0"],0);// 加购人数
              temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
              tab_data.push(temp_arr);
        }
      }                    

          
    table.render({
    elem: '#test'
    ,id: 'testReload'
    // ,initSort: {field:'tradeIndex', type:'desc'}
    ,data:tab_data
    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
    ,limit:10
    ,height:472
    ,cols: [[
    {field:'cateRankId', width:"4%", title: '#' ,sort: false}
    ,{field:'time', width:"7%", title: '日期', sort: true}
    ,{field:'page_name', width:"4%", title: '平台'}
    ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
    ,{field:'item_title', title: '商品名称',width: "28%",templet:function (d) { 
    if (d.item_title!=undefined) {
    return '<div style="text-align: left;"> <a  style="    outline: none; text-decoration: none; color: #72afd2;" target="view_window"   href="https://item.taobao.com/item.htm?id='+d.itemId+'" > '+d.item_title+' </a> </div>';
    }else{
    return '<div style="text-align: left;" ></div>'; 
    }
    }}
    ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
    if (d.pictUrl!=undefined) {
    return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
    }else{
    return '<div style="text-align: center;" ></div>'; 
    }
    }}
    ,{field:'itemId', title: '商品ID', sort: true,width: "9%"}
    ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
    ,{field:'cltHits', width: "8%", title: '收藏人数', sort: true}
    ,{field:'cartHits', width: "8%", title: '加购人数', sort: true}
    ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
    ]],  
    page:  {limits: [10, 20, 50, 100, 300,500]},
    done: function (res, curr, count) {
    exportData_temp=res.data;
    }
    });
  }else if(id=="czExport6"){

      //重新加载
      tab_data=[];
      for (var i = 0; i < exportdata.length; i++) {
        if (exportdata[i]["brandModel"]["brandName"].indexOf(demoReload) >= 0||demoReload=="") {
          temp_arr={};
          temp_arr["order"]=i+1;
          temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["shop_title"]=exportdata[i]["brandModel"]["brandName"];
          temp_arr["pictureUrl"]=exportdata[i]["brandModel"]["logo"];  //店铺图片
          temp_arr["shop_userId"]=exportdata[i]["brandModel"]["brandId"]; //访客
          temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
          temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
          temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
          temp_arr["payRateIndex"]=decimal(exportdata2["zhl"][i]["0"]*100,2);// 转化率
          tab_data.push(temp_arr);
        }
      }                    
          
      table.render({
      elem: '#test'
      ,id: 'testReload'
      // ,initSort: {field:'tradeIndex', type:'desc'}
      ,data:tab_data
      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
      ,limit:10
      ,height:472
      ,cols: [[
      {field:'cateRankId', width:"3%", title: '#' ,sort: false}
      ,{field:'time', width:"7%", title: '日期', sort: true}
      ,{field:'page_name', width:"8%", title: '平台'}
      ,{field:'source', title: '终端', width: "8%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
      ,{field:'shop_title', title: '品牌名称',width: "10%" }
      ,{field: 'pictureUrl', width: "10%", title: '品牌图片',  sort: false,templet:function (d) { 
      if (d.pictureUrl!=undefined) {
      return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.pictureUrl+'_120x120.jpg"></div>';
      }else{
      return '<div style="text-align: center;" ></div>'; 
      }
      }}
      ,{field:'shop_userId', title: '品牌ID', sort: true,width: "9%"}
      ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
      ,{field:'tradeIndex', width: "12%", title: '交易金额', sort: true}
      ,{field:'tradeGrowthRange', width: "12%", title: '交易增值幅度(%)', sort: true}
      ,{field:'payRateIndex', width: "12%", title: '转化率(%)', sort: true}
      ]],  
      page:  {limits: [10, 20, 50, 100, 300,500]},
      done: function (res, curr, count) {
      exportData_temp=res.data;
      }
      });
  }else if(id=="czExport7"){

      //重新加载
      tab_data=[];
      for (var i = 0; i < exportdata.length; i++) {
        if (exportdata[i]["brandModel"]["brandName"].indexOf(demoReload) >= 0||demoReload=="") {
            temp_arr={};
            temp_arr["order"]=i+1;
            temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["shop_title"]=exportdata[i]["brandModel"]["brandName"];
            temp_arr["pictureUrl"]=exportdata[i]["brandModel"]["logo"];  //店铺图片
            temp_arr["shop_userId"]=exportdata[i]["brandModel"]["brandId"]; //访客
            temp_arr["cateRankId"]=exportdata[i]["cateRankId"]["value"];// 行业排名
            temp_arr["uvIndex"]=decimal(exportdata2["uvIndex"][i]["0"],0);// 访客数
            temp_arr["seIpvUvHits"]=decimal(exportdata2["seIpvUvHits"][i]["0"],0);// 搜索人数
            temp_arr["tradeIndex"]=decimal(exportdata2["tradeIndex"][i]["0"],0);// 交易额
            temp_arr["searchProportion"]=decimal((exportdata2["seIpvUvHits"][i]["0"]/exportdata2["uvIndex"][i]["0"])*100,2);// 搜索占比
            temp_arr["uvCost"]=decimal(exportdata2["tradeIndex"][i]["0"]/exportdata2["uvIndex"][i]["0"],2);// UV价值
            tab_data.push(temp_arr);
        }
      }                    
          
     table.render({
      elem: '#test'
      ,id: 'testReload'
      // ,initSort: {field:'tradeIndex', type:'desc'}
      ,data:tab_data
      ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
      ,limit:10
      ,height:472
      ,cols: [[
      {field:'cateRankId', width:"3%", title: '#' ,sort: false}
      ,{field:'time', width:"8%", title: '日期', sort: true}
      ,{field:'page_name', width:"6%", title: '平台'}
      ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
      ,{field:'shop_title', title: '品牌名称',width: "10%" }
      ,{field: 'pictureUrl', width: "10%", title: '品牌图片',  sort: false,templet:function (d) { 
      if (d.pictureUrl!=undefined) {
      return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.pictureUrl+'_120x120.jpg"></div>';
      }else{
      return '<div style="text-align: center;" ></div>'; 
      }
      }}
      ,{field:'shop_userId', title: '品牌ID', sort: true,width: "9%"}
      ,{field:'cateRankId', title: '行业排名',sort: true,width: "8%"}
      ,{field:'uvIndex', width: "8%", title: '访客数', sort: true}
      ,{field:'seIpvUvHits', width: "8%", title: '搜索人数', sort: true}
      ,{field:'tradeIndex', width: "8%", title: '交易金额', sort: true}
      ,{field:'searchProportion', width: "8%", title: '搜索占比(%)', sort: true}
      ,{field:'uvCost', width: "8%", title: 'UV价值', sort: true}
      ]],  
      page:  {limits: [10, 20, 50, 100, 300,500]},
      done: function (res, curr, count) {
      exportData_temp=res.data;
      }
      });
  }else if(id=='jkmbExport1'){
    if (dateType=="today"){
      exportdata=exportdata1['data']['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{  
      exportdata=exportdata1['data'];
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    //监控面板店铺
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if(item_type2=="店铺"){
        model='shop';
        name='title';
      }else if(item_type2=="商品"){
        model='item';
        name='title';
      }else if(item_type2=="品牌"){
        model='brandModel';
        name='brandName';
      }

      if (exportdata[i][model][name].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(item_type2=="店铺"){
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["shop_title"]=data[i]["shop"]["title"];
          temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
          temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
          temp_arr["id"]=c++;// 序号
          temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

          temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
          temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
          temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

          temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
          temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
          temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
          temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
          if(temp_arr["payNum"]==0){
            temp_arr["kdj"]=0;
          }else{
            temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
          }
          temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
        }else if(item_type2=="商品"){
          temp_arr["order"]=i+1;
          temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["shop_title"]=data[i]["item"]["title"];
          temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
          temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
          temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
          temp_arr["id"]=c++;// 序号
          temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

          temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
          temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
          temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

          temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
          temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
          temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
          temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
          if(temp_arr["payNum"]==0){
            temp_arr["kdj"]=0;
          }else{
            temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
          }
          temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
        }else if(item_type2=="品牌"){
          if(dateType=='today'){
            temp_arr["order"]=i+1;
            temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
            temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
            temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID
            temp_arr["id"]=c++;// 序号
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          }else{
            temp_arr["order"]=i+1;
            temp_arr["time"]=date_start==date_end?date_start:date_start+'|'+date_end;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
            temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
            temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID
            temp_arr["id"]=c++;// 序号
            temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

            temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
            temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
            temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数

            temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
            temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
            temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
            temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
            temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
            if(temp_arr["payNum"]==0){
              temp_arr["kdj"]=0;
            }else{
              temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
            }
            temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
          }
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(item_type2=="店铺"){
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控面板-店铺'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'page_name', width:"4%", title: '平台'}
            ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'shop_title', title: '店铺名称',width: "8%" }
            ,{field: 'pictureUrl', width: "5%", title: '店铺图片',  sort: false,templet:function (d) { 
              if (d.pictureUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'shop_userId', title: '店铺ID', sort: true,width: "4%"}
            ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
            ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
            ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
            ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
            ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
            ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
            ,{field:'payNum', width: "5%", title: '买家数', sort: true}
            ,{field:'kdj', width: "5%", title: '客单价', sort: true}
            ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
            ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
            ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
            ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          // initSort: {
          //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
          //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
          // },
          done: function (res, curr, count) {
            exportData=res.data;
          }
      });
    }else if(item_type2=="商品"){
      var ins1= table.render({
        elem: '#test'
        ,id: 'testReload'
        ,title: '监控面板-商品'
        ,data:tab_data
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,limit:10
        ,height:472
        ,cols: [[
          {field:'order', width:"2%", title: '#' ,sort: false}
          ,{field:'time', width:"5%", title: '日期', sort: true}
          ,{field:'page_name', width:"4%", title: '平台'}
          ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'shop_title', title: '商品名称',width: "8%",templet:function (d) { 
            return '<a href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
          }}
          ,{field: 'pictUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
            if (d.pictUrl!=undefined) {
              return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
            }else{
              return '<div style="text-align: center;" ></div>'; 
            }
          }}
          ,{field:'itemId', title: '商品ID', sort: true,width: "4%"}
          ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
          ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
          ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
          ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
          ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
          ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
          ,{field:'payNum', width: "5%", title: '买家数', sort: true}
          ,{field:'kdj', width: "5%", title: '客单价', sort: true}
          ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
          ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
          ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
          ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
        ]],  
        page:  {limits: [10, 20, 50, 100, 300,500]},
        // initSort: {
        //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
        //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
        // },
        done: function (res, curr, count) {
          exportData=res.data;
        }
      });
    }else if(item_type2=="品牌"){
      if(dateType=='today'){
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控面板-品牌'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"10%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'page_name', width:"10%", title: '平台'}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'brandName', title: '品牌名称',width: "15%"}
            ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
              if (d.logo!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'brandId', title: '品牌ID', sort: true,width: "15%"}
            ,{field:'tradeIndex', width: "15%", title: '交易金额', sort: true}
            
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }else{
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控面板-品牌'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'page_name', width:"4%", title: '平台'}
            ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'brandName', title: '品牌名称',width: "8%"}
            ,{field: 'logo', width: "5%", title: '品牌LOGO',  sort: false,templet:function (d) { 
              if (d.logo!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'brandId', title: '品牌ID', sort: true,width: "4%"}
            ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
            ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
            ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
            ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
            ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
            ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
            ,{field:'payNum', width: "5%", title: '买家数', sort: true}
            ,{field:'kdj', width: "5%", title: '客单价', sort: true}
            ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
            ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
            ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
            ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }
    }
  }else if(id=='jkmbExport2'){
    var short_name=$('.short-name').text();
    if (dateType=="today"){
      exportdata=exportdata1['data']['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{  
      exportdata=exportdata1['data'];
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    //监控面板店铺
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if(item_type2=="热门店铺"){
        model='shop';
        name='title';
      }else if(item_type2=="热门商品"){
        model='item';
        name='title';
      }else if(item_type2=="热门品牌"){
        model='brandModel';
        name='brandName';
      }
      if (exportdata[i][model][name].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(item_type2=="热门店铺"){
          if(dateType=='today'){
            temp_arr["order"]=i+1;
            temp_arr["time"]=now_date;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["shop_title"]=data[i]["shop"]["title"];
            temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
            temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
            temp_arr["short_name"]=short_name;// 类目名称
            temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          }else{
            temp_arr["order"]=i+1;
            temp_arr["time"]=now_date;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["shop_title"]=data[i]["shop"]["title"];
            temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
            temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
            temp_arr["short_name"]=short_name;// 类目名称
            temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
            temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
            temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
          }
        }else if(item_type2=="热门商品"){
          if(dateType=='today'){
            temp_arr["order"]=i+1;
            temp_arr["time"]=now_date;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["shop_title"]=data[i]["item"]["title"];
            temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
            temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
            temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
            temp_arr["short_name"]=short_name;// 类目名称
            temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          }else{
            temp_arr["order"]=i+1;
            temp_arr["time"]=now_date;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["shop_title"]=data[i]["item"]["title"];
            temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //店铺图片
            temp_arr["itemId"]=data[i]["item"]["itemId"]; //商品ID
            temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
            temp_arr["short_name"]=short_name;// 类目名称
            temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
            temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
            temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
          }
        }else if(item_type2=="热门品牌"){
          if(dateType=='today'){
            temp_arr["order"]=i+1;
            temp_arr["time"]=now_date;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
            temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
            temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID

            temp_arr["short_name"]=short_name;// 类目名称
            temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          }else{
            temp_arr["order"]=i+1;
            temp_arr["time"]=now_date;
            temp_arr["page_name"]=sellerType_title;
            temp_arr["source"]= device_title;  //来源
            temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
            temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
            temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID

            temp_arr["short_name"]=short_name;// 类目名称
            temp_arr["cateRankId"]=data[i]["cateRankId"]["value"];// 行业排名
            temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
            temp_arr["tradeGrowthRange"]=decimal(exportdata[i]["tradeGrowthRange"]["value"]*100,2);// 交易增值指数
            temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
          }
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(item_type2=="热门店铺"){
      if(dateType=='today'){
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '行业监控-热门店铺'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'cateRankId', width:"5%", title: '#' ,sort: false}
              ,{field:'time', width:"15%", title: '日期', sort: true}
              ,{field:'page_name', width:"8%", title: '平台'}
              ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
              ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
              ,{field:'shop_title', title: '店铺名称',width: "10%" }
              ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
                if (d.pictureUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'shop_userId', title: '店铺ID', sort: true,width: "10%"}
              ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
              ,{field:'cateRankId', width: "7%", title: '行业排名', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }else{
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '行业监控-热门店铺'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'cateRankId', width:"5%", title: '#' ,sort: false}
              ,{field:'time', width:"15%", title: '日期', sort: true}
              ,{field:'page_name', width:"6%", title: '平台'}
              ,{field:'source', title: '终端', width: "12%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
              ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
              ,{field:'shop_title', title: '店铺名称',width: "10%" }
              ,{field: 'pictureUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
                if (d.pictureUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'shop_userId', title: '店铺ID', sort: true,width: "5%"}
              ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
              ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
              ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
              ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }
    }else if(item_type2=="热门商品"){
      if(dateType=='today'){
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '行业监控-热门商品'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'cateRankId', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'page_name', width:"8%", title: '平台'}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
            ,{field:'shop_title', title: '商品名称',width: "12%",templet:function (d) { 
              return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
            }}
            ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
              if (d.pictUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'itemId', title: '商品ID', sort: true,width: "10%"}
            ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
            ,{field:'cateRankId', width: "8%", title: '行业排名', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }else{
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '行业监控-热门商品'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'cateRankId', width:"5%", title: '#' ,sort: false}
              ,{field:'time', width:"13%", title: '日期', sort: true}
              ,{field:'page_name', width:"6%", title: '平台'}
              ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
              ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
              ,{field:'shop_title', title: '商品名称',width: "12%",templet:function (d) { 
                return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.shop_title+'</a>';
              }}
              ,{field: 'pictUrl', width: "8%", title: '商品图片',  sort: false,templet:function (d) { 
                if (d.pictUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'itemId', title: '商品ID', sort: true,width: "5%"}
              ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
              ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
              ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
              ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }
    }else if(item_type2=="热门品牌"){
      if(dateType=='today'){
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '行业监控-热门商品'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'cateRankId', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'page_name', width:"8%", title: '平台'}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'short_name', title: '类目名称', width: "15%", minWidth: 30}
            ,{field:'brandName', title: '品牌名称',width: "12%"}
            ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
              if (d.logo!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'brandId', title: '商品ID', sort: true,width: "10%"}
            ,{field:'tradeIndex', width: "10%", title: '交易金额', sort: true}
            ,{field:'cateRankId', width: "8%", title: '行业排名', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }else{
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '行业监控-热门品牌'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'cateRankId', width:"5%", title: '#' ,sort: false}
              ,{field:'time', width:"10%", title: '日期', sort: true}
              ,{field:'page_name', width:"6%", title: '平台'}
              ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
              ,{field:'short_name', title: '类目名称', width: "12%", minWidth: 30}
              ,{field:'brandName', title: '品牌名称',width: "12%"}
              ,{field: 'logo', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                if (d.logo!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'brandId', title: '商品ID', sort: true,width: "10%"}
              ,{field:'tradeIndex', width: "5%", title: '交易金额', sort: true}
              ,{field:'tradeGrowthRange', width: "10%", title: '交易增值幅度(%)', sort: true}
              ,{field:'payRateIndex', width: "8%", title: '转化率(%)', sort: true}
              ,{field:'cateRankId', width: "6%", title: '行业排名', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }
    }
  }else if(id=='ssphExport'){
    var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    //搜索排行
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]['searchWord'].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(item_type=="搜索词"||item_type=="长尾词"){
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["source"]= device_title;  //来源
          temp_arr["searchWord"]=data[i]["searchWord"];//搜索词
          temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
          temp_arr["clickHits"]=decimal(data2["clickHits"][i]["0"],0);// 点击人数
          temp_arr["payRate"]=decimal((data[i]["payRate"]*100),0);// 支付转化率
          temp_arr["clickRate"]=decimal((data[i]["clickRate"]*100),0);// 点击率
          
          temp_arr["sszb"]=100;// 搜索占比
          if(default_title=='热搜'){
            temp_arr["p4pRefPrice"]=decimal(data[i]["p4pRefPrice"],0);// 参考价格
            temp_arr["hotSearchRank"]=data[i]["hotSearchRank"];// 排名
          }else{
            temp_arr["soarRank"]=data[i]["soarRank"];// 排名
            temp_arr["seRiseRate"]=decimal((data[i]["seRiseRate"]*100),0);// 涨幅增长率
          }
        }else if(item_type=="品牌词"||item_type=="核心词"||item_type=="修饰词"){
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["source"]= device_title;  //来源
          temp_arr["searchWord"]=data[i]["searchWord"];//搜索词
          temp_arr["avgWordSeIpvUvHits"]=decimal(data2["avgWordSeIpvUvHits"][i]["0"],0);// 搜索人数
          temp_arr["avgWordClickHits"]=decimal(data2["avgWordClickHits"][i]["0"],0);// 点击人数
          temp_arr["avgWordPayRate"]=decimal((data[i]["avgWordPayRate"]*100),0);// 支付转化率
          temp_arr["p4pRefPrice"]=decimal(data[i]["p4pRefPrice"],0);// 参考价格
          temp_arr["sszb"]=100;// 搜索占比
          if(item_type=='品牌词'){
            temp_arr["relSeWordCnt"]=data[i]["relSeWordCnt"];// 相关词搜索数
            if(default_title=='热搜'){
              temp_arr["hotSearchRank"]=data[i]["hotSearchRank"];// 排名
              temp_arr["avgWordClickRate"]=decimal((data[i]["avgWordClickRate"]*100),0);// 点击率
            }else{
              temp_arr["soarRank"]=data[i]["soarRank"];// 排名
              
              temp_arr["avgWordSeRiseRate"]=decimal((data[i]["avgWordSeRiseRate"]*100),0);// 涨幅增长率
            }
          }else if(item_type=="核心词"||item_type=="修饰词"){
            temp_arr["relSeWordCnt"]=data[i]["relSeWordCnt"];// 相关词搜索数
            if(default_title=='热搜'){
              temp_arr["hotSearchRank"]=data[i]["hotSearchRank"];// 排名
              temp_arr["avgWordClickRate"]=decimal((data[i]["avgWordClickRate"]*100),0);// 点击率
            }else{
              temp_arr["soarRank"]=data[i]["soarRank"];// 排名
              temp_arr["avgWordSeRiseRate"]=decimal((data[i]["avgWordSeRiseRate"]*100),0);// 涨幅增长率
            }
          }
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(item_type=="搜索词"||item_type=="长尾词"){
      if(default_title=='热搜'){
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '搜索排行-'+item_type+'-热词'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'hotSearchRank', width:"5%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30}
            ,{field:'searchWord', title: '搜索词', width: "10%", minWidth: 30}
            ,{field:'hotSearchRank', width: "7%", title: '搜索词排名', sort: true}
            ,{field:'seIpvUvHits', title: '搜索人数',width: "10%" }
            ,{field:'clickHits', title: '点击人数', sort: true,width: "10%"}
            ,{field:'clickRate', title: '点击率', sort: true,width: "7%"}
            ,{field:'payRate', width: "10%", title: '支付转化率', sort: true}
            ,{field:'p4pRefPrice', width: "10%", title: '参考价格', sort: true}
            ,{field:'sszb', width: "6%", title: '搜索占比', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }else{
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '搜索排行-'+item_type+'-飙升'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'soarRank', width:"5%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30}
            ,{field:'searchWord', title: '搜索词', width: "10%", minWidth: 30}
            ,{field:'soarRank', width: "7%", title: '飙升排名', sort: true}
            ,{field:'seRiseRate', width: "10%", title: '搜索增长幅度', sort: true}
            ,{field:'seIpvUvHits', title: '搜索人数',width: "10%" }
            ,{field:'clickHits', title: '点击人数', sort: true,width: "10%"}
            ,{field:'clickRate', title: '点击率', sort: true,width: "7%"}
            ,{field:'payRate', width: "10%", title: '支付转化率', sort: true}
            ,{field:'sszb', width: "6%", title: '搜索占比', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }
    }else if(item_type=="品牌词"||item_type=="核心词"||item_type=="修饰词"){
      if(default_title=='热搜'){
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '搜索排行-'+item_type+'-热词'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'hotSearchRank', width:"3%", title: '#' ,sort: false}
            ,{field:'time', width:"11%", title: '日期', sort: true}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30}
            ,{field:'searchWord', title: '搜索词', width: "6%", minWidth: 30}
            ,{field:'hotSearchRank', width: "7%", title: '热度排名', sort: true}
            ,{field:'relSeWordCnt', width: "10%", title: '相关搜索词数', sort: true}
            ,{field:'avgWordSeIpvUvHits', title: '相关词搜索人数',width: "10%" }
            ,{field:'avgWordClickHits', title: '相关词点击人数', sort: true,width: "8%"}
            ,{field:'avgWordClickRate', title: '词均点击率', sort: true,width: "9%"}
            ,{field:'avgWordPayRate', width: "10%", title: '词均支付转化率', sort: true}
            ,{field:'p4pRefPrice', width: "8%", title: '参考价格', sort: true}
            ,{field:'sszb', width: "8%", title: '搜索占比', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }else{
        var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '搜索排行-'+item_type+'-热词'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'soarRank', width:"3%", title: '#' ,sort: false}
            ,{field:'time', width:"13%", title: '日期', sort: true}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30}
            ,{field:'searchWord', title: '搜索词', width: "6%", minWidth: 30}
            ,{field:'soarRank', width: "8%", title: '飙升排名', sort: true}
            ,{field:'avgWordSeRiseRate', width: "14%", title: '词汇搜索增长幅度', sort: true}
            ,{field:'relSeWordCnt', width: "10%", title: '相关搜索词数', sort: true}
            ,{field:'avgWordSeIpvUvHits', title: '相关词搜索人数',width: "10%" }
           
            ,{field:'avgWordClickHits', title: '相关词点击人数', sort: true,width: "8%"}
            ,{field:'avgWordPayRate', width: "10%", title: '词均支付转化率', sort: true}
            
            ,{field:'sszb', width: "8%", title: '搜索占比', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
      }
    }
  }else if(id=='lsdpExport'){
    var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]["shop"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        temp_arr["order"]=i+1;
        temp_arr["time"]=now_date;
        temp_arr["page_name"]=sellerType_title;
        temp_arr["source"]= device_title;  //来源
        temp_arr["shop_title"]=data[i]["shop"]["title"];
        temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
        temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
        temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
        temp_arr["lostHits"]=decimal(data2["lostHits"][i]["0"],0);// 流失人数
        temp_arr["lostIndex"]=decimal(data2["lostIndex"][i]["0"],0);// 点击次数
        temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
        temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
        temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
        temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
        tab_data.push(temp_arr);
      }
    }                    
    var ins1= table.render({
        elem: '#test'
        ,id: 'testReload'
        ,title: '竞店识别-TOP流失店铺列表'
        ,data:tab_data
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,limit:10
        ,height:472
        ,cols: [[
          {field:'order', width:"5%", title: '#' ,sort: false}
          ,{field:'time', width:"8%", title: '日期', sort: true}
          ,{field:'page_name', width:"8%", title: '平台'}
          ,{field:'source', title: '终端', width: "6%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'shop_title', title: '店铺名称',width: "8%" }
          ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
            if (d.pictureUrl!=undefined) {
              return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
            }else{
              return '<div style="text-align: center;" ></div>'; 
            }
          }}
          ,{field:'shop_userId', title: '店铺ID', sort: true,width: "7%"}
          ,{field:'lostIndex', width: "7%", title: '点击次数', sort: true}
          ,{field:'lostHits', width: "7%", title: '流失人数', sort: true}
          ,{field:'uvIndex', width: "7%", title: '访客数', sort: true}
          ,{field:'seIpvUvHits', width: "7%", title: '搜索人数', sort: true}
          ,{field:'tradeIndex', width: "7%", title: '交易金额', sort: true}
          ,{field:'searchProportion', width: "7%", title: '搜索占比(%)', sort: true}
          ,{field:'uvCost', width: "6%", title: 'UV价值', sort: true}
        ]],  
        page:  {limits: [10, 20, 50, 100, 300,500]},
        done: function (res, curr, count) {
          exportData=res.data;
        }
    });
  }else if(id=='jkspExport'){
    if (dateType=="today"){
      exportdata=exportdata1['data']['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{  
      exportdata=exportdata1['data'];
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]["item"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(dateType=="today"){
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["item_title"]=data[i]["item"]["title"];
          temp_arr["pictureUrl"]=data[i]["item"]["pictUrl"];  //商品图片
          temp_arr["item_userId"]=data[i]["item"]["userId"]; //商品ID
          temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品url
          temp_arr["id"]=c++;// 序号
          temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数

          temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
          temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
          temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数
          temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i],0);// 搜索人数
          
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          temp_arr["searchProportion"]=decimal((temp_arr["seIpvUvHits"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
          temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
          temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
          temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
          if(temp_arr["payNum"]==0){
            temp_arr["kdj"]=0;
          }else{
            temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
          }
          temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
        }else{
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["item_title"]=data[i]["item"]["title"];
          temp_arr["pictureUrl"]=data[i]["item"]["pictUrl"];  //商品图片
          temp_arr["item_userId"]=data[i]["item"]["userId"]; //商品ID
          temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品url
          temp_arr["id"]=c++;// 序号
          temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购数
          temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏数
          if(data2["seIpvUvHits"]!=undefined){
            temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i],0);// 搜索人数
          }else{
            temp_arr["seIpvUvHits"]=0;// 搜索人数
          }
          

          temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
          
          temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值
          temp_arr["searchProportion"]=decimal((temp_arr["seIpvUvHits"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
          temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
          temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
          
          if(temp_arr["payNum"]==0){
            temp_arr["kdj"]=0;
          }else{
            temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
          }
          temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(dateType=="today"){
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控商品-竞品列表'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'page_name', width:"4%", title: '平台'}
            ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'item_title', title: '商品名称',width: "8%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
            ,{field: 'pictureUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
              if (d.pictureUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'item_userId', title: '商品ID', sort: true,width: "4%"}

            ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
            ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
            ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
            ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
            ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
            ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
            ,{field:'payNum', width: "5%", title: '买家数', sort: true}
            ,{field:'kdj', width: "5%", title: '客单价', sort: true}
            ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
            ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
            ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
            ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          // initSort: {
          //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
          //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
          // },
          done: function (res, curr, count) {
            exportData=res.data;
          }
      });
    }else{
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控商品-竞品列表'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'page_name', width:"4%", title: '平台'}
            ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'item_title', title: '商品名称',width: "8%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
            ,{field: 'pictureUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
              if (d.pictureUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'item_userId', title: '商品ID', sort: true,width: "4%"}
            ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
            ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
            ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
            ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
            ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
            ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
            ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
            ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
            ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
            ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
            ,{field:'payNum', width: "5%", title: '买家数', sort: true}
            ,{field:'kdj', width: "5%", title: '客单价', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          // initSort: {
          //   field: 'payNum' //排序字段，对应 cols 设定的各字段名
          //   ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
          // },
          done: function (res, curr, count) {
            exportData=res.data;
          }
      });
    }
  }else if(id=='jpsbExport'){
    var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    var db_chick=$("input[class='ant-radio-input']:checked").parent().next().text();
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]["item"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        temp_arr["order"]=i+1;
        temp_arr["time"]=now_date;
        temp_arr["item_title"]=data[i]["item"]["title"];
        temp_arr["pictureUrl"]=data[i]["item"]["pictUrl"];  //商品图片
        temp_arr["item_userId"]=data[i]["item"]["itemId"]; //商品ID
        temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品url
        if(item_type=='顾客流失竞品推荐'){
          temp_arr["cartLosByrCnt"]=data[i]["cartLosByrCnt"]["value"]; //加购后流失人数
          temp_arr["cltLosByrCnt"]=data[i]["cltLosByrCnt"]["value"]; //收藏后流失人数
          temp_arr["cltJmpByrCnt"]=data[i]["cltJmpByrCnt"]["value"]; //收藏后跳失人数
          temp_arr["cartJmpByrCnt"]=data[i]["cartJmpByrCnt"]["value"]; //加购后跳失人数
          temp_arr["losItmCnt"]=data[i]["losItmCnt"]["value"]; //引起流失的商品数
          temp_arr["directLosCnt"]=data[i]["directLosCnt"]["value"]; //直接跳失人数
          temp_arr["losShopCnt"]=data[i]["directLosCnt"]["value"]; //引起流失的店铺数
          temp_arr["cartLosByrCnt"]=data[i]["payLostAmt"]["value"]; //流失金额
          temp_arr["losByrCnt"]=data[i]["losByrCnt"]["value"]; //流失人数
          temp_arr["losRate"]=decimal((data[i]["losRate"]["value"])*100,2); //流失率
        }else{
            temp_arr["seRivalItmCnt"]=data[i]["seRivalItmCnt"]["value"]; //搜索竞争商品数
          if(db_chick=='搜索引导访客数'){
            temp_arr["seGuideUv"]=data[i]["seGuideUv"]["value"]; //本店商品搜索引导访客数
            temp_arr["rivalItmAvgSeGuideUv"]=data[i]["rivalItmAvgSeGuideUv"]["value"]; //竞品平均搜索引导访客数
          }else if(db_chick=='搜索引导加购人数'){
            temp_arr["rivalItmAvgSeGuideCartByrCnt"]=data[i]["rivalItmAvgSeGuideCartByrCnt"]["value"]; //竞品平均搜索引导加购人数
            temp_arr["seGuideCartByrCnt"]=data[i]["seGuideCartByrCnt"]["value"]; //本店商品搜索引导加购人数
          }else if(db_chick=='搜索引导支付买家数'){
            temp_arr["rivalItmAvgSeGuidePayByrCnt"]=data[i]["rivalItmAvgSeGuidePayByrCnt"]["value"]; //竞品平均搜索引导支付买家数
            temp_arr["seGuidePayByrCnt"]=data[i]["seGuidePayByrCnt"]["value"]; //本店商品搜索引导支付买家数
          }else if(db_chick=='搜索引导支付转化率'){
            temp_arr["rivalItmAvgSeGuidePayRate"]=decimal((data[i]["rivalItmAvgSeGuidePayRate"]["value"])*100,2); //竞品平均搜索引导支付转化率
            temp_arr["seGuidePayRate"]=decimal((data[i]["seGuidePayRate"]["value"])*100,2); //本店商品搜索引导支付转化率
          }
          
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(item_type=='顾客流失竞品推荐'){
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '竞品识别-顾客流失竞品推荐'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'item_title', title: '商品名称',width: "8%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
            ,{field: 'pictureUrl', width: "5%", title: '商品图片',  sort: false,templet:function (d) { 
              if (d.pictureUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'item_userId', title: '商品ID', sort: true,width: "4%"}

            ,{field:'cartLosByrCnt', width: "7%", title: '流失金额', sort: true}
            ,{field:'losByrCnt', width: "7%", title: '流失人数', sort: true}
            ,{field:'losRate', width: "7%", title: '流失率(%)', sort: true}

            ,{field:'cartLosByrCnt', width: "8%", title: '收藏后流失人数', sort: true}
            ,{field:'cltLosByrCnt', width: "8%", title: '加购后流失人数', sort: true}
            ,{field:'cltJmpByrCnt', width: "8%", title: '收藏后跳失人数', sort: true}
            ,{field:'cartJmpByrCnt', width: "8%", title: '加购后跳失人数', sort: true}
            ,{field:'directLosCnt', width: "8%", title: '直接跳失人数', sort: true}
            ,{field:'losItmCnt', width: "8%", title: '引起流失的商品数', sort: true}
            ,{field:'losShopCnt', width: "8%", title: '引起流失的店铺数', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
      });
    }else{
      if(db_chick=='搜索引导访客数'){
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '竞品识别-搜索流失竞品推荐'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'order', width:"10%", title: '#' ,sort: false}
              ,{field:'time', width:"15%", title: '日期', sort: true}
              ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
              ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                if (d.pictureUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
              ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
              ,{field:'seGuideUv', width: "15%", title: '本店商品搜索引导访客数', sort: true}
              ,{field:'rivalItmAvgSeGuideUv', width: "15%", title: '竞品平均搜索引导访客数', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }else if(db_chick=='搜索引导加购人数'){
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '竞品识别-搜索流失竞品推荐'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'order', width:"10%", title: '#' ,sort: false}
              ,{field:'time', width:"15%", title: '日期', sort: true}
              ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
              ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                if (d.pictureUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
              ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
              ,{field:'rivalItmAvgSeGuideCartByrCnt', width: "15%", title: '竞品平均搜索引导加购人数', sort: true}
              ,{field:'seGuideCartByrCnt', width: "15%", title: '本店商品搜索引导加购人数', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }else if(db_chick=='搜索引导支付买家数'){
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '竞品识别-搜索流失竞品推荐'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'order', width:"10%", title: '#' ,sort: false}
              ,{field:'time', width:"15%", title: '日期', sort: true}
              ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
              ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                if (d.pictureUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
              ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
              ,{field:'rivalItmAvgSeGuidePayByrCnt', width: "15%", title: '竞品平均搜索引导支付买家数', sort: true}
              ,{field:'seGuidePayByrCnt', width: "15%", title: '本店商品搜索引导支付买家数', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }else if(db_chick=='搜索引导支付转化率'){
        var ins1= table.render({
            elem: '#test'
            ,id: 'testReload'
            ,title: '竞品识别-搜索流失竞品推荐'
            ,data:tab_data
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,limit:10
            ,height:472
            ,cols: [[
              {field:'order', width:"10%", title: '#' ,sort: false}
              ,{field:'time', width:"15%", title: '日期', sort: true}
              ,{field:'item_title', title: '商品名称',width: "10%",templet:function(d){return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';} }
              ,{field: 'pictureUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
                if (d.pictureUrl!=undefined) {
                  return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src='+d.pictureUrl+'></div>';
                }else{
                  return '<div style="text-align: center;" ></div>'; 
                }
              }}
              ,{field:'item_userId', title: '商品ID', sort: true,width: "10%"}
              ,{field:'seRivalItmCnt', width: "15%", title: '搜索竞争商品数', sort: true}
              ,{field:'rivalItmAvgSeGuidePayRate', width: "15%", title: '竞品平均搜索引导支付转化率(%)', sort: true}
              ,{field:'seGuidePayRate', width: "15%", title: '本店商品搜索引导支付转化率(%)', sort: true}
            ]],  
            page:  {limits: [10, 20, 50, 100, 300,500]},
            done: function (res, curr, count) {
              exportData=res.data;
            }
        });
      }
    }
  }else if(id=='jkppExport'){
    if(dateType=='today'){
      exportdata=exportdata1['data']['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{
      exportdata=exportdata1['data'];
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]["brandModel"]["brandName"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        temp_arr["order"]=i+1;
        temp_arr["time"]=now_date;
        temp_arr["page_name"]=sellerType_title;
        temp_arr["source"]= device_title;  //来源
        temp_arr["brandName"]=data[i]["brandModel"]["brandName"];
        temp_arr["logo"]=data[i]["brandModel"]["logo"];  //店铺图片
        temp_arr["brandId"]=data[i]["brandModel"]["brandId"]; //品牌ID
        if(dateType=='today'){
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
        }else{
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
          
          temp_arr["payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 转化率
          temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
          temp_arr["seIpvUvHits"]=decimal(data2["seIpvUvHits"][i]["0"],0);// 搜索人数
          temp_arr["cltHits"]=decimal(data2["cltHits"][i]["0"],0);// 收藏人数
          temp_arr["cartHits"]=decimal(data2["cartHits"][i]["0"],0);// 加购人数
          temp_arr["searchProportion"]=decimal((data2["seIpvUvHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 搜索占比
          temp_arr["searchCltHits"]=decimal((data2["cltHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 收藏率
          temp_arr["searchCartHits"]=decimal((data2["cartHits"][i]["0"]/data2["uvIndex"][i]["0"])*100,2);// 加购率
          temp_arr["uvCost"]=decimal(data2["tradeIndex"][i]["0"]/data2["uvIndex"][i]["0"],2);// UV价值

          temp_arr["payNum"]=decimal(decimal(data2["uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 买家数
          if(temp_arr["payNum"]==0){
            temp_arr["kdj"]=0;
          }else{
            temp_arr["kdj"]=decimal(data2["tradeIndex"][i]["0"]/temp_arr["payNum"],2);// 客单价
          }
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(dateType=='today'){
       var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控品牌-品牌列表'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"10%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'page_name', width:"10%", title: '平台'}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'brandName', title: '品牌名称',width: "15%"}
            ,{field: 'logo', width: "10%", title: '品牌LOGO',  sort: false,templet:function (d) { 
              if (d.logo!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'brandId', title: '品牌ID', sort: true,width: "15%"}
            ,{field:'tradeIndex', width: "15%", title: '交易金额', sort: true}
            
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
    }else{
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控品牌-品牌列表'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"2%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'page_name', width:"4%", title: '平台'}
            ,{field:'source', title: '终端', width: "4%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            ,{field:'brandName', title: '品牌名称',width: "8%"}
            ,{field: 'logo', width: "5%", title: '品牌LOGO',  sort: false,templet:function (d) { 
              if (d.logo!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src="http://img.alicdn.com/tps/'+d.logo+'_120x120.jpg"></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'brandId', title: '品牌ID', sort: true,width: "4%"}
            ,{field:'uvIndex', width: "5%", title: '访客数', sort: true}
            ,{field:'seIpvUvHits', width: "6%", title: '搜索人数', sort: true}
            ,{field:'payRateIndex', width: "6%", title: '转化率', sort: true}
            ,{field:'tradeIndex', width: "6%", title: '交易金额', sort: true}
            ,{field:'searchProportion', width: "6%", title: '搜索占比(%)', sort: true}
            ,{field:'uvCost', width: "5%", title: 'UV价值', sort: true}
            ,{field:'payNum', width: "5%", title: '买家数', sort: true}
            ,{field:'kdj', width: "5%", title: '客单价', sort: true}
            ,{field:'cartHits', width: "6%", title: '加购人数', sort: true}
            ,{field:'cltHits', width: "6%", title: '收藏人数', sort: true}
            ,{field:'searchCartHits', width: "6%", title: '加购率(%)', sort: true}
            ,{field:'searchCltHits', width: "6%", title: '收藏率(%)', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
        });
    }
  }else if(id=='topspExport'){
    if (dateType=="today"){
      exportdata=exportdata1['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{  
      exportdata=exportdata1;
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    //重新加载
    tab_data=[];
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]["item"]["title"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(default_title=='热销'){
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
     
          temp_arr["source"]= device_title;  //来源
          temp_arr["item_title"]=data[i]["item"]["title"];

          shopId=data[i]["item"]["detailUrl"].split('id=');
          temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //商品图片
          temp_arr["item_userId"]=shopId[1]; //商品ID
          temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
          temp_arr["id"]=c++;// 序号
          temp_arr["tradeIndex"]=decimal(data2["tradeIndex"][i]["0"],0);// 交易额
        }else{
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          shopId=data[i]["item"]["detailUrl"].split('id=');
          temp_arr["source"]= device_title;  //来源
          temp_arr["item_title"]=data[i]["item"]["title"];
          sp_id = data[i]["item"]["detailUrl"].indexOf('id=');
          temp_arr["pictUrl"]=data[i]["item"]["pictUrl"];  //商品图片
          temp_arr["item_userId"]=shopId[1]; //商品ID
          temp_arr["detailUrl"]=data[i]["item"]["detailUrl"]; //商品链接
          temp_arr["id"]=c++;// 序号
          temp_arr["uvIndex"]=decimal(data2["uvIndex"][i]["0"],0);// 访客数
        }
        tab_data.push(temp_arr);
      }
    }                    
    if(default_title=='热销'){
      var ins1= table.render({
        elem: '#test'
        ,id: 'testReload'
        ,title: '竞店分析-TOP店铺榜-热销'
        ,data:tab_data
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,limit:10
        ,height:472
        ,cols: [[
          {field:'order', width:"10%", title: '#' ,sort: false}
          ,{field:'time', width:"20%", title: '日期', sort: true}
          ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'item_title', title: '商品名称',width: "20%",templet:function (d) { 
            return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';
          }}
          ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
            if (d.pictUrl!=undefined) {
              return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
            }else{
              return '<div style="text-align: center;" ></div>'; 
            }
          }}
          ,{field:'item_userId', title: '商品ID', sort: true,width: "15%"}
          ,{field:'tradeIndex', width: "15%", title: '交易金额', sort: true}
        ]],  
        page:  {limits: [10, 20, 50, 100, 300,500]},
        done: function (res, curr, count) {
          exportData=res.data;
        }
      });
    }else{
      var ins1= table.render({
        elem: '#test'
        ,id: 'testReload'
        ,title: '竞店分析-TOP店铺榜-流量'
        ,data:tab_data
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,limit:10
        ,height:472
        ,cols: [[
         {field:'order', width:"10%", title: '#' ,sort: false}
          ,{field:'time', width:"20%", title: '日期', sort: true}
          ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'item_title', title: '商品名称',width: "20%",templet:function (d) { 
            return '<a style="outline: none; text-decoration: none; color: #72afd2;" target="view_window" href="https:'+d.detailUrl+'">'+d.item_title+'</a>';
          }}
          ,{field: 'pictUrl', width: "10%", title: '商品图片',  sort: false,templet:function (d) { 
            if (d.pictUrl!=undefined) {
              return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
            }else{
              return '<div style="text-align: center;" ></div>'; 
            }
          }}
          ,{field:'item_userId', title: '商品ID', sort: true,width: "15%"}
          ,{field:'uvIndex', width: "15%", title: '访客数', sort: true}
        ]],  
        page:  {limits: [10, 20, 50, 100, 300,500]},
        done: function (res, curr, count) {
          exportData=res.data;
        }
      });
    }
  }else if(id=='rdlyExport'){
    if (dateType=="today"){
      exportdata=exportdata1['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{  
      exportdata=exportdata1;
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    //重新加载
    tab_data1=[];
    for (var i = 0; i < tab_data.length; i++) {
      if (tab_data[i]["pageName"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(dateType=="today"){
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["source"]= device_title;  //来源
          temp_arr["pageName"]=tab_data[i]["pageName"];//流量来源 
          temp_arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
          temp_arr["shop_userId"]=userId; //店铺ID
          temp_arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
          temp_arr["selfShopUv"]=tab_data[i]["selfShopUv"]; //访客数
          temp_arr["id"]=c++;// 序号
        }else{
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["source"]= device_title;  //来源
          temp_arr["pageName"]=tab_data[i]["pageName"]["value"];//流量来源
          temp_arr["pictUrl"]=$('.sycm-common-select-selected-image-wrapper').find('img').attr('src');  //店铺图片
          temp_arr["shop_userId"]=userId; //店铺ID
          temp_arr["shop_name"]=$('.sycm-common-select-selected-title').text(); //店铺名称
          temp_arr["selfShopUv"]=tab_data[i]["selfShopUv"]; //访客数
          
          temp_arr["selfShopPayAmt"]=decimal(tab_data[i]["selfShopPayAmt"],2); //交易金额
          if(tab_data[i]["selfShopPayByrCnt"]["value"]!=NaN){
            temp_arr["selfShopPayByrCnt"]=decimal(tab_data[i]["selfShopPayByrCnt"],0); //买家数
          }else{
            temp_arr["selfShopPayByrCnt"]=0;
          }
          if(tab_data[i]["selfShopPayByrCnt"]["value"]!=NaN&&tab_data[i]["selfShopPayByrCnt"]>0){
            temp_arr["kdj"]=decimal(temp_arr["selfShopPayAmt"]/temp_arr["selfShopPayByrCnt"],2); //客单价
          }else{
            temp_arr["selfShopPayByrCnt"]=0;
          }
          temp_arr["selfShopPayRate"]=decimal((tab_data[i]["selfShopPayRate"])*100,2); //转化率
          temp_arr["uvCost"]=decimal(temp_arr["selfShopPayAmt"]/temp_arr["selfShopUv"],2);// UV价值
          temp_arr["id"]=c++;// 序号
        }
        tab_data1.push(temp_arr);
      }
    }                
    if(dateType=="today"){
      var ins1= table.render({
        elem: '#test'
        ,id: 'testReload'
        ,title: '竞店分析-流量来源'
        ,data:tab_data1
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,limit:10
        ,height:472
        ,cols: [[
          {field:'order', width:"10%", title: '#' ,sort: false}
          ,{field:'time', width:"10%", title: '日期', sort: true}
          ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'pageName', title: '流量来源', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'shop_name', title: '店铺名称',width: "20%"}
          ,{field: 'pictUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
            if (d.pictUrl!=undefined) {
              return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
            }else{
              return '<div style="text-align: center;" ></div>'; 
            }
          }}
          ,{field:'shop_userId', title: '店铺ID', sort: true,width: "15%"}
          ,{field:'selfShopUv', width: "15%", title: '访客数', sort: true}
        ]],  
        page:  {limits: [10, 20, 50, 100, 300,500]},
        done: function (res, curr, count) {
          exportData=res.data;
        }
      });
    }else{
      var ins1= table.render({
        elem: '#test'
        ,id: 'testReload'
        ,title: '竞店分析-流量来源'
        ,data:tab_data1
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,limit:10
        ,height:472
        ,cols: [[
          {field:'ids', width:"5%", title: '#' ,sort: false}
          ,{field:'time', width:"10%", title: '日期', sort: true}
          ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'pageName', title: '流量来源', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
          ,{field:'shop_name', title: '店铺名称',width: "10%"}
          ,{field: 'pictUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
            if (d.pictUrl!=undefined) {
              return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictUrl+'></div>';
            }else{
              return '<div style="text-align: center;" ></div>'; 
            }
          }}
          ,{field:'shop_userId', title: '店铺ID', sort: true,width: "6%"}
          ,{field:'selfShopUv', width: "8%", title: '访客数', sort: true}
          ,{field:'selfShopPayRate', width: "8%", title: '转化率(%)', sort: true}
          ,{field:'selfShopPayAmt', width: "8%", title: '交易金额', sort: true}
          ,{field:'selfShopPayByrCnt', width: "8%", title: '买家数', sort: true}
          ,{field:'kdj', width: "8%", title: '客单价', sort: true}
          ,{field:'selfShopUv', width: "8%", title: 'UV价值', sort: true}
        ]],  
        page:  {limits: [10, 20, 50, 100, 300,500]},
        done: function (res, curr, count) {
          exportData=res.data;
        }
      });
    }
  }else if(id=='jdlbExport'){
    if (dateType=="today"){
      exportdata=exportdata1['data'];
      $('#dateRange_text').text(exportdata1['updateTime']);
      var now_date=date_start=exportdata1['updateTime'];
    }else{  
      exportdata=exportdata1;
      var now_date=date_start==date_end?date_start:date_start+'|'+date_end;
    }
    tab_data=[];
    //重新加载
    for (var i = 0; i < exportdata.length; i++) {
      if (exportdata[i]['shop']["title"].indexOf(demoReload) >= 0||demoReload=="") {
        temp_arr={};
        if(dateType=='today'){
          // if(data.hasOwnProperty(data[i]["cate_cateRankId"])==true){
          if(data[i]["cate_cateRankId"]!= undefined){
              temp_arr["cate_cateRankId"]=data[i]["cate_cateRankId"]["value"];// 类目-行业排名
          }else{
              temp_arr["cate_cateRankId"]='';// 类目-行业排名
          }
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["shop_title"]=data[i]["shop"]["title"];
          temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
          temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
          temp_arr["shop_uvIndex"]=decimal(data2["shop_uvIndex"][i]["0"],0);// 全店-访客数
          temp_arr["shop_seIpvUvHits"]=decimal(data2["shop_seIpvUvHits"][i]["0"],0);// 全店-搜索人数
          temp_arr["shop_cltHits"]=decimal(data2["shop_cltHits"][i]["0"],0);// 全店-收藏人数
          temp_arr["shop_cartHits"]=decimal(data2["shop_cartHits"][i]["0"],0);// 全店-加购人数
          temp_arr["shop_payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 全店-转化率
          // temp_arr["cate_payRateIndex"]=decimal(data2["zhl2"][i]["0"]*100,2);// 类目-转化率
          temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-交易额
          // temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-预售定金金额
          // temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-预售定金商品件数
          if(data2["cate_tradeIndex"][i]){
            temp_arr["cate_tradeIndex"]=decimal(data2["cate_tradeIndex"][i]["0"],0);// 类目-交易额
          }else{
            temp_arr["cate_tradeIndex"]=0;// 类目-交易额
          }
          
          temp_arr["shop_payNum"]=decimal(decimal(data2["shop_uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 全店-买家数
          // temp_arr["cate_payNum"]=decimal(decimal(data2["shop_uvIndex"][i]["0"],0)*decimal(temp_arr["cate_payRateIndex"],2),0);// 类目-买家数

          
          // temp_arr["shop_fstOnsItmCnt"]=data[i]["shop_fstOnsItmCnt"]["value"];// 上新商品件数
        }else{
          temp_arr["order"]=i+1;
          temp_arr["time"]=now_date;
          temp_arr["page_name"]=sellerType_title;
          temp_arr["source"]= device_title;  //来源
          temp_arr["shop_title"]=data[i]["shop"]["title"];
          temp_arr["pictureUrl"]=data[i]["shop"]["pictureUrl"];  //店铺图片
          temp_arr["shop_userId"]=data[i]["shop"]["userId"]; //店铺ID
          temp_arr["shop_uvIndex"]=decimal(data2["shop_uvIndex"][i]["0"],0);// 全店-访客数
          temp_arr["shop_seIpvUvHits"]=decimal(data2["shop_seIpvUvHits"][i]["0"],0);// 全店-搜索人数
          temp_arr["shop_cltHits"]=decimal(data2["shop_cltHits"][i]["0"],0);// 全店-收藏人数
          temp_arr["shop_cartHits"]=decimal(data2["shop_cartHits"][i]["0"],0);// 全店-加购人数
          temp_arr["shop_payRateIndex"]=decimal(data2["zhl"][i]["0"]*100,2);// 全店-转化率
          temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-交易额
          // temp_arr["shop_payByrCntIndex"]=decimal(data2["shop_payByrCntIndex"][i]["0"],0);// 全店-预售定金金额
          // temp_arr["shop_tradeIndex"]=decimal(data2["shop_tradeIndex"][i]["0"],0);// 全店-预售定金商品件数
          temp_arr["cate_tradeIndex"]=decimal(data2["cate_tradeIndex"][i]["0"],0);// 类目-交易额

          temp_arr["cate_cartHits"]=decimal(data2["cate_cartHits"][i]["0"],0);// 类目-加购人数
          temp_arr["cate_uvIndex"]=decimal(data2["cate_uvIndex"][i]["0"],0);// 类目-访客数
          temp_arr["cate_seIpvUvHits"]=decimal(data2["cate_seIpvUvHits"][i]["0"],0);// 类目-搜索人数
          temp_arr["cate_cltHits"]=decimal(data2["cate_cltHits"][i]["0"],0);// 类目-搜藏人数
          temp_arr["cate_payRateIndex"]=decimal(data2["zhl2"][i]["0"]*100,2);// 类目-转化率
          temp_arr["shop_payNum"]=decimal(decimal(data2["shop_uvIndex"][i]["0"],0)*decimal(data2["zhl"][i]["0"],2),0);// 全店-买家数
          temp_arr["cate_payNum"]=decimal(decimal(temp_arr["cate_uvIndex"],0)*decimal(temp_arr["cate_payRateIndex"],2),0);// 类目-买家数

          temp_arr["cate_cateRankId"]=data[i]["cate_cateRankId"]["value"];// 类目-行业排名
          // temp_arr["shop_fstOnsItmCnt"]=data[i]["shop_fstOnsItmCnt"]["value"];// 全店-上新商品件数
        }
        tab_data.push(temp_arr);
      }
    }                
    if(dateType=='today'){
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控店铺-竞店列表'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"5%", title: '#' ,sort: false}
            ,{field:'time', width:"5%", title: '日期', sort: true}
            ,{field:'page_name', width:"5%", title: '平台'}
            ,{field:'source', title: '终端', width: "5%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            
            ,{field:'shop_title', title: '店铺名称',width: "6%" }
            ,{field: 'pictureUrl', width: "6%", title: '店铺图片',  sort: false,templet:function (d) { 
              if (d.pictureUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'shop_userId', title: '店铺ID', sort: true,width: "5%"}
            ,{field:'shop_uvIndex', width: "7%", title: '全店-访客数', sort: true}
            ,{field:'shop_seIpvUvHits', width: "7%", title: '全店-搜索人数', sort: true}
            ,{field:'shop_cltHits', width: "7%", title: '全店-收藏人数', sort: true}
            ,{field:'shop_cartHits', width: "7%", title: '全店-加购人数', sort: true}
            ,{field:'shop_payRateIndex', width: "7%", title: '全店-转化率', sort: true}
            ,{field:'shop_tradeIndex', width: "7%", title: '全店-交易额', sort: true}
            ,{field:'shop_payNum', width: "7%", title: '全店-买家数', sort: true}
            ,{field:'cate_tradeIndex', width: "7%", title: '类目-交易额', sort: true}
            ,{field:'cate_cateRankId', width: "7%", title: '类目-行业排名', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
      });
    }else{
      var ins1= table.render({
          elem: '#test'
          ,id: 'testReload'
          ,title: '监控店铺-竞店列表'
          ,data:tab_data
          ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
          ,limit:10
          ,height:472
          ,cols: [[
            {field:'order', width:"5%", title: '#' ,sort: false}
            ,{field:'time', width:"15%", title: '日期', sort: true}
            ,{field:'page_name', width:"8%", title: '平台'}
            ,{field:'source', title: '终端', width: "10%", minWidth: 30} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
            
            ,{field:'shop_title', title: '店铺名称',width: "10%" }
            ,{field: 'pictureUrl', width: "10%", title: '店铺图片',  sort: false,templet:function (d) { 
              if (d.pictureUrl!=undefined) {
                return '<div style="text-align: center;" ><img style=" width: 30px;height: 30px;"  src=https:'+d.pictureUrl+'></div>';
              }else{
                return '<div style="text-align: center;" ></div>'; 
              }
            }}
            ,{field:'shop_userId', title: '店铺ID', sort: true,width: "10%"}
            ,{field:'shop_uvIndex', width: "10%", title: '全店-访客数', sort: true}
            ,{field:'shop_seIpvUvHits', width: "7%", title: '全店-搜索人数', sort: true}
            ,{field:'shop_cltHits', width: "7%", title: '全店-收藏人数', sort: true}
            ,{field:'shop_cartHits', width: "7%", title: '全店-加购人数', sort: true}
            ,{field:'shop_payRateIndex', width: "7%", title: '全店-转化率', sort: true}
            ,{field:'shop_tradeIndex', width: "7%", title: '全店-交易额', sort: true}
            ,{field:'shop_payNum', width: "7%", title: '全店-买家数', sort: true}
            ,{field:'cate_uvIndex', width: "7%", title: '类目-访客数', sort: true}
            ,{field:'cate_seIpvUvHits', width: "7%", title: '类目-搜索人数', sort: true}
            ,{field:'cate_cartHits', width: "7%", title: '类目-加购人数', sort: true}
            ,{field:'cate_cltHits', width: "7%", title: '类目-收藏人数', sort: true}
            ,{field:'cate_payRateIndex', width: "7%", title: '类目-转化率', sort: true}
            ,{field:'cate_tradeIndex', width: "7%", title: '类目-交易额', sort: true}
            ,{field:'cate_payNum', width: "7%", title: '类目-买家数', sort: true}
            ,{field:'cate_cateRankId', width: "7%", title: '类目-行业排名', sort: true}
          ]],  
          page:  {limits: [10, 20, 50, 100, 300,500]},
          done: function (res, curr, count) {
            exportData=res.data;
          }
      });
    }
  }

});



function getDayTypeTwo(day){    
       var today = new Date();    
           
       var targetday_milliseconds=today.getTime() + 1000*60*60*24*day;            
    
       today.setTime(targetday_milliseconds); //注意，这行是关键代码  
           
       var tYear = today.getFullYear();    
       var tMonth = today.getMonth();    
       var tDate = today.getDate();    
       tMonth = doHandleMonth(tMonth + 1);    
       tDate = doHandleMonth(tDate);    
       return tYear+"-"+tMonth+"-"+tDate;    
}    

function doHandleMonth(month){    
       var m = month;    
       if(month.toString().length == 1){    
          m = "0" + month;    
       }    
       return m;    
}  


function getSelfMonth(date,str){    



  date=date+"-01";

  now = new Date(date.replace(/\-/g,"/"));
  oDate =new Date(now.setMonth(now.getMonth() - str));
  oYear = oDate.getFullYear(),//年
  oMonth = oDate.getMonth()+1,//月
  oTime = oYear +'-'+ getzf(oMonth);//最后拼接时间


  return oTime;
}  



  function getMyDate(str){
            var oDate = new Date(str),
                oYear = oDate.getFullYear(),//年
                oMonth = oDate.getMonth()+1,//月
                oDay = oDate.getDate(),//日
                oHour = oDate.getHours(),//时
                oMin = oDate.getMinutes(),//分
                oSen = oDate.getSeconds(),//秒
                oFf=oDate.getMilliseconds()//毫秒
                oTime = oYear +'-'+ getzf(oMonth) +'-'+ getzf(oDay) +' '+ getzf(oHour) +':'+ getzf(oMin) +':'+getzf(oSen)+':'+getzf(oFf);//最后拼接时间
            return oTime;
        };
        //补0操作
        function getzf(num){
            if(parseInt(num) < 10){
                num = '0'+num;
            }
            return num;
        }

//保留小数
function decimal(num,v){
var vv = Math.pow(10,v);
return Math.round(num*vv)/vv;
}



/*
    格式化成可以直接请求指数接口的数据
data:需要格式化的数据
titles_arr：需要转化的指数，第一种加密（数组类型）
zhl：需要转化的指数，第二种加密（字符类型）
 */
function formattingData(data,titles_arr,zhl,zhl2=""){

arr_all={};
for (var j = 0; j < titles_arr.length; j++) {
  for (var i = 0; i < data.length; i++) {
    if (arr_all.hasOwnProperty(titles_arr[j])) {
      temp_arr=arr_all[titles_arr[j]];

      if (data[i][titles_arr[j]]==0||data[i][titles_arr[j]]==undefined||data[i][titles_arr[j]]=="undefined"||data[i][titles_arr[j]].hasOwnProperty("value")==false) {
        temp_arr.push(0);
      }else{
        temp_arr.push(data[i][titles_arr[j]]["value"]);
      }
        
      arr_all[titles_arr[j]] = temp_arr;
    }else{
      temp_arr=[];
      
      if (data[i][titles_arr[j]]==0||data[i][titles_arr[j]]==undefined||data[i][titles_arr[j]]=="undefined"||data[i][titles_arr[j]].hasOwnProperty("value")==false) {
        temp_arr.push(0);
      }else{
        temp_arr.push(data[i][titles_arr[j]]["value"]);
      }

      arr_all[titles_arr[j]] = temp_arr;
    }
  }
}



     

  if (zhl!="") {
      for (var i = 0; i < data.length; i++) {
      if (arr_all.hasOwnProperty("zhl")) {
        temp_arr=arr_all["zhl"];

        if (data[i][zhl]==0||data[i][zhl]==undefined||data[i][zhl]=="undefined") {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[i][zhl]["value"]);
        }
       
        arr_all["zhl"] = temp_arr;
      }else{
        temp_arr=[];

        if (data[i][zhl]==0||data[i][zhl]==undefined||data[i][zhl]=="undefined") {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[i][zhl]["value"]);
        }

        arr_all["zhl"] = temp_arr;
      }
    }
  }



    if (zhl2!="") {
      for (var i = 0; i < data.length; i++) {
      if (arr_all.hasOwnProperty("zhl2")) {
        temp_arr=arr_all["zhl2"];

        if (data[i][zhl2]==0||data[i][zhl2]==undefined||data[i][zhl2]=="undefined") {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[i][zhl2]["value"]);
        }

        arr_all["zhl2"] = temp_arr;
      }else{
        temp_arr=[];

        if (data[i][zhl2]==0||data[i][zhl2]==undefined||data[i][zhl2]=="undefined") {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[i][zhl2]["value"]);
        }

        arr_all["zhl2"] = temp_arr;
      }
    }
  }

  arr_all={arr_all:{arr_all:arr_all}};


return arr_all;

}





/*
格式化成可以直接请求指数接口的数据（一个数据的类型）
data:需要格式化的数据
titles_arr：需要转化的指数，第一种加密（数组类型）
zhl：需要转化的指数，第二种加密（字符类型）
 */
function formattingDataOne(data,titles_arr,zhl,zhl2=""){

arr_all={};
for (var j = 0; j < titles_arr.length; j++) {

    if (arr_all.hasOwnProperty(titles_arr[j])) {
      temp_arr=arr_all[titles_arr[j]];

      if (data[titles_arr[j]]==0) {
        temp_arr.push(0);
      }else{
        temp_arr.push(data[titles_arr[j]]["value"]);
      }
      
      arr_all[titles_arr[j]] = temp_arr;
    }else{
      temp_arr=[];
      
      if (data[titles_arr[j]]==0) {
        temp_arr.push(0);
      }else{
        temp_arr.push(data[titles_arr[j]]["value"]);
      }

      arr_all[titles_arr[j]] = temp_arr;
    }
  
}

  if (zhl!="") {

      if (arr_all.hasOwnProperty("zhl")) {
        temp_arr=arr_all["zhl"];
        if (data[zhl]==0) {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[zhl]["value"]);
        }
       
        arr_all["zhl"] = temp_arr;
      }else{
        temp_arr=[];

        if (data[zhl]==0) {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[zhl]["value"]);
        }
        arr_all["zhl"] = temp_arr;
      }
    
  }



    if (zhl2!="") {

      if (arr_all.hasOwnProperty("zhl2")) {
        temp_arr=arr_all["zhl2"];

        if (data[zhl2]==0) {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[zhl2]["value"]);
        }

        arr_all["zhl2"] = temp_arr;
      }else{
        temp_arr=[];

        if (data[zhl2]==0) {
          temp_arr.push(0);
        }else{
           temp_arr.push(data[zhl2]["value"]);
        }

        arr_all["zhl2"] = temp_arr;
      }
 
  }




  arr_all={arr_all:{arr_all:arr_all}};

return arr_all;
}

//转换指数包含两个第二中加密
function formattingData00(data,titles_arr,zhl,zhl2){

arr_all={};
for (var j = 0; j < titles_arr.length; j++) {
  for (var i = 0; i < data.length; i++) {
    if (arr_all.hasOwnProperty(titles_arr[j])) {
      temp_arr=arr_all[titles_arr[j]];
      temp_arr.push(data[i][titles_arr[j]]["value"]);
      arr_all[titles_arr[j]] = temp_arr;
    }else{
      temp_arr=[];



      temp_arr.push(data[i][titles_arr[j]]["value"]);
      arr_all[titles_arr[j]] = temp_arr;
    }
  }
}

  if (zhl!="") {
      for (var i = 0; i < data.length; i++) {
      if (arr_all.hasOwnProperty("zhl")) {
        temp_arr=arr_all["zhl"];
        temp_arr.push(data[i][zhl]["value"]);
        arr_all["zhl"] = temp_arr;
      }else{
        temp_arr=[];
        temp_arr.push(data[i][zhl]["value"]);
        arr_all["zhl"] = temp_arr;
      }
    }
  }
  if (zhl2!="") {
      for (var i = 0; i < data.length; i++) {
      if (arr_all.hasOwnProperty("zhl2")) {
        temp_arr=arr_all["zhl2"];
        temp_arr.push(data[i][zhl2]["value"]);
        arr_all["zhl2"] = temp_arr;
      }else{
        temp_arr=[];
        temp_arr.push(data[i][zhl2]["value"]);
        arr_all["zhl2"] = temp_arr;
      }
    }
  }
  arr_all={arr_all:{arr_all:arr_all}};


return arr_all;

}



//指数转换无value
function formattingData01(data,titles_arr,zhl){

arr_all={};
for (var j = 0; j < titles_arr.length; j++) {
  for (var i = 0; i < data.length; i++) {
    if (arr_all.hasOwnProperty(titles_arr[j])) {
      temp_arr=arr_all[titles_arr[j]];
      temp_arr.push(data[i][titles_arr[j]]);
      arr_all[titles_arr[j]] = temp_arr;
    }else{
      temp_arr=[];
      temp_arr.push(data[i][titles_arr[j]]);
      arr_all[titles_arr[j]] = temp_arr;
    }
  }
}

  if (zhl!="") {
      for (var i = 0; i < data.length; i++) {
      if (arr_all.hasOwnProperty("zhl")) {
        temp_arr=arr_all["zhl"];
        temp_arr.push(data[i][zhl]);
        arr_all["zhl"] = temp_arr;
      }else{
        temp_arr=[];
        temp_arr.push(data[i][zhl]);
        arr_all["zhl"] = temp_arr;
      }
    }
  }

  arr_all={arr_all:{arr_all:arr_all}};


return arr_all;

}



//去除字符串中所有的空格
function Trim(str,is_global)
{
    var result;
    result = str.replace(/(^\s+)|(\s+$)/g,"");
    if(is_global.toLowerCase()=="g")
    {
        result = result.replace(/\s/g,"");
     }
    return result;
}


//获取url中的参数
function getUrlParam(name) {
var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
var r = window.location.search.substr(1).match(reg);  //匹配目标参数
if (r != null) return unescape(r[2]); return null; //返回参数值
}


//请求url
function httpRequest(url, callback){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            callback(xhr.responseText);
        }
    }
    xhr.send();
}






$(document).on('click',"#inserLoginBtn", function(e) { 
//点击用户信息
  nick = $(".ebase-Selector__title").text();
   phone = localStorage.dzs_phone;
  if (localStorage.dzs_phone!=undefined) {
    html='<div id="myModal" class="reveal-modal"><div class="el-dialog__header"><span class="el-dialog__title">用户信息</span></div><div class="layui-form " lay-filter="jpts_form" ><div class="layui-form-item"><label style="width:100%;text-align:left;font-size: 14px;" class="layui-form-label">店铺名字：'+nick+'</label></div><div class="layui-form-item"><label style="width:100%;text-align:left;font-size: 14px;" class="layui-form-label">账户名称：'+phone+' </label></div></div></div>';
      }else{
    html='<div id="myModal" class="reveal-modal"><div class="el-dialog__header"><span class="el-dialog__title">用户信息</span></div><div class="layui-form " lay-filter="jpts_form" ><div class="layui-form-item"><label style="width:100%;text-align:left;font-size: 14px;" class="layui-form-label">店铺名字：'+nick+'</label></div><div class="layui-form-item"><label style="width:100%;text-align:left;font-size: 14px;" class="layui-form-label">账户名称：未登录 <button style="margin-left: 18px;" id="dzs_login" class="layui-btn layui-btn-sm layui-btn-normal">登陆</button> </label></div></div></div>';
  }
  
 layer.open({
      type: 1,
      title:false,
      area: ['420px', '219px'],
      shadeClose: true, //点击遮罩关闭
      content: html 
    });

   // form.render();

});





$(document).on('click',"#dzs_login", function(e) { 
//注册
  
  nick = $(".ebase-Selector__title").text();
  html_login='<div class="layui-form-item"><label class="layui-form-label">手机号</label><div class="layui-input-block" style="margin-right: 18px;margin-left: 80px;"><input type="text" id="phone"  name="phone" lay-verify="phone" autocomplete="off" placeholder="请输入手机号" class="layui-input"></div></div><div class="layui-form-item"><label class="layui-form-label">密码</label><div class="layui-input-block" style="margin-right: 18px;margin-left:80px;" ><input type="password" id="password"  name="password" placeholder="请输入密码" autocomplete="off" class="layui-input"></div></div><div class="layui-form-item"><div style="margin-right: 18px;margin-left:80px;"  class="layui-input-block"><button id="dzs_dl" class="layui-btn layui-btn-sm layui-btn-normal">登陆</button> <a  href="http://www.dianzhushou.net/site/register?nick='+nick+'"  target="view_window" style="text-decoration:none" class="layui-btn layui-btn-sm layui-btn-normal">注册</a> </div></div>';
 $(".layui-form ").html(html_login);


});


$(document).on('click',"#dzs_dl", function(e) { 
//点击登录

   phone = $("#phone").val();
   password = $("#password").val();
   nick = $(".ebase-Selector__title").text();

   if (phone=="") {
     layer.msg("请输入手机号", {icon: 2,  time:3500,});
   }else if (password=="") {
    layer.msg("请输入密码", {icon: 2,  time:3500,});
   }else{

        $.ajax({
                  type:'post',
                  url:"https://work.dinghaiec.com/set_save.php",  
                  data:{state:22,phone:phone,password:password,nick},
                  dataType:'json',
                  async: false,
                  beforeSend: function (request) {     
                  },
                  success:function(result)
                  {  

                    if (result["status"]==1) {
                     layer.msg(result["message"], {icon: 1,  time:1500,});
                     
                     html='    <div class="layui-form-item"><label style="width:100%;text-align:left;font-size: 14px;" class="layui-form-label">店铺名字：'+nick+'</label></div><div class="layui-form-item"> <label style="width:100%;text-align:left;font-size: 14px;" class="layui-form-label">账户名称：'+phone+' </label> </div>';
                     $(".layui-form ").html(html);
                     localStorage.dzs_phone=phone;

                    }else{
                     layer.msg(result["message"], {icon: 2,  time:3500,}); 
                    }

                  }
         });

   }
});





$(document).on('click',".button-primary", function(e) { 

  // localStorage.is_one="undefined";

  //检测是否是第一次使用
  if (localStorage.is_one==undefined||localStorage.is_one=="undefined") {

    html='<div class="el-dialog__body"><p style="margin-top: 50px;  font-size: 40px; color: rgb(60, 172, 252); font-weight: bold; text-align: center; letter-spacing: 0px;"><span style="margin-right: 40px;">提</span><span>醒</span></p> <p style="color: rgb(60, 172, 252); font-size: 22px; text-align: center; margin: 2px 0px 20px;">使用前须加客服微信</p> <p style="text-align: center;"><img width="200" height="200" src="http://www.dianzhushou.net//image/dzs_wx.jpg"></p> <p style="height: 70px;"></p></div>';
    
     layer.open({
        type: 1,
        title:false,
        area: ['650px', '450px'],
        shadeClose: true, //点击遮罩关闭
        content: html 
      });

    localStorage.is_one="yes";
  }
});





